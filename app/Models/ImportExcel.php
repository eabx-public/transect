<?php 
namespace App\Models;
use Ppci\Models\PpciModel;


/**
 * @author Eric Quinton
 * @copyright Copyright (c) 2014, IRSTEA / Eric Quinton
 * @license http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html LICENCE DE LOGICIEL LIBRE CeCILL-C
 *  Creation 11 juin 2014
 */


include_once 'plugins/phpExcelReader-2.21/excel_reader2.php';
/**
 * Classe permettant d'extraire les données de la feuille excel
 * @author quinton
 *
 */
class ImportExcel extends Spreadsheet_Excel_Reader
{
	public $fileOk = 0;
	public function __construct()
	{
		parent::__construct();
		if (is_file($nomFichier)) {
			$this->read($nomFichier);
			if (count($this->sheets) > 0) $this->fileOk = 1;
		}
	}
	/**
	 * Affiche le contenu de la première feuille
	 */
	function getDataTest($data)
	{
		for ($i = 1; $i <= $data->sheets[0]['numRows']; $i++) {
			for ($j = 1; $j <= $data->sheets[0]['numCols']; $j++) {
				echo "\"" . $data->sheets[0]['cells'][$i][$j] . "\",";
			}
			echo "\n";
		}
	}
}
