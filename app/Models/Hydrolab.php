<?php

namespace App\Models;

use Ppci\Models\PpciModel;

class Hydrolab extends PpciModel
{
    public function __construct()
    {

        $this->table = "hydrolab";

        $this->fields = array(
            "hydrolab_id" => array(
                "type" => 1,
                "key" => 1,
                "requis" => 1,
                "defaultValue" => 0
            ),
            "trait_id" => array(
                "type" => 1,
                "requis" => 1,
                "parentAttrib" => 1
            ),
            "releve_date" => array(
                "type" => 3,
                "requis" => 1
            ),
            "profondeur" => array(
                "type" => 1
            ),
            "conductivite" => array(
                "type" => 1
            ),
            "salinite" => array(
                "type" => 1
            ),
            "temperature" => array(
                "type" => 1
            ),
            "turbidite" => array(
                "type" => 1
            ),
            "oxygene_dissous" => array(
                "type" => 1
            ),
            "saturation_oxygene" => array(
                "type" => 1
            )
        );
        parent::__construct();
    }
}
