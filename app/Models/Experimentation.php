<?php

namespace App\Models;

use Ppci\Models\PpciModel;

/**
 * ORM de gestion de la table experimentation
 *
 * @author quinton
 *
 */
class Experimentation extends PpciModel
{
	public function __construct()
	{

		$this->table = "experimentation";

		$this->fields = array(
			"experimentation_id" => array(
				"type" => 1,
				"key" => 1,
				"requis" => 1,
				"defaultValue" => 0
			),
			"experimentation_nom" => array(
				"type" => 0,
				"requis" => 1
			),
			"experimentation_description" => array(
				"requis" => 1
			),
			"experimentation_debut" => array(
				"type" => 2
			),
			"experimentation_fin" => array(
				"type" => 2
			)
		);
		parent::__construct();
	}
	/**
	 * Retourne la liste des experimentations actives (sans date de fin)
	 * et celle qui correspond au trait_id fourni
	 *
	 * @param int $trait_id
	 * @return tableau
	 */
	function getListActif($trait_id = 0)
	{
		$sql = "select * from experimentation
				where experimentation_fin is null ";
		$data = [];
		if ($trait_id > 0) {
			$sql .= " or experimentation_id in (select experimentation_id from trait
						where trait_id = :trait_id:)";
			$data["trait_id"] = $trait_id;
		}

		$order = " order by experimentation_id desc";
		return $this->getListeParam($sql . $order, $data);
	}
}
