<?php 
namespace App\Models;

class Xml
{

	/**
	 * Fonction permettant de charger un fichier xml, et de récupérer le
	 * tableau correspondant
	 * Les objets imbriqués sont stockés dans un tableau de type :
	 * array [0] ['val'] ['valUnique']
	 *                   ['valUnique2']
	 *                   [0] ['sousVal']['sousValUnique']
	 *                                  ['sousValUnique2']
	 *                   [1] ['sousVal']['sousValUnique']
	 *                                  ['sousValUnique2']
	 *       [1] ['val'] (...)
	 * La fonction ne gère pas les attributs
	 * 
	 * @param string $fileName : adresse du fichier à charger       	
	 * @return array
	 */
	function loadFile($fileName)
	{
		$dataXml = simplexml_load_file($fileName);
		$json = json_encode($dataXml);
		$data = json_decode($json, TRUE);
		//$data = $this->xml2array ( $dataXml );
		return $data;
	}
	/**
	 * 
	 * @param SimpleXMLElement $xml
	 * @return array
	 */
	function xml2array($xml)
	{
		$res = array();
		$i = 0;
		foreach ($xml as $element) {
			$key = $element->getName();
			$e = get_object_vars($element);
			if (!empty($e)) {
				if ($element instanceof \SimpleXMLElement) {
					$res[$i][$key] = $this->xml2array($element);
					$i++;
				} else {
					$res[$key] = $e;
				}
			} else
				$res[$key] = trim($element);
		}
		return $res;
	}
}
