<?php

namespace App\Models;

use chillerlan\QRCode\QRCode;
use Config\App;
use Ppci\Libraries\PpciException;
use Ppci\Models\PpciModel;

class Individu extends PpciModel
{
    private $sql = "select individu.*, type_code, nom, nom_fr, code_csp, nom as taxon
					from individu
					join echantillon using (echantillon_id)
					join peche using (peche_id)
					join espece_type using (espece_type_id)
                    join espece using (espece_id)
					left outer join type using (type_id)
					";

    private $dataPrint;
    /**
     *
     * @var App
     */
    public $appConfig;

    public function __construct()
    {


        $this->table = "individu";

        $this->fields = array(
            "individu_id" => array(
                "type" => 1,
                "key" => 1,
                "requis" => 1,
                "defaultValue" => 0
            ),
            "echantillon_id" => array(
                "type" => 1,
                "requis" => 1,
                "parentAttrib" => 1
            ),
            "longueur_fourche" => array(
                "type" => 1
            ),
            "longueur_fourche_by_classe" => array(
                "type" => 1
            ),
            "individu_masse" => array(
                "type" => 1
            )
        );
        $this->appConfig = service ("AppConfig");
        parent::__construct();
    }

    /**
     * Retourne la liste des individus rattachés à un échantillon
     *
     * @param int $echantillon_id
     */
    function getListFromEchantillon(int $echantillon_id)
    {
        $sql = "select individu_id, echantillon_id,
                    longueur_fourche, longueur_fourche_by_classe, individu_masse,
                    e.uuid as parent_uuid,
                    trait_debut, site_transect_name, rive_libelle, position_engin_name,
                    nom, nom_fr
                    from individu
                    join echantillon e using (echantillon_id)
                    join espece_type using (espece_type_id)
                    join espece using (espece_id)
                    join v_trait_peche using (peche_id)
					where echantillon_id = :echantillon_id:
					order by individu_id";
        return $this->getListeParamAsPrepared($sql, array("echantillon_id" => $echantillon_id));
    }

    /**
     * Retourne la liste des individus pour une pêche donnée
     *
     * @param int $peche_id
     * @return array
     */
    function getListFromPeche($peche_id)
    {
        if ($peche_id > 0) {
            $sql = $this->sql . "
					where peche_id = :peche_id:
					order by ordre_tri, nom_fr, type_code";
            return $this->getListeParam($sql, ["peche_id" => $peche_id]);
        }
    }

    /**
     * Genere le QRcode pour tous les individus rattaches a un echantillon
     *
     * @param int $ech_id
     *            : identifiant de l'echantillon
     * @return array : liste des individus
     */
    function generateQrcodeFromEchan($ech_id)
    {
        $data = array();
        if ($ech_id>0) {
            $sql = $this->sql . " where echantillon_id = :echantillon_id:";
            $data = $this->getListeParamAsPrepared(
                $sql,
                array(
                    "echantillon_id" => $ech_id
                )
            );
            $echantillon = new Echantillon;
            $dechan = $echantillon->lire($ech_id);
            /*
             * Recuperation du radical de l'identifiant metier
             */
            $radical = $echantillon->generateCSName($dechan);
            $qrcode = new QRCode();
            /*
             * Traitement de chaque individu
             */
            foreach ($data as $key => $row) {
                if ($row["individu_id"] > 0) {
                    $code = array();
                    $code["db"] = "transect";
                    $code["uid"] = $row["individu_id"];
                    $code["id"] = $radical . "-" . $row["individu_id"];
                    $code["taxon"] = $row["taxon"];
                    $filename = WRITEPATH. 'temp/' . $row["individu_id"] . ".png";
                    $qrcode->clearSegments();
                    $qrcode->render(json_encode($code), $filename);
                    /*
                     * Ajout du nom genere dans la liste, pour utilisation dans l'etiquette
                     */
                    $data[$key]["radical"] = $radical;
                }
            }
        }
        return $data;
    }

    /**
     * Genere le fichier PDF des etiquettes pour tous les poissons de l'echantillon
     *
     * @param int $ech_id
     *            : identifiant de l'echantillon
     * @throws PpciException
     * @return string : nom du fichier pdf genere
     */
    function generatePdfFromEchan($ech_id)
    {
        $APPLI_temp = WRITEPATH.'temp';
        $APPLI_fop = $this->appConfig->fop;
        $pdffile = "";
        $data = $this->generateQrcodeFromEchan($ech_id);
        /*
         * Generation du fichier xml
         */
        if (count($data) > 0) {
            try {
                $xml_id = bin2hex(openssl_random_pseudo_bytes(6));
                /*
                 * Preparation du fichier xml
                 */
                $doc = new \DOMDocument('1.0', 'utf-8');
                $objects = $doc->createElement("objects");
                foreach ($data as $object) {
                    $item = $doc->createElement("object");
                    foreach ($object as $key => $value) {
                        if (strlen($key) > 0 && (strlen($value) > 0 || ($value === false))) {
                            // cas des booléens
                            if ($value === true) {
                                $elem = $doc->createElement($key, "true");
                            } elseif ($value === false) {
                                $elem = $doc->createElement($key, "false");
                            } else {
                                $elem = $doc->createElement($key, $value);
                            }

                            $item->appendChild($elem);
                        }
                    }
                    $objects->appendChild($item);
                }
                $doc->appendChild($objects);

                $xmlfile = $APPLI_temp . '/' . $xml_id . ".xml";
                if (!$doc->save($xmlfile)) {
                    throw new PpciException("Impossible de générer le fichier XML");
                }
                if (!file_exists($xmlfile)) {
                    throw new PpciException("Impossible de générer le fichier XML");
                }
                /*
                 * Recuperation du fichier xsl
                 */
                $xslfile = $this->appConfig->individuLabelTemplate;
                $pdffile = $APPLI_temp . '/' . $xml_id . ".pdf";
                $command = $APPLI_fop . " -xsl $xslfile -xml $xmlfile -pdf $pdffile";
                exec($command);
                /*
                 * Nettoyage des fichiers
                 */
                foreach ($data as $value) {
                    unlink($APPLI_temp . "/" . $value["individu_id"] . ".png");
                }
                unlink($xmlfile);
            } catch (\Exception $e) {
                $this->message->set("Erreur lors de la génération du fichier xml",true);
                $this->message->setSyslog($e->getMessage());
            }
        } else {
            throw new PpciException("Génération des étiquettes : pas d'individus saisis");
        }
        return $pdffile;
    }
}
