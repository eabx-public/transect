<?php

namespace App\Models;

use Ppci\Models\PpciModel;


/**
 * ORM de gestion de la table peche
 *
 * @author quinton
 *
 */
class Peche extends PpciModel
{
    public array $warning = array();

    public function __construct()
    {

        $this->table = "peche";

        $this->fields = array(
            "peche_id" => array(
                "type" => 1,
                "key" => 1,
                "requis" => 1,
                "defaultValue" => 0
            ),
            "trait_id" => array(
                "type" => 1,
                "requis" => 1,
                "parentAttrib" => 1
            ),
            "materiel_id" => array(
                "type" => 1,
                "requis" => 1
            ),
            "position_engin_id" => array(
                "type" => 1,
                "requis" => 1
            ),
            "etat_conservation_echan" => array(
                "type" => 1
            ),
            "couranto_debut" => array(
                "type" => 1
            ),
            "couranto_fin" => array(
                "type" => 1
            ),
            "volume_filtre" => array(
                "type" => 1
            ),
            "volume_filtre_pri" => array(
                "type" => 1
            ),
            "couranto_sec_debut" => array(
                "type" => 1
            ),
            "couranto_sec_fin" => array(
                "type" => 1
            ),
            "volume_filtre_secours" => array(
                "type" => 1
            ),
            "capture_non_conserve" => array(
                "type" => 0
            ),
            "oxygene" => array(
                "type" => 1
            ),
            "temp_eau" => array(
                "type" => 1
            ),
            "turbidite" => array(
                "type" => 1
            ),
            "salinite" => array(
                "type" => 1
            ),
            "conductivite" => array(
                "type" => 1
            ),
            "vitesse" => array(
                "type" => 1
            ),
            "vitesse_sec" => array(
                "type" => 1
            ),
            "duree_filage" => array(
                "type" => 1
            ),
            "duree_virage" => array(
                "type" => 1
            ),
            "capture_other" => array(
                "type" => 0
            ),
            "commentaire" => array(
                "type" => 0
            ),
            "gelatineux" => array(
                "type" => 1
            ),
            "oxygene_sat" => array(
                "type" => 1
            ),
            "immersion_tx" => array(
                "type" => 1
            ),
            "ph" => array(
                "type" => 1
            ),
            "completed" => array(
                "type" => 1,
                "defaultValue" => 0
            ),
            "cs_uid" => array(
                "type" => 1
            ),
            "poisson_congele" => array(
                "type" => 1
            ),
            "congelation_ok" => array(
                "type" => 1
            ),
            "uuid" => array(
                "type" => 0
            ),
            "couranto_valid" => array(
                "type" => 1,
                "defaultValue" => 1
            )
        );
        parent::__construct();
    }

    function getDefaultValues($parentKey = 0): array
    {
        $data = parent::getDefaultValues($parentKey);
        $data["uuid"] = $this->getUUID();
        return $data;
    }

    /**
     * Fonction permettant d'importer des données externes dans la base
     * (relevés bateau)
     *
     * @param array $data
     * @return int
     */
    function importData($data)
    {
        $retour = -1;
        if (is_array($data)) {
            /*
             * Recherche d'un enregistrement pré-existant dans la base
             */
            if ($data["trait_id"] > 0) {
                if ($data["position_engin_id"] > 0) {
                    $sql = "select peche_id from peche
							 where trait_id = :trait_id:
							  and position_engin_id = :position_engin_id:";
                    $res = $this->lireParam($sql, ["trait_id" => $data["trait_id"], "position_engin_id" => $data["position_engin_id"]]);
                    if ($res["peche_id"] > 0) {
                        $data["peche_id"] = $res["peche_id"];
                    } else {
                        $data["peche_id"] = 0;
                    }
                    /**
                     * Forçage du volume du couranto principal pour les filets babord et tribord
                     */
                    if (empty($data["volume_fitre_pri"] && ($data["position_engin_id"] == 2 || $data["position_engin_id"] == 3))) {
                        $data["volume_fitre_pri"] = $data["volume_filtre"];
                    }
                    /**
                     * Mise à niveau de l'indicateur couranto_valid pour le fond
                     */
                    if ($data["volume_filtre"] == $data["volume_filtre_secours"] && $data["position_engin_id"] == 4) {
                        $data["couranto_valid"] = 0;
                    }
                    /*
                     * Recherche des warnings
                     */
                    $twarning = array(
                        "temp_eau" => array(
                            "min" => 0,
                            "max" => 30
                        ),
                        "conductivite" => array(
                            "min" => 0,
                            "max" => 100000
                        ),
                        /*"oxygene" => array (
                                                     "min" => 1.5,
                                                     "max" => 22
                                             ),*/
                        /*
                         * "turbidite" => array ( "min" => 25, "max" => 2000 ),
                         */
                        "salinite" => array(
                            "min" => 0,
                            "max" => 30
                        ),
                        "volume_filtre" => array(
                            "min" => 100,
                            "max" => 9000
                        ),
                        "vitesse" => array(
                            "min" => 0.5,
                            "max" => 2.3
                        )
                    );
                    $this->warning = array();
                    foreach ($twarning as $kwarn => $dwarn) {
                        if (is_null($data[$kwarn])) {
                            $this->warning[] = $kwarn . " : l'information n'a pas été renseignée";
                        } else {
                            /*
                             * Remplacement des virgules par des points, le cas echeant
                             */
                            $data[$kwarn] = str_replace(",", ".", $data[$kwarn]);
                            /*
                             * Recherche des valeurs mini et maxi
                             */
                            if ($data[$kwarn] < $dwarn["min"] || $data[$kwarn] > $dwarn["max"]) {
                                $this->warning[] = $kwarn . " : la valeur renseignée (" . $data[$kwarn] . ")
									est hors limite (min : " . $dwarn["min"] . ', max :' . $dwarn["max"] . ')';
                            }
                        }
                    }
                    /*
                     * Ecriture de l'enregistrement
                     */
                    foreach ($data as $k => $v) {
                        if (is_array($v)) {
                            $data[$k] = $v[0];
                        }
                    }
                    $retour = $this->ecrire($data);
                } else {
                    $this->errorData[][0]["message"] = "La position de l'engin de pêche n'a pas été renseignée - problème technique";
                }
            } else {
                $this->errorData[][0]["message"] = "Le numéro informatique du trait n'est pas renseigné - problème technique";
            }
        } else {
            $this->errorData[][0]["message"] = 'Pas de données à importer';
        }
        return $retour;
    }

    function getListeFromTrait($id)
    {
        if ($id > 0) {
            $sql = "select p.peche_id, p.trait_id, p.materiel_id, p.position_engin_id, p.conductivite,
            p.oxygene, p.temp_eau, p.turbidite, p.salinite, p.ph, p.oxygene_sat,
					to_char(couranto_debut, '9G999G999') as couranto_debut,
					to_char(couranto_fin, '9G999G999') as couranto_fin,
					to_char(couranto_sec_debut, '9G999G999') as couranto_sec_debut,
					to_char(couranto_sec_fin, '9G999G999') as couranto_sec_fin,
					p.volume_filtre, p.volume_filtre_pri, p.volume_filtre_secours,
					p.capture_non_conserve, p.capture_other,
					p.vitesse, p.vitesse_sec,
					p.duree_filage, p.duree_virage,
					p.commentaire,
					position_engin_name, p.gelatineux,
					echantillon_nb,
					p.completed, p.cs_uid,
                    p.poisson_congele, p.congelation_ok,
                    couranto_valid,
                    round((volume_filtre_pri/duree_seconde/ case when position_engin_id = 4 then 2.4 else 4 end )::numeric,2) as vitesse_couranto
				from peche p
                join v_trait_peche using (peche_id)
				left outer join v_echantillon_nb_by_peche using (peche_id)
				where p.trait_id = :id:
				order by (position_engin_id=3), (position_engin_id=4), (position_engin_id=2)";
            return ($this->getListeParam($sql, ["id" => $id]));
        }
    }
}
