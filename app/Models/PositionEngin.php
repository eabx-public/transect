<?php

namespace App\Models;

use Ppci\Models\PpciModel;

/**
 * ORM de gestion de la table position_engin
 *
 * @author quinton
 *
 */
class PositionEngin extends PpciModel
{

    public function __construct()
    {

        $this->table = "position_engin";

        $this->fields = array(
            "position_engin_id" => array(
                "type" => 1,
                "requis" => 1,
                "key" => 1
            ),
            "position_engin_name" => array(
                "requis" => 1
            ),
            "profondeur_peche_id" => array(
                "type" => 1,
                "requis" => 1
            )
        );
        parent::__construct();
    }
}
