<?php

namespace App\Models;

use Ppci\Models\PpciModel;

class Protocole extends PpciModel
{
    public function __construct()
    {
        $this->table = "protocole";
        $this->fields = array(
            "protocole_id" => array(
                "type" => 1,
                "key" => 1,
                "requis" => 1,
                "defaultValue" => 0
            ),
            "protocole_name" => array(
                "type" => 0,
                "requis" => 1
            ),
            "protocole_description" => array(
                "requis" => 1
            )
        );
        parent::__construct();
    }
}
