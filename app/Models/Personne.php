<?php

namespace App\Models;

use Ppci\Models\PpciModel;

class Personne extends PpciModel
{
    public function __construct()
    {

        $this->table = "personne";

        $this->fields = array(
            "personne_id" => array(
                "type" => 1,
                "key" => 1,
                "requis" => 1,
                "defaultValue" => 0
            ),
            "personne_nom" => array(
                "type" => 0,
                "requis" => 1
            ),
            "personne_prenom" => array(
                "type" => 0
            ),
            "actif" => array("type" => 1)
        );
        parent::__construct();
    }
    /**
     * Retourne l'identifiant du nom ou du prénom fournis
     *
     * @param string $nom
     * @return array
     */
    function getIdByNameSurname($nom)
    {
        if (strlen($nom) > 0) {
            $nom = "%" . $nom . "%";
            $sql = "select personne_id from personne
					where upper (personne_nom) like upper (:n1:)
					or upper (personne_prenom) like upper (:n2:) ";
            return $this->lireParam($sql, ["n1" => $nom, "n2" => $nom]);
        }
    }

    function getListeActif($trait_id = 0)
    {
        if (is_numeric($trait_id)) {
            $sql = "select* from personne
					where actif = 1
					or personne_id in (select personne_id from trait where trait_id = :trait_id:)
					order by personne_nom, personne_prenom";
            return $this->getListeParam($sql, ["trait_id" => $trait_id]);
        }
    }
}
