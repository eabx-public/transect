<?php 
namespace App\Models;

/**
 * @author Eric Quinton
 * @copyright Copyright (c) 2014, IRSTEA / Eric Quinton
 * @license http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html LICENCE DE LOGICIEL LIBRE CeCILL-C
 *  Creation 28 août 2014
 */
require_once ROOTPATH.'/vendor/tecnickcom/tcpdf/tcpdf.php';

class TraitPdf extends \TCPDF {
	public $param = array (
			"orientation" => "P",
			"unit" => "mm",
			"format" => "A4",
			"unicode" => true,
			"encoding" => "UTF-8",
			"nomFichier" => "transect",
			"hl" => 10, // Hauteur de ligne
			"title" => "Transect - données récoltées depuis le bateau",
			"nbTraitByPage" => 2 
	) // nombre de traits par page
;
	public $nbTrait = 0;
	public $nbLigne = 0;
	public $color = array (
			"bleu" => array (
					"R" => 200,
					"G" => 235,
					"B" => 254 
			),
			"olive" => array (
					"R" => 209,
					"G" => 199,
					"B" => 86 
			),
			"orange" => array (
					"R" => 252,
					"G" => 164,
					"B" => 75 
			),
			"jaune" => array (
					"R" => 255,
					"G" => 216,
					"B" => 67 
			),
			"violet" => array (
					"R" => 224,
					"G" => 209,
					"B" => 204 
			),
			"vert" => array (
					"R" => 208,
					"G" => 232,
					"B" => 213 
			) 
	);
	/**
	 * Constructeur de la classe
	 */
	public function __construct($param) {
		if (is_array ( $param )) {
			foreach ( $param as $key => $value ) {
				$this->param [$key] = $value;
			}
		}
		/*
		 * Nettoyage du tampon avant de generer le document
		 */
		ob_clean ();
		parent::__construct();
		$this->entete ();
	}
	/**
	 * Génération du pied de page
	 * (non-PHPdoc)
	 *
	 * @see TCPDF::Footer()
	 */
	function Footer() {
		$this->SetY ( - 15 );
		// Set font
		$this->SetFont ( 'helvetica', 'I', 8 );
		// Page number
		$this->Cell ( 0, 10, 'Page ' . $this->getAliasNumPage () . '/' . $this->getAliasNbPages (), 0, false, 'C', 0, '', 0, false, 'T', 'M' );
	}
	function entete() {
		/*
		 * Definition de la marge
		 */
		$this->SetMargins ( 10, 10, 10 );
		$this->SetFont ( "helvetica" );
		$this->setPrintHeader ( false );
		$this->AddPage ( $this->param ["orientation"] );
		/*
		 * Impression du titre
		 */
		$this->SetFont ( "", "B" );
		$this->SetFontSize ( 10 );
		$this->MultiCell ( 0, 0, $this->param ["title"], 0, "C" );
		$this->SetFont ( "", "I" );
		$this->MultiCell ( 0, $this->param ["hl"], "Généré le " . date ( "d/m/Y H:i" ), 0, "R" );
		$this->SetFont ( "", "" );
	}
	
	/**
	 * Envoie le document au navigateur
	 */
	function sent() {
		$nomFichier = $this->param ["nomFichier"] . "_" . date ( "d-m-Y-Hi" ) . ".pdf";
		$this->Output ( $nomFichier, "I" );
	}
	/**
	 * Genere l'affichage pour les informations spécifiques du trait
	 *
	 * @param array $data        	
	 */
	function setDataTrait($data) {
		if ($this->nbTrait > ($this->param ["nbTraitByPage"] - 1)) {
			$this->AddPage ();
			$this->nbTrait = 0;
		}
		$this->nbTrait ++;
		if ($this->nbTrait > 1) {
			$this->Ln ( $this->param ["hl"] );
		}
		$this->SetFont ( "", "B" );
		$this->Cell ( 95, 0, $data ["site_transect_name"] . " " . $data ["rive_libelle"] );
		$this->SetFont ( "", "" );
		$this->Cell ( 95, 0, "Biologiste : " . $data ["personne_prenom"] . " " . $data ["personne_nom"], 0, 1, "R" );
		$this->MultiCell ( 0, 0, "Trait réalisé le " . $data ["trait_debut"] . " au " . $data ["trait_fin"], 0, "L" );
		$this->MultiCell ( 0, 0, " (en TU : du " . $data ["trait_debut_tu"] . " au " . $data ["trait_fin_tu"] . ")", 0, "L" );
		$this->MultiCell ( 0, 0, "Coefficient de marée : " . $data ["maree_coef"] . " Hauteur d'eau : " . $data ["hauteur_eau"], 0, "L" );
		if (strlen ( $data ["commentaire"] ) > 0)
			$this->MultiCell ( 0, 0, "Commentaires : " . $data ["commentaire"], 0, "L" );
	}
	function setDataPeche($data) {
		/*
		 * Nbre de lignes : 17
		 */
		$att = array (
				"position_engin_name",
				"conductivite",
				"oxygene_ppt",
				"oxygene",
				"temp_eau",
				"turbidite",
				"salinite",
				"ph",
				"couranto_debut",
				"couranto_fin",
				"volume_filtre",
				"vitesse",
				"couranto_sec_debut",
				"couranto_sec_fin",
				"volume_filtre_secours",
				"vitesse_sec",
				"duree_filage",
				"duree_virage",
				"capture_non_conserve" 
		);
		$lib = array (
				"",
				"Conductivité (µs/cm)",
				"Oxygène (ppt)",
				"Oxygène (mg/l)",
				"Température de l'eau (en °C)",
				"Turbidité",
				"Salinité (ppt)",
				"pH",
				"Couranto : valeur début",
				"Valeur fin",
				"Volume filtré (m3)",
				"Vitesse (m/s)",
				"Couranto de secours : valeur début",
				"Valeur fin",
				"Volume filtré (secours)",
				"Vitesse (secours)",
				"Durée de filage",
				"Durée de virage",
				"Captures non conservées" 
		);
		/*
		 * Traitement de chaque ligne
		 */
		for($nl = 0; $nl < 17; $nl ++) {
			switch ($nl) {
				case 0 :
					$align = "C";
					$this->SetFont ( "", "B" );
					break;
				case 16 :
					$align = "L";
					$this->SetFont ( "", "" );
					break;
				default :
					$align = "R";
					$this->SetFont ( "", "" );
			}
			/*
			 * Ecriture du libelle
			 */
			$this->Cell ( 70, 0, $lib [$nl], 1, 0, "R" );
			/*
			 * Ecriture des valeurs
			 */
			for($c = 0; $c < 3; $c ++) {
				// $this->Cell(40,0,$data[$c][$att[$nl]],1,0,$align);
				$this->MultiCell ( 40, 0, $data [$c] [$att [$nl]], 1, $align, 0, 0 );
			}
			$this->Ln ();
		}
		
		/*
		 * Affichage des echantillons
		 */
		for($c = 0; $c < 3; $c ++) {
			if (count ( $data [$c] ["echantillon"] ) > 0) {
				$this->nbTrait ++; // forcage du saut de page
				$this->Cell ( 95, 0, "Échantillons - " . $data [$c] ["position_engin_name"] );
				$this->Ln ();
				/*
				 * Entete
				 */
				$this->Cell ( 110, 0, "", 1, 0, "L" );
				$this->SetFont ( "", "B" );
				$this->Cell ( 40, 0, "Nombre", 1, 0, "C" );
				$this->Cell ( 40, 0, "Masse", 1, 0, "C" );
				$this->SetFont ( "", "" );
				$this->Ln ();
				foreach ( $data [$c] ["echantillon"] as $key => $value ) {
					$this->MultiCell ( 110, 0, $value ["code_csp"] . " " . $value ["nom"] . " " . $value ["type_code"], 1, "L", 0, 0 );
					$this->MultiCell ( 40, 0, $value ["nombre"], 1, "R", 0, 0 );
					$this->MultiCell ( 40, 0, $value ["masse"], 1, "R", 0, 0 );
					$this->Ln ();
				}
			}
		}
	}
}
