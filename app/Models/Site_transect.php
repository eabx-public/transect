<?php

namespace App\Models;

use Ppci\Models\PpciModel;

class Site_transect extends PpciModel
{
    public function __construct()
    {

        $this->table = "site_transect";

        $this->fields = array(
            "site_transect_id" => array(
                "type" => 1,
                "key" => 1,
                "requis" => 1,
                "defaultValue" => 0
            ),
            "site_transect_name" => array(
                "type" => 0,
                "requis" => 1
            )
        );
        parent::__construct();
    }
}
