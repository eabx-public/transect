<?php

namespace App\Models;

use Ppci\Models\PpciModel;

/**
 * ORM de gestion de la table Materiel
 *
 * @author quinton
 *
 */
class Materiel extends PpciModel
{

    /**
     * Constructeur de la classe
     *
     * @param PDO $bdd
     * @param array $param
     */
    public function __construct()
    {

        $this->table = "materiel";

        $this->fields = array(
            "materiel_id" => array(
                "type" => 1,
                "requis" => 1,
                "key" => 1
            ),
            "materiel_type" => array(
                "requis" => 1
            ),
            "materiel_code" => array(
                "requis" => 1
            ),
            "materiel_description" => array(
                "requis" => 1
            ),
            "is_actif" => array(
                "type" => 1,
                "requis" => 1,
                "defaultValue" => 1
            )
        );
        parent::__construct();
    }

    /**
     * Retourne les engins de peche actuellement utilises
     *
     * @param number $order
     * @return array
     */
    function getListeActif($peche_id = 0)
    {
        $sql = "select * from materiel where is_actif = 1
					or materiel_id in
					(select materiel_id from peche where peche_id = :peche_id:)";
        return $this->getListeParam($sql, ["peche_id" => $peche_id]);
    }
}
