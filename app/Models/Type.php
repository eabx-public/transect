<?php

namespace App\Models;

use Ppci\Models\PpciModel;

class Type extends PpciModel
{
    public function __construct()
    {
        $this->table = "type";
        $this->fields = array(
            "type_id" => array(
                "type" => 1,
                "key" => 1,
                "requis" => 1,
                "defaultValue" => 0
            ),
            "type_code" => array(
                "type" => 0,
                "requis" => 1
            ),
            "type_description" => array("type" => 0)
        );
        parent::__construct();
    }
}
