<?php

namespace App\Models;

use Ppci\Libraries\PpciException;
use Ppci\Models\PpciModel;

/**
 * ORM permettant d'afficher facilement les données de la vue v_trait_peche
 *
 * @author quinton
 *
 */
class TraitPeche extends PpciModel
{

    public function __construct()
    {

        $this->table = "v_trait_peche";
        $this->fields = array(
            "peche_id" => array(
                "type" => 1,
                "key" => 1
            ),
            "trait_id" => array(
                "type" => 1
            ),
            "volume_filtre" => array(
                "type" => 1
            ),
            "trait_debut" => array(
                'type' => 3
            ),
            "trait_fin" => array(
                'type' => 3
            ),
            "trait_debut_tu" => array(
                'type' => 3
            ),
            "trait_fin_tu" => array(
                "type" => 3
            ),
            "site_transect_name" => array(
                "type" => 0
            ),
            "station_nom" => array(
                "type" => 0
            ),
            "rive_libelle" => array(
                "type" => 0
            ),
            "position_engin_name" => array(
                "type" => 0
            ),
            "etat_conservation_echan" => array(
                "type" => 1
            ),
            "duree" => array(
                "type" => 0
            ),
            "duree_seconde" => array(
                "type" => 1
            ),
            "profondeur_peche_libelle" => array(
                "type" => 0
            ),
            "conductivite" => array(
                "type" => 1
            ),
            "oxygene" => array(
                "type" => 1
            ),
            "temp_eau" => array(
                "type" => 1
            ),
            "turbidite" => array(
                "type" => 1
            ),
            "salinite" => array(
                "type" => 1
            ),
            "capture_non_conserve" => array(
                "type" => 0
            ),
            "capture_other" => array(
                "type" => 0
            ),
            "commentaire" => array(
                "type" => 0
            ),
            "gelatineux" => array(
                "type" => 1
            ),
            "oxygene_sat" => array(
                "type" => 1
            ),
            "completed" => array(
                "type" => 1
            ),
            "cs_uid" => array(
                "type" => 1
            ),
            "poisson_congele" => array(
                "type" => 1
            ),
            "congelation_ok" => array(
                "type" => 1
            ),
            "uuid" => array(
                "type" => 0
            )
        );
        parent::__construct();
    }

    /**
     * Retourne les derniers traits présents dans la table
     *
     * @param number $limite
     * @return array
     */
    function getLastTrait(int $limite = 50)
    {
        $sql = "select * from trait_peche
				order by trait_debut desc limit " . $limite;
        return ($this->getListeParam($sql));
    }

    /**
     * Function generateCSfile: generate the file for import into collec-science
     *
     * @param mixed $peche_id
     * @param mixed $project_id
     * @param mixed $sample_type_id
     *
     * @return mixed
     */
    function generateCSfile($peche_id, $project_id = 1, $sample_type_id = 11)
    {
        $data = array();
        if ($peche_id > 0) {
            $this->autoFormatDate = false;
            $dpeche = $this->lire($peche_id);
            /*
             * Verification que la saisie soit bien terminee et l'UID de Collec-Science renseigne
             */
            if ($dpeche["cs_uid"] > 0 && $dpeche["completed"] == 1) {
                /*
                 * Lecture des echantillons associes
                 */
                $echan = new Echantillon;
                $echans = $echan->getListFromPeche($peche_id);
                /*
                 * Generation du fichier en sortie
                 */

                $listmois = array(
                    "01" => "JANVIER",
                    "02" => "FEVRIER",
                    "03" => "MARS",
                    "04" => "AVRIL",
                    "05" => "MAI",
                    "06" => "JUIN",
                    "07" => "JUILLET",
                    "08" => "AOUT",
                    "09" => "SEPTEMBRE",
                    "10" => "OCTOBRE",
                    "11" => "NOVEMBRE",
                    "12" => "DECEMBRE"
                );
                $annee = substr($dpeche["trait_debut"], 0, 4);
                $mois = $listmois[substr($dpeche["trait_debut"], 5, 2)];

                $name = $dpeche["site_transect_name"] . "-" . $dpeche["rive_libelle"] . "-" . $dpeche["position_engin_name"] . "-" . $mois . "-" . $annee;
                $name = str_replace(" ", "_", $name);
                /**
                 * Generate record for peche
                 */
                $dbuid_origin = "TRANSECT-PECHE:" . $dpeche["peche_id"];
                $data[] = array(
                    "dbuid_origin" => $dbuid_origin,
                    "identifier" => strtoupper($name),
                    "sample_type_name" => $_SESSION["cs_sample_name_peche"],
                    "collection_name" => $_SESSION["cs_collection_name"],
                    "sampling_date" => $dpeche["trait_debut"],
                    "dbuid_parent" => "",
                    "md_taxon" => "",
                    "uuid" => $dpeche["uuid"],
                );
                foreach ($echans as $ech) {
                    $line = array();
                    /*$line["sample_identifier"] = strtoupper($name . "-" . $ech["nom_fr"]);
                    $line["project_id"] = $project_id;
                    $line["sample_type_id"] = $sample_type_id;
                    $line["sample_status_id"] = 1;
                    $line["sampling_date"] = substr($dpeche["trait_debut"],0, 10);
                    $line["sample_parent_uid"] = $dpeche["cs_uid"];
                    $line["sample_multiple_value"] = $ech["nombre"];
                    $metadata["taxon"] = $ech["nom"];
                    $line["sample_metadata_json"] = json_encode($metadata);
                    $line["sample_uuid"] = $ech["uuid"];*/
                    $line["dbuid_origin"] = "TRANSECT-ECHAN:" . $ech["echantillon_id"];
                    $line["identifier"] = strtoupper($name . "-" . $ech["nom_fr"]);
                    $line["sample_type_name"] = $_SESSION["cs_sample_name_echantillon"];
                    $line["collection_name"] = $_SESSION["cs_collection_name"];
                    $line["sampling_date"] = $dpeche["trait_debut"];
                    $line["dbuid_parent"] = $dbuid_origin;
                    $line["md_taxon"] = $ech["nom"];
                    $line["uuid"] = $ech["uuid"];
                    $data[] = $line;
                }
            } else {
                throw new PpciException("UID Collec-Science non connu ou saisie non terminee");
            }
        } else {
            throw new PpciException("Identifiant de la peche inconnu");
        }
        return $data;
    }
}
