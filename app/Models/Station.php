<?php

namespace App\Models;

use Ppci\Models\PpciModel;

class Station extends PpciModel
{
    public function __construct()
    {

        $this->table = "station";

        $this->fields = array(
            "station_id" => array(
                "type" => 1,
                "key" => 1,
                "requis" => 1,
                "defaultValue" => 0
            ),
            "site_id" => array(
                "type" => 1,
                "requis" => 1,
                "parentAttrib" => 1
            ),
            "profondeur_peche_id" => array(
                "type" => 1,
                "requis" => 1
            ),
            "point" => array(
                "type" => 0
            ),
            "station_nom" => array(
                "type" => 0
            )
        );
        parent::__construct();
    }
    /**
     * Recupere le numero de site a partir du nom de la station
     *
     * @param string $station
     * @return array
     */
    function getSiteIdFromStationName($station)
    {
        if (strlen($station) > 0) {
            $sql = "select site_id from " . $this->table . " where station_nom = :station: ";
            return $this->lireParam($sql, ["station" => $station]);
        }
    }
}
