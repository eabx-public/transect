<?php

namespace App\Models;

use Ppci\Models\PpciModel;


/**
 * @author Eric Quinton
 * @copyright Copyright (c) 2014, IRSTEA / Eric Quinton
 * @license http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html LICENCE DE LOGICIEL LIBRE CeCILL-C
 *  Creation 19 juin 2014
 */
/**
 * ORM de gestion de la table espece_type
 *
 * @author quinton
 *
 */
class EspeceType extends PpciModel
{
	public function __construct()
	{

		$this->table = "espece_type";

		$this->fields = array(
			"espece_type_id" => array(
				"type" => 1,
				"key" => 1,
				"requis" => 1,
				"defaultValue" => 0
			),
			"type_id" => array(
				"type" => 1
			),
			"espece_id" => array(
				"type" => 1,
				"requis" => 1
			),
			"ordre_tri" => array(
				"type" => 1,
				"requis" => 1,
				"defaultValue" => 999
			),
			"code_csp" => array(
				"type" => 0
			),
			"message" => array(
				"type" => 0
			),
			"espece_type_grouping" => array(
				"type" => 1
			)
		);
		parent::__construct();
	}
	/**
	 * Recherche la liste des espèces/types à partir d'une chaîne,
	 * sur le nom latin, le nom français, le code CSP
	 *
	 * @param string $name
	 * @return array
	 */
	function getListByName($name)
	{
		if (strlen($name) > 0) {
			$name = '%' . strtoupper($name) . '%';
			$sql = "select espece_type_id, code_csp, nom, nom_fr, type_code, message
					from espece_type
					join espece using (espece_id)
					left outer join type using (type_id)
					where upper(nom) like :n1:
						or upper(nom_fr) like :n2:
						or upper(code_csp) like :n3:
					order by ordre_tri, nom, type_code";
			return $this->getListeParam($sql, ['n1' => $name, 'n2' => $name, 'n3' => $name]);
		}
	}

	function getListe($order = ""): array
	{
		$sql = "select espece_type_id, code_csp, nom, nom_fr, type_code, message, 
				ordre_tri, espece_type_grouping
				from espece_type
				join espece using (espece_id)
				left outer join type using (type_id)";
		return $this->getListeParam($sql);
	}
}
