<?php 
namespace App\Models;

use Ppci\Libraries\PpciException;
use Ppci\Models\PpciModel;


/**
 * ORM de gestion de la table trait
 * @author quinton
 *
 */
class Trait_op extends PpciModel
{
	/**
	 * Liste des alertes detectees
	 *
	 * @var array
	 */
	public $warning = array();
	/**
	 * Constructeur de la classe
	 *
	 * @param connection $bdd
	 * @param array $param
	 */
	private $sql = "select trait.*, personne_nom, personne_prenom, site_transect_name, rive_libelle,
					bateau_nom, maree_name, protocole_description,
					experimentation_nom,
					campagne.date_debut, campagne_statut_name,
					vpc.peche_nb, vpc.peche_completed, nb_freezing_pending,
					tg.longitude_txt, tg.latitude_txt, tg.long_wgs84, tg.lat_wgs84";
	private $from = " from trait
					join site using (site_id)
					join site_transect using (site_transect_id)
					left outer join campagne using (campagne_id)
					left outer join campagne_statut using (campagne_statut_id)
					left outer join rive using (rive_id)
					left outer join personne using (personne_id)
					left outer join bateau using (bateau_id)
					left outer join maree using (maree_id)
					left outer join protocole using (protocole_id)
					left outer join experimentation e  on (trait.experimentation_id = e.experimentation_id)
					left outer join trait_geom tg using (trait_id)
					left outer join v_peche_completed vpc using (trait_id)
          left outer join v_peche_freezing_pending using (trait_id)
			";
			private $params = [];
	public function __construct()
	{
		
		
		$this->table = "trait";
		
		$this->fields = array(
			"trait_id" => array(
				"type" => 1,
				"key" => 1,
				"requis" => 1,
				"defaultValue" => 0
			),
			"experimentation_id" => array(
				"type" => 1,
				"requis" => 1,
				"defaultValue" => 10
			),
			"personne_id" => array(
				"type" => 1
			),
			"site_id" => array(
				"type" => 1,
				"requis" => 1
			),
			"bateau_id" => array(
				'type' => 1,
				"defaultValue" => 3
			),
			"maree_id" => array(
				'type' => 1
			),
			"jour_id" => array(
				'type' => 1,
				"defaultValue" => 1
			),
			"trait_nom" => array(
				'type' => 0,
				"defaultValue" => "Peche"
			),
			"trait_debut" => array(
				'type' => 3,
				"requis" => 1
			),
			"trait_fin" => array(
				'type' => 3
			),
			"trait_debut_tu" => array(
				'type' => 3
			),
			"trait_fin_tu" => array(
				"type" => 3
			),
			/*				"duree_filage" => array (
												   "type" => 1
										   ),
										   "duree_virage" => array (
												   "type" => 1
										   ),
										   "vitesse" => array (
												   "type" => 1
										   ),*/
			"maree_coef" => array(
				"type" => 1
			),
			"hauteur_eau" => array(
				"type" => 1
			),
			"meduse" => array(
				"type" => 1
			),
			"apim" => array(
				"type" => 0
			),
			"pompe" => array(
				"type" => 0
			),
			"protocole_id" => array(
				"type" => 1,
				"defaultValue" => 11
			),
			"commentaire" => array(
				"type" => 0
			),
			"campagne_id" => array(
				"type" => 1
			)
		);
		parent::__construct();
	}

	/**
	 * Surcharge de la fonction lire pour eclater les zones datetime en plusieurs champs
	 * (non-PHPdoc)
	 *
	 * @see ObjetBDD::lire()
	 */
	function read(int $id, bool $getDefault = false, $parentValue = 0):array
	{
		//$this->fields["date_debut"] = array("type"=>2);
		$data = parent::read($id, $getDefault, $parentValue);
		//unset ($this->fields["date_debut"]);
		$dateTime = explode(" ", $data["trait_debut"]);
		$data["trait_date"] = $dateTime[0];
		$data["trait_debut_time"] = $dateTime[1];
		$dateTime = explode(" ", $data["trait_fin"]);
		$data["trait_fin_time"] = $dateTime[1];
		$dateTime = explode(" ", $data["trait_debut_tu"]);
		$data["trait_debut_tu_time"] = $dateTime[1];
		$dateTime = explode(" ", $data["trait_fin_tu"]);
		$data["trait_fin_tu_time"] = $dateTime[1];
		return $data;
	}

	/**
	 * Surcharge de la fonction ecrire pour reconstituer les champs time
	 * (non-PHPdoc)
	 *
	 * @see ObjetBDD::ecrire()
	 */
	function write($data):int
	{
		$data["trait_debut"] = $data["trait_date"] . " " . $data["trait_debut_time"];
		$data["trait_fin"] = $data["trait_date"] . " " . $data["trait_fin_time"];
		$data["trait_debut_tu"] = $data["trait_date"] . " " . $data["trait_debut_tu_time"];
		$data["trait_fin_tu"] = $data["trait_date"] . " " . $data["trait_fin_tu_time"];

		$id = parent::write($data);
		/*
		 * Mise en table des donnees geographiques
		 */
		if (strlen($data["long_wgs84"]) > 0 && strlen($data["lat_wgs84"]) > 0 && $id > 0) {
			$data["trait_id"] = $id;
			$traitGeom = new Trait_geom;
			$traitGeom->ecrire($data);
		}
		return $id;
	}
	/**
	 * Fonction permettant d'importer des données externes dans la base
	 * (relevés bateau)
	 *
	 * @param array $data
	 * @return int
	 */
	function importData($data)
	{
		$this->warning = array();
		$retour = -1;
		if (is_array($data)) {
			/*
			 * Recherche de la station
			 */
			$station = new Station;
			$res = $station->getSiteIdFromStationName($data["station_nom"]);
			if ($res["site_id"] > 0) {
				$data["site_id"] = $res["site_id"];
				/*
				 * Recherche s'il existe un trait correspondant
				 */
				$dateTU = $this->formatDateLocaleVersDB($data["trait_debut_tu"], 3);
				$sql = "select trait_id from trait where site_id = :site_id:
						 and trait_debut_tu = :date_tu:";
				$res = $this->lireParam($sql, ["site_id"=> $data["site_id"], "date_tu" => $dateTU]);
				if ($res["trait_id"] > 0) {
					$data["trait_id"] = $res["trait_id"];
				} else {
					$data["trait_id"] = 0;
				}
				/*
				 * Recherche du biologiste responsable
				 */
				$personne = new Personne;
				$res = $personne->getIdByNameSurname($data["biologiste"]);
				if ($res["personne_id"] > 0) {
					$data["personne_id"] = $res["personne_id"];
				} else {
					$this->warning[] = "Le biologiste n'a pas été indiqué, ou ne peut être récupéré dans la base. Vérifiez que la zone ad-hoc ne comprend que le nom ou le prénom";
				}
				/*
				 * Recherche des warnings
				 */
				$twarning = array(
					"maree_coef" => array(
						"min" => 20,
						"max" => 130
					),
					"hauteur_eau" => array(
						"min" => 0,
						"max" => 15
					)
				);
				foreach ($twarning as $kwarn => $dwarn) {
					if (is_null($data[$kwarn])) {
						$this->warning[] = $kwarn . " : l'information n'a pas été renseignée";
					} else {
						/*
						 * Recherche des valeurs mini et maxi
						 */
						if ($data[$kwarn] < $dwarn["min"] || $data[$kwarn] > $dwarn["max"]) {
							$this->warning[] = $kwarn . " : la valeur renseignée (" . $data[$kwarn] . ")
									est hors limite (min : " . $dwarn["min"] . ', max :' . $dwarn["max"] . ')';
						}
					}
				}
				/*
				 * Ecriture de l'enregistrement
				 */
				$retour = $this->ecrire($data);
			} else {
				$this->errorData[][0]["message"] = 'La station ' . $data["station_nom"] . " est inconnue";
			}
		} else {
			$this->errorData[][0]["message"] = 'Pas de données à importer';
		}
		return $retour;
	}
	function supprimer($id)
	{
		if ( $id > 0) {
			/*
			 * Recherche s'il existe ou non des operations de peche
			 */
			$sql = "select count(*) as nombre from peche where trait_id = :id:";
			$data = $this->lireParam($sql, ["id"=>$id]);
			if ($data["nombre"] == 0) {
				$traitGeom = new Trait_geom;
				$traitGeom->supprimer($id);
				return parent::supprimer($id);
			} else {
				throw new PpciException("Suppression du trait impossible, il existe des pêches associées");
			}
		}
	}
	/**
	 * Importe les données au format XML (logiciel TransectJ)
	 * Pas de vérifications particulières : elles sont considérées comme déjà faites
	 *
	 * @param array $data
	 * @return int
	 */
	function importXml(array $data)
	{
		$this->warning = array();
		$retour = -1;
		/*
		 * Recherche du site
		 */
		$site = new Site;
		$site_id = $site->getSiteFromSiteTransectAndRive($data["site_transect_id"], $data["rive_id"]);
		if ($site_id > 0) {
			$data["site_id"] = $site_id;
			/*
			 * Recherche du trait pré-existant
			 */
			$sql = "select trait_id from trait where site_id = :site_id:
						 and trait_debut_tu = :trait_debut_tu:";
			$res = $this->lireParamAsPrepared(
				$sql,
				array(
					"site_id" => $data["site_id"],
					"trait_debut_tu" => $this->formatDateTimeLocaleToDB( $data["trait_debut_tu"])
				)
			);
			if ($res["trait_id"] > 0) {
				$data["trait_id"] = $res["trait_id"];
			} else {
				$data["trait_id"] = 0;
			}
			/*
			 * Ecriture de l'enregistrement
			 */
			if (is_array($data["commentaire"])) {
				$data["commentaire"] = implode(" ",$data["commentaire"]);
			}
			$retour = parent::write($data);
		} else {
			$this->errorData[][0]["message"] = "L'identifiant de la station n'a pas été renseigné";
		}
		return $retour;
	}
	/**
	 * Fonction de recherche des traits a partir des parametres
	 *
	 * @param array $dataSearch
	 * @return array
	 */
	function getListeSearch($dataSearch)
	{
		$where = $this->generateWhere($dataSearch);
		if (strlen($where) > 7) {
			$sql = $this->sql . ",nombre ";
			$this->fields["date_debut"] = array("type" => 2);
			$from = $this->from . "
				left outer join v_hydrolab_count using (trait_id)";
			$order = " order by trait_debut_tu desc";
			return $this->getListeParam($sql . $from . $where . $order, $this->params);
		}
	}
	function generateWhere($dataSearch)
	{
		$where = " where ";
		$and = "";
		$this->params = [];
		if ($dataSearch["site_id"] > 0) {
			$where .= $and . " site_id = :site_id:" ;
			$and = " and ";
			$this->params["site_id"]=  $dataSearch["site_id"];
		}
		if ($dataSearch["site_transect_id"] > 0) {
			$where .= $and . " site_transect_id = :site_transect_id:";
			$this->params["site_transect_id"] = $dataSearch["site_transect_id"];
			$and = " and ";
		}
		if ($dataSearch["experimentation_id"] > 0 ) {
			$where .= $and . " trait.experimentation_id = :experimentation_id:";
			$this->params["experimentation_id"] =  $dataSearch["experimentation_id"];
			$and = " and ";
		}
		if ($dataSearch["campagne_id"] > 0 ) {
			$where .= $and . " campagne_id = :campagne_id:";
			$this->params["campagne_id"] = $dataSearch["campagne_id"];
			$and = " and ";
		} else {
			if (!empty($dataSearch["date_debut"]) && !empty($dataSearch["date_fin"])) {
				$this->params["datefrom"]= $this->formatDateLocaleVersDB($dataSearch["date_debut"]);
				$this->params["dateto"] = $this->formatDateLocaleVersDB($dataSearch["date_fin"]);
				$where .= $and . "trait_debut_tu::date between :datefrom: and :dateto:";
				$and = " and ";
			}
		}
		return ($where);
	}
	/**
	 * Retourne la liste des traits pour édition PDF
	 *
	 * @param array $dataSearch
	 * @return array
	 */
	function getListePdf($dataSearch)
	{
		$where = $this->generateWhere($dataSearch);
		if (strlen($where) > 7) {
			$order = " order by site_transect_name, rive_libelle, trait_debut_tu limit 20";
			/*
			 * Ajout d'une limitation sur l'annee
			 */
			$annee = date('Y');
			$where .= " and extract(year from trait_debut_tu) in (" . $annee . "," . ($annee - 1) . ")";
			$this->dateFields[] = "date_debut";
			return $this->getListeParam($this->sql . $this->from . $where . $order, $this->params);
		}
	}

	/**
	 * Retourne le detail d'un trait
	 *
	 * @param int $id
	 * @return array
	 */
	function getDetail($id)
	{
		if ($id > 0 && is_numeric($id)) {
			$this->dateFields[] = "date_debut";
			$where = " where trait_id = :id:";
			return $this->lireParam($this->sql . $this->from . $where, ["id"=>$id]);
		}
	}
}
