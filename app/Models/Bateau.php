<?php

namespace App\Models;

use Ppci\Models\PpciModel;

class Bateau extends PpciModel
{
    /**
     * Constructeur
     *
     * @param ADO $bdd
     * @param array $param
     */
    public function __construct()
    {

        $this->table = "bateau";

        $this->fields = array(
            "bateau_id" => array(
                "type" => 1,
                "key" => 1,
                "requis" => 1,
                "defaultValue" => 0
            ),
            "bateau_nom" => array(
                "type" => 0,
                "requis" => 1
            ),
            "bateau_type" => array(
                "type" => 0
            ),
            "bateau_taille" => array(
                "type" => 1
            ),
            "puissance" => array(
                "type" => 1
            ),
            "materiau" => array(
                "type" => 1
            ),
            "mise_en_service" => array(
                "type" => 2
            )
        );
        parent::__construct();
    }
}
