<?php

namespace App\Models;

use Ppci\Models\PpciModel;

/**
 * ORM of the table motif_annulation
 */
class MotifAnnulation extends PpciModel
{

    public function __construct()
    {
        $this->table = "motif_annulation";
        $this->fields = array(
            "motif_annulation_id" => array("type" => 1, "requis" => 1, "key" => 1, "defaultValue"=>0),
            "motif_annulation_name" => array("requis" => 1)
        );
        parent::__construct();
    }
}
