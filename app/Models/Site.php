<?php

namespace App\Models;

use Ppci\Models\PpciModel;

class Site extends PpciModel
{
    public function __construct()
    {

        $this->table = "site";

        $this->fields = array(
            "site_id" => array(
                "type" => 1,
                "key" => 1,
                "requis" => 1,
                "defaultValue" => 0
            ),
            "cours_eau_id" => array(
                "type" => 1,
                "requis" => 1
            ),
            "rive_id" => array(
                "type" => 1,
                "requis" => 1
            ),
            "site_transect_id" => array(
                "type" => 1,
                "requis" => 1
            ),
            "latitude" => array(
                "type" => 0
            ),
            "longitude" => array(
                "type" => 0
            ),
            "hydro_pk" => array(
                "type" => 1
            )
        );
        parent::__construct();
    }
    /**
     * Retourne la liste des sites dans un format lisible
     *
     * @return array
     */
    function getListeConsult()
    {
        $sql = "select site_id, cours_eau_id, rive_id, site_transect_id,
				latitude, longitude, hydro_pk,
				site_transect_name,
				rive_libelle,
				cours_eau_nom
				from site
				join cours_eau using (cours_eau_id)
				left outer join rive using (rive_id)
				join site_transect using (site_transect_id)
				order by site_transect_name, rive_libelle";
        return $this->getListeParam($sql);
    }
    /**
     * Retourne le numero de site a partir du site Transect et de la rive
     *
     * @param int $siteTransect
     * @param int $rive
     * @return int
     */
    function getSiteFromSiteTransectAndRive($siteTransect, $rive)
    {
        $retour = 0;
        if ($siteTransect > 0 && $rive > 0) {
            $sql = "select site_id from site where site_transect_id = :site_transect: and rive_id =:rive:";
            $rep = $this->lireParam($sql, ["site_transect" => $siteTransect, "rive" => $rive]);
            if ($rep["site_id"] > 0) {
                $retour = $rep["site_id"];
            }
        }
        return $retour;
    }
}
