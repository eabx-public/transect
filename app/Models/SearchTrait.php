<?php

namespace App\Models;

class SearchTrait extends SearchParam
{
    public function __construct()
    {
        $dj = new \DateTime("now");
        $dj->sub(new \DateInterval("P1M"));
        $this->param = array(
            "site_id" => 0,
            "site_transect_id" => 0,
            "experimentation_id" => 0,
            "campagne_id" => 0,
            "date_debut" => $dj->format("d/m/Y"),
            "date_fin" =>  date('d/m/Y')
        );
        parent::__construct();
    }
}
