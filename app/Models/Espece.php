<?php 
namespace App\Models;
use Ppci\Models\PpciModel;

class Espece extends PpciModel
{
    public function __construct()
    {
        $this->table = "espece";
        $this->fields = array(
            "espece_id" => array(
                "type" => 1,
                "key" => 1,
                "requis" => 1,
                "defaultValue" => 0
            ),
            "nom" => array(
                "type" => 0,
                "requis" => 1
            ),
            "nom_fr" => array("type" => 0),
            "auteur" => array("type" => 0),
            "phylum" => array("type" => 0),
            "subphylum" => array("type" => 0),
            "classe" => array("type" => 0),
            "ordre" => array("type" => 0),
            "genre" => array("type" => 0),
            "famille" => array("type" => 0),
            "code_perm_ifremer" => array("type" => 0),
            "code_sandre" => array("type" => 0)
        );
        parent::__construct();
    }
    function getListName($order = "")
    {
        $sql = "select espece_id, nom, nom_fr
                from espece";
        if (!empty($order)) {
            $sql .= " order by $order";
        }
        return $this->getListeParam($sql);
    }
}