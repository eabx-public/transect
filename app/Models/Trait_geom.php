<?php

namespace App\Models;

use Ppci\Models\PpciModel;

class Trait_geom extends PpciModel
{
    public function __construct()
    {

        $this->table = "trait_geom";
        $this->useAutoIncrement = false;
        $this->fields = array(
            "trait_id" => array(
                "type" => 1,
                "key" => 1,
                "requis" => 1
            ),
            "long_wgs84" => array(
                "type" => 1,
                "requis" => 1
            ),
            "lat_wgs84" => array(
                "type" => 1,
                "requis" => 1
            ),
            "longitude_txt" => array(
                "type" => 0
            ),
            "latitude_txt" => array(
                "type" => 0
            )
        );

        parent::__construct();
    }
    /**
     * Surcharge de la fonction ecrire pour generer le point Postgis
     * (non-PHPdoc)
     *
     * @see ObjetBDD::ecrire()
     */
    function write($data): int
    {
        $id = parent::write($data);
        /*
		 * generation du point postgis
		 */
        if ($id > 0) {
            $sql = "update trait_geom set point_geom =
					st_setsrid(st_makepoint( :lon:,:lat:),4326)
					where trait_id = :id:";
            $param = ["id" => $id, "lon" => $data["long_wgs84"], "lat" => $data["lat_wgs84"]];
            $this->executeSQL($sql, $param, true);
        }
        return $id;
    }
}
