<?php

namespace App\Models;

use Ppci\Libraries\PpciException;
use Ppci\Models\PpciModel;

class Echantillon_labo extends PpciModel
{

    public function __construct()
    {


        $this->table = "echantillon_labo";

        $this->fields = array(
            "echantillon_labo_id" => array(
                "type" => 1,
                "key" => 1,
                "requis" => 1,
                "defaultValue" => 0
            ),
            "echantillon_id" => array(
                "type" => 1,
                "requis" => 1,
                "parentAttrib" => 1
            ),
            "labo_nombre" => array(
                "type" => 1,
                "requis" => 1
            ),
            "labo_masse" => array(
                "type" => 1
            ),
            "labo_coef_nombre" => array(
                "type" => 1,
                "requis" => 1
            )
        );
        parent::__construct();
    }

    /**
     * Recupère la liste des individus d'un échantillon
     *
     * @param int $echantillon_id
     * @return array
     */
    function getListFromEchantillon($echantillon_id)
    {
        if ($echantillon_id > 0 && is_numeric($echantillon_id)) {
            $sql = "select * from echantillon_labo
					where echantillon_id = :echantillon_id:
					order by echantillon_labo_id";
            return $this->getListeParam($sql, ["echantillon_id" => $echantillon_id]);
        }
    }

    /**
     * Retourne la liste des analyses pour une pêche donnée
     *
     * @param int $peche_id
     * @return array
     */
    function getListFromPeche($peche_id)
    {
        if ($peche_id > 0 && is_numeric($peche_id)) {
            $sql = "select echantillon_labo.*, type_code, nom, nom_fr, code_csp
					from echantillon_labo
					natural join echantillon
					natural join peche
					natural join espece_type
					left outer join type using (type_id)
					natural join espece
					where peche_id = :peche_id:
					order by ordre_tri, nom_fr, type_code";
            return $this->getListeParam($sql, ["peche_id" => $peche_id]);
        }
    }
}
