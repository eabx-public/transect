<?php

namespace App\Models;

use Ppci\Models\PpciModel;


/**
 * ORM de gestion de la table campagne
 *
 * @author quinton
 *
 */
class Campagne extends PpciModel
{
    private $sql = "select campagne_id, experimentation_id, campagne_statut_id,
                  motif_annulation_id, date_debut, duree, commentaire,
                  campagne_statut_name,
                  experimentation_nom,
                  motif_annulation_name,
                  case when compte_rendu is not null then true else false end as has_compte_rendu
                  from campagne
                  join campagne_statut using (campagne_statut_id)
                  join experimentation using (experimentation_id)
                  left outer join motif_annulation using (motif_annulation_id)
  ";
    public function __construct()
    {
        $this->table = "campagne";
        $this->fields = array(
            "campagne_id" => array(
                "type" => 1,
                "requis" => 1,
                "key" => 1,
                "defaultValue" => 0
            ),
            "experimentation_id" => array(
                "type" => 1,
                "requis" => 1,
                "defaultValue" => 10
            ),
            "campagne_statut_id" => array(
                "requis" => 1,
                "type" => 1,
                "defaultValue" => 1
            ),
            "motif_annulation_id" => array(
                "type" => 1
            ),
            "date_debut" => array(
                "type" => 2,
                "requis" => 1,
                "defaultValue" => date($this->dateFormatMask)
            ),
            "duree" => array(
                "type" => 1,
                "defaultValue" => 2
            ),
            "commentaire" => array(
                "type" => 0
            )
        );
        $this->autoFormatDate = false;
        parent::__construct();
    }

    /**
     * Rewrite for add has_compte_rendu
     *
     * @param int $id
     * @param boolean $default
     * @param integer $parent_id
     * @return array
     */
    public function read(int $id, bool $default = true, $parent_id = 0): array
    {
        if ($id == 0) {
            $data = $this->getDefaultValues();
            $data["has_compte_rendu"] = 0;
        } else {
            $where = " where campagne_id = :campagne_id:";
            $data = $this->lireParamAsPrepared($this->sql . $where, array("campagne_id" => $id));
        }
        $data["date_debut"] = $this->formatDateDBtoLocal($data["date_debut"]);
        return $data;
    }

    /**
     * Surcharge de la fonction ecrire pour integrer
     * l'enregistrement de la pj
     *
     * @param array $data
     * @return null|int
     */
    function write(array $data): int
    {
        $data["date_debut"] = $this->formatDateLocaleToDB($data["date_debut"]);
        $id = parent::write($data);
        if ($id > 0) {
            if ($data["delete_compte_rendu"] == 1) {
                $sql = "update campagne set compte_rendu = null where campagne_id = :id:";
                $this->executeSQL($sql, ["id" => $id], true);
            } else {
                $file = array();
                if (isset($_FILES["upfile"])) {
                    $file = $_FILES["upfile"];
                } else if (isset($_FILES['compte_rendu'])) {
                    $file = $_FILES['compte_rendu'];
                }
                if (!empty($file) && $file['error'] == 0)
                    $dataBinaire = fread(fopen($file["tmp_name"], "r"), $file["size"]);
                $dataDoc = pg_escape_bytea($dataBinaire);
                $sql = "update campagne set compte_rendu = '$dataDoc' where campagne_id = :id:";
                $this->executeSQL($sql, ["id" => $id], true);
            }
        }
        return ($id);
    }
    /**
     * Retourne la liste des campagnes
     *
     * @param integer $exp_id
     * @param integer $limit
     * @return array
     */
    function getLastCampaigns($exp_id = 0, $limit = 10)
    {
        $where = "";
        $param = array();
        if ($exp_id > 0) {
            $where = " where experimentation_id = :exp_id:";
            $param["exp_id"] = $exp_id;
        }
        $order = " order by date_debut desc";
        if (is_numeric($limit) && $limit > 0) {
            $order .= " limit $limit";
        }
        $data =  $this->getListeParamAsPrepared($this->sql . $where . $order, $param);
        foreach ($data as $k=>$v) {
            $data[$k]["date_debut"] = $this->formatDateDBtoLocal($v["date_debut"]);
        }
        return $data;
    }

    function formatDateDBtoLocal(string $value)
    {
        if (!empty($value)) {
            $date = date_create_from_format("Y-m-d", $value);
            if ($date) {
                $value = date_format($date, $this->dateFormatMask);
            }
        }
        return $value;
    }
}
