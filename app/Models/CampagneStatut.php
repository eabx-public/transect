<?php

namespace App\Models;

use Ppci\Models\PpciModel;

/**
 * ORM of the table CampagneStatut
 */
class CampagneStatut extends PpciModel
{

    public function __construct()
    {
        $this->table = "campagne_statut";
        $this->fields = array(
            "campagne_statut_id" => array("type" => 1, "requis" => 1, "key" => 1,"defaultValue"=>0),
            "campagne_statut_name" => array("requis" => 1)
        );
        parent::__construct();
    }
}
