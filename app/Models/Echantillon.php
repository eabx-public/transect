<?php

namespace App\Models;

use Ppci\Libraries\PpciException;
use Ppci\Models\PpciModel;

class Echantillon extends PpciModel
{

    public function __construct()
    {
        $this->table = "echantillon";

        $this->fields = array(
            "echantillon_id" => array(
                "type" => 1,
                "key" => 1,
                "requis" => 1,
                "defaultValue" => 0
            ),
            "peche_id" => array(
                "type" => 1,
                "requis" => 1,
                "parentAttrib" => 1
            ),
            "nombre" => array(
                "type" => 1
            ),
            "masse" => array(
                "type" => 1
            ),
            "abondance" => array(
                "type" => 1
            ),
            "densite" => array(
                "type" => 1
            ),
            "espece_type_id" => array(
                "type" => 1,
                "requis" => 1
            ),
            "individu_masse_totale" => array(
                "type" => 1
            ),
            "individu_nombre_total" => array(
                "type" => 1
            ),
            "labo_nombre" => array(
                "type" => 1
            ),
            "labo_masse" => array(
                "type" => 1
            ),
            "labo_coef_nombre" => array(
                "type" => 1
            ),
            "echantillon_commentaire" => array(
                "type" => 0
            ),
            "uuid" => array(
                "type" => 0
            )
        );
        parent::__construct();
    }
    function getDefaultValues($parentKey = 0): array
    {
        $data = parent::getDefaultValues($parentKey);
        $data["uuid"] = $this->getUUID();
        return $data;
    }

    /**
     * Réécriture de la fonction lire, pour récupérer les noms des espèces/types
     */
    function read(int $id, bool $getDefault = false, $parentValue = 0): array
    {
        if ($id > 0) {
            $sql = "select echantillon.*, nom, nom_fr, code_csp, type_code, message
				from echantillon
				join espece_type using (espece_type_id)
				join espece using (espece_id)
				left outer join type using (type_id)
				where echantillon_id = :id:";
            $data = $this->lireParam($sql, ["id" => $id]);
        } else {
            $data = array();
        }
        if (!$data["echantillon_id"] > 0) {
            /*
             * Generation des valeurs par défaut
             */
            if ($getDefault == true) {
                $data = $this->getDefaultValues($parentValue);
            }
        }
        return $data;
    }

    /**
     * Surcharge de la fonction write, pour mettre en table les individus saisis
     * (non-PHPdoc)
     *
     * @see ObjetBDD::write()
     */
    function write(array $data): int
    {
        $id = parent::write($data);
        if ($id > 0) {
            /*
             * Mise en fichier des individus
             */
            $individu = new Individu;
            /*
             * Recuperation de la liste des individus pre-existants pour suppression eventuelle
             */
            $indold = $individu->getListFromEchantillon($data["echantillon_id"]);
            $indnew = array();
            /*
             * Recuperation des variables
             */
            foreach ($data as $key => $value) {
                if (substr($key, 0, 2) == "im") {
                    /*
                     * recuperation de l'identifiant
                     */
                    $ident = substr($key, 3);
                    /*
                     * Verification qu'il ne s'agit pas d'une ligne vide
                     */
                    if (strlen($value) > 0 || strlen($data["lf_" . $ident]) > 0 || strlen($data["lfbc_" . $ident]) > 0) {
                        $indiv = array();
                        if (substr($ident, 0, 3) == "new") {
                            $indiv["individu_id"] = 0;
                        } else {
                            $indiv["individu_id"] = $ident;
                        }
                        /*
                         * insertion de l'echantillon
                         */
                        $indiv["echantillon_id"] = $id;
                        /*
                         * Recuperation des valeurs
                         */
                        $indiv["longueur_fourche"] = $data["lf_" . $ident];
                        $indiv["longueur_fourche_by_classe"] = $data["lfbc_" . $ident];
                        $indiv["individu_masse"] = $value;
                        /*
                         * Ecriture
                         */
                        $indnew[] = $individu->ecrire($indiv);
                    }
                }
            }
            /*
             * Suppression des individus
             */
            foreach ($indold as $val) {
                if (!in_array($val["individu_id"], $indnew)) {
                    $individu->supprimer($val["individu_id"]);
                }
            }
        }
        return ($id);
    }

    /**
     * Surcharge de la fonction supprimer pour effacer les individus lies
     * (non-PHPdoc)
     *
     * @see ObjetBDD::supprimer()
     */
    function supprimer($id)
    {
        /*
         * Suppression des individus rattaches
         */
        $individu = new Individu;
        $individu->supprimerChamp($id, "echantillon_id");
        return parent::supprimer($id);
    }

    /**
     * Recherche la liste des échantillons pour une opération de pêche
     *
     * @param int $peche_id
     * @return 
     */
    function getListFromPeche($peche_id): ?array
    {
        if ($peche_id > 0) {
            $sql = "select echantillon.*, nom, nom_fr, code_csp, type_code
					from echantillon
					join espece_type using (espece_type_id)
					left outer join type using (type_id)
					join espece using (espece_id)
					where peche_id = :peche_id:
					order by nom_fr, type_code";
            return $this->getListeParam($sql, ["peche_id" => $peche_id]);
        }
    }
    /**
     * Récupère les échantillons d'une pêche, en 
     * regroupant les espèces-types apparentées et en supprimant les accents
     *
     * @param int $peche_id
     * @return array
     */
    function getListGroupedFromPeche(int $peche_id): array
    {
        $sql = "
        with req as (select echantillon_id, peche_id, nom,
        unaccent(nom_fr) as nom_fr, uuid,
        nombre
        /*,case when espece_type_grouping is not null then espece_type_grouping else espece_type_id end as espece_type_id*/
        from echantillon
        join espece_type using (espece_type_id)
        join espece using (espece_id)
        where peche_id = :peche_id:
        ),
        req2 as (
        select  peche_id, nom, nom_fr, /*espece_type_id,*/ sum(nombre) as nombre, min(echantillon_id) as echantillon_id
        from req
        group by  req.peche_id, nom, nom_fr/*, espece_type_id*/
        )
       select req2.peche_id, nom, nom_fr, /*req2.espece_type_id,*/ req2.nombre, echantillon_id, uuid
        from req2 
        join echantillon using(echantillon_id)
        order by req2.nom_fr
        ";
        return $this->getListeParamAsPrepared($sql, array("peche_id" => $peche_id));
    }

    function generateCSfile($id)
    {
        $data = array();
        if (is_numeric($id)) {
            /*
             * Lecture de l'echantillon
             */
            $ech = $this->lire($id);
            /*
             * Lecture des donnees de la peche
             */
            $peche = new TraitPeche;
            $peche->autoFormatDate = false;
            $dpeche = $peche->lire($ech["peche_id"]);

            /*
             * Generation du radical du nom
             */
            $radical = $this->generateCSName($ech);

            /*
             * Lecture des individus associes
             */
            $individu = new Individu;
            $individus = $individu->getListFromParent($id);

            if (count($individus) > 0) {
                /*
                 * Generation du fichier en sortie
                 */
                foreach ($individus as $indiv) {
                    $line = array();
                    $line["dbuid_origin"] = "transect:" . $indiv["individu_id"];

                    $line["identifier"] = strtoupper($radical . "-" . $indiv["individu_id"]);
                    $line["sample_type_name"] = "individu transect";
                    $line["collection_name"] = "Transect-EDF";
                    $metadata = array(
                        "taxon" => $ech["nom"]
                    );
                    if ($ech["echan_cs_uid"] > 0) {
                        $metadata["uid_parent"] = $ech["echan_cs_uid"];
                    }
                    $line["metadata"] = json_encode($metadata);
                    $line["sampling_date"] = $dpeche["trait_debut"];
                    $line["sample_creation_date"] = date("Y-m-d H:i:s");
                    $line["object_status_name"] = "État normal";
                    $data[] = $line;
                }
            } else {
                throw new PpciException("Génération du fichier pour Collab : pas d'individus saisis");
            }
        }

        return $data;
    }

    /**
     * Generation du radical de l'identifiant metier
     *
     * @param array $data
     *            : donnees de l'echantillon
     * @return string
     */
    function generateCSName(array $data)
    {
        $peche = new TraitPeche;
        $dpeche = $peche->lire($data["peche_id"]);
        $listmois = array(
            "01" => "JANVIER",
            "02" => "FEVRIER",
            "03" => "MARS",
            "04" => "AVRIL",
            "05" => "MAI",
            "06" => "JUIN",
            "07" => "JUILLET",
            "08" => "AOUT",
            "09" => "SEPTEMBRE",
            "10" => "OCTOBRE",
            "11" => "NOVEMBRE",
            "12" => "DECEMBRE"
        );
        $annee = substr($dpeche["trait_debut"], 6, 4);
        $mois = $listmois[substr($dpeche["trait_debut"], 3, 2)];

        $name = $dpeche["site_transect_name"] . "-" . $dpeche["rive_libelle"] . "-" . $dpeche["position_engin_name"] . "-" . $mois . "-" . $annee /*. "-" . $data["nom_fr"]*/;
        $name = str_replace(array(" ", "é"), array("_", "e"), $name);
        $name = strtoupper(str_replace(" ", "_", $name));
        return $name;
    }
}
