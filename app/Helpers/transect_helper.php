<?php

use App\Libraries\DefaultLibrary;
use Ppci\Libraries\PpciException;

/**
 * Fonctions specifiques de l'application, chargees systematiquement
 */



/**
 * 
 * @param int $annee
 * @param string $horaire : E/H
 * @return DateTime
 */
function calcul_date_ete_hiver($annee, $horaire = "E" /*"E" : été, "H" : hiver*/)
{
	if ($annee == "") {
        $annee =  date("Y");
    }
	if ("E" == $horaire) {
		$mois = "03";
	} else {
		$mois = "10";
	}
	$date = date_create_from_format('d/m/Y', "31/" . $mois . "/" . $annee);
	$dernier_jour = date_format($date, "w");
	if ($dernier_jour > 0) {
		/*
		 * Calcul du dernier dimanche du mois
		 */
		$date->sub(new DateInterval('P' . $dernier_jour . 'D'));
	}
	//printr(date_format($date, "d/m/Y"));
	return ($date);
}
/**
 * Fonction permettant de vérifier la validité d'un fichier téléchargé
 * @param string $name : nom de la variable téléchargée
 * @param array $extension : liste des extensions autorisées
 * @return string : extension si tout est ok, -1 sinon
 */
function upload_verif($name, $extension)
{
	$import_ok = 0;
	if (strlen($name) > 0 && is_array($extension)) {
		/*
		 * Vérification du fichier téléchargé
		*/
		 $message = service ("Message");
		if (!isset($_FILES[$name]['error']) || is_array($_FILES[$name]['error'])) {
			$message->set("Problème au moment de l'import du fichier", true);
		} else {
			switch ($_FILES[$name]['error']) {
				case UPLOAD_ERR_OK:
					/*
					 * Verification de l'extension
					 */
					$ext = substr(strrchr($_FILES[$name]['name'], '.'), 1);
					foreach ($extension as $value) {
						if ($ext == $value) $import_ok = 1;
					}
					break;
				case UPLOAD_ERR_NO_FILE:
					$message->set("Pas de fichier fourni", true);
				case UPLOAD_ERR_INI_SIZE:
				case UPLOAD_ERR_FORM_SIZE:
					$message->set("Le fichier téléchargé dépasse la taille autorisée", true);
				default:
					$message->set("Erreur d'import du fichier inconnue", true);
			}
		}
	}
	if ($import_ok == 1) return $ext;
	else return -1;
}
/**
 * call a api with curl
 * code from
 * @param string $method
 * @param string $url
 * @param array $data
 * @return
 */
function apiCall($method, $url, $certificate_path = "", $data = array(), $modeDebug = false)
{
  $curl = curl_init();
  if (!$curl) {
    throw new PpciException(_("Impossible d'initialiser le composant curl"));
  }
  switch ($method) {
    case "POST":
      curl_setopt($curl, CURLOPT_POST, true);
      if (!empty($data)) {
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
      }
      break;
    case "PUT":
      curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
      if (!empty($data)) {
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
      }
      break;
    default:
      if (!empty($data)) {
        $url = sprintf("%s?%s", $url, http_build_query($data));
      }
  }
  /**
   * Set options
   */
  curl_setopt($curl, CURLOPT_URL, $url);
  curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
  //curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
  if (!empty($certificate_path)) {
    curl_setopt($curl, CURLOPT_SSLCERT, $certificate_path);
  }
  if ($modeDebug) {
    curl_setopt($curl, CURLOPT_SSL_VERIFYSTATUS, false);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($curl, CURLOPT_VERBOSE, true);
  }
  /**
   * Execute request
   */
  $res = curl_exec($curl);
  if (!$res) {
    throw new PpciException(
      sprintf(
        _("Une erreur est survenue lors de l'exécution de la requête vers le serveur distant. Code d'erreur CURL : %s"),
        curl_error($curl)
      )
    );
  }
  curl_close($curl);
  return $res;
}

/**
 * Fonction permettant de reorganiser les donnees des fichiers telecharges,
 * pour une utilisation directe en tableau
 * @return array
 */
function formatFiles($attributName = "documentName")
{
  global $_FILES;
  $files = array();
  $fdata = $_FILES[$attributName];
  if (is_array($fdata['name'])) {
    for ($i = 0; $i < count($fdata['name']); ++$i) {
      $files[] = array(
        'name' => $fdata['name'][$i],
        'type' => $fdata['type'][$i],
        'tmp_name' => $fdata['tmp_name'][$i],
        'error' => $fdata['error'][$i],
        'size' => $fdata['size'][$i]
      );
    }
  } else
    $files[] = $fdata;
  return $files;
}