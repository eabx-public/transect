<table id="laboList" class="table table-bordered table-hover datatable display">
<thead>
<tr>
<th>Espèce</th>
<th>Type</th>
<th>Nombre</th>
<th>Coef. de comptage</th>
<th>Masse</th>
</tr>
</thead>
<tbody>
{section name=lst loop=$dataLabo}
<tr>
<td>{$dataLabo[lst].code_csp} {$dataLabo[lst].nom}</td>
<td>{$dataLabo[lst].type_code}</td>
<td>{$dataLabo[lst].nombre}</td>
<td>
{if $dataLabo[lst].labo_coef_nombre > 0}
1/{intval(1/$dataLabo[lst].labo_coef_nombre)}
{/if}
</td>
<td>{$dataLabo[lst].masse}</td>
</tr>
{/section}
</tbody>
</table>