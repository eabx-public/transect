<div class="form-display col-md-12">
  <div class="row">
    <div class="col-md-1">Station :</div>
    <div class="col-md-5 display">
      {$dataPeche.station_nom} - {$dataPeche.site_transect_name} {$dataPeche.rive_libelle}
      {$dataPeche.position_engin_name}
    </div>
    <div class="col-md-1">UUID :</div>
    <div class="col-md-3 display">{$dataPeche.uuid}</div>
  </div>
  <div class="row">
    <div class="col-md-3">Date de pêche (TU) :</div>
    <div class="col-md-3 display">{$dataPeche.trait_debut_tu}</div>
    <div class="col-md-1">-></div>
    <div class="col-md-3 display">{$dataPeche.trait_fin_tu}</div>
  </div>
  <div class="row">
    <div class="col-md-2">Volume filtré (m3)</div>
    <div class="col-md-1 display">{$dataPeche.volume_filtre}</div>
    <div class="col-md-2">Conductivité (µS/cm)</div>
    <div class="col-md-1 display">{$dataPeche.conductivite}</div>
    <div class="col-md-2">O2 sat (%)</div>
    <div class="col-md-1 display">{$dataPeche.oxygene_sat}</div>
    <div class="col-md-2">O2 (mg/l)</div>
    <div class="col-md-1 display">{$dataPeche.oxygene}</div>
  </div>
  <div class="row">
    <div class="col-md-2">Température eau (°C)</div>
    <div class="col-md-1  display">{$dataPeche.temp_eau}</div>
    <div class="col-md-2">Turbidité (FTU)</div>
    <div class="col-md-1 display">{$dataPeche.turbidite}</div>
    <div class="col-md-2">Salinité (mg/l)</div>
    <div class="col-md-1 display">{$dataPeche.salinite}</div>
    <div class="col-md-2">UID dans Collab</div>
    <div class="col-md-1 display">{$dataPeche.cs_uid}</div>
  </div>
  {if strlen($dataPeche.capture_non_conserve) > 0}
  <div class="row">
    <div class="col-md-4">Captures non conservées :</div>
    <div class="col-md-7 display">{$dataPeche.capture_non_conserve}</div>
  </div>
  {/if}
  {if strlen($dataPeche.capture_other) > 0 }
  <div class="row">
    <div class="col-md-4">Captures pour autres manips :</div>
    <div class="col-md-7 display"> {$dataPeche.capture_other}</div>
  </div>
  {/if}
  {if strlen($dataPeche.commentaire) > 0}
  <div class="row">
    <div class="col-md-4">Commentaire :</div>
    <div class="col-md-7 textareaDisplay display">{$dataPeche.commentaire}</div>
  </div>
  {/if}
  {if strlen($dataPeche.gelatineux) > 0 || $dataPeche.poisson_congele > 0}
  <div class="row">
    <div class="col-md-2">Gélatineux (en litres) :</div>
    <div class="col-md-3 textareaDisplay display">{$dataPeche.gelatineux}</div>
    <div class="col-md-3">Nombre de poissons congelés à bord :</div>
    <div class="col-md-1 display">{$dataPeche.poisson_congele}</div>
  </div>
  {/if}
</div>