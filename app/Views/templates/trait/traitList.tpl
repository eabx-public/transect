<h2>Liste des traits</h2>
<div class="row">
  {include file="trait/traitSearch.tpl"}
</div>
{if $isSearch == 1}
{if $rights.import == 1}

<a href="traitImportXml">
  <img src="display/images/xml.png" height="30">
  Import d'un fichier XML (export depuis le logiciel TransectJ - saisie bateau)
</a>
{/if}

{if $module == "importXml"}
<div class="row">
  <h2>Import d'un fichier XML généré à partir du logiciel TransectJ (saisie bateau)</h2>
  <div class="col-md-8">
    <form method="post" action="importXmlImport" enctype="multipart/form-data" class="form-horizontal">
      <div class="row">
        <label for="campagne_id_xml" class="col-md-4 control-label">Campagne de pêche :</label>
        <div class="col-md-8">
          <select name="campagne_id" id="campagne_id_xml" class="form-control" readonly>
            {foreach $campagnes as $campagne}
            <option value="{$campagne.campagne_id}" {if $dataSearch.campagne_id==$campagne.campagne_id}selected{/if}>
              {$campagne.date_debut} - <i>{$campagne.campagne_statut_name}</i> ({$campagne.experimentation_nom})
            </option>
            {/foreach}
          </select>
        </div>
      </div>
      <div class="row">
        <label for="upfile" class="col-md-4 control-label">Nom du fichier à importer (xml) :</label>
        <div class="col-md-8">
          <input type="file" id="upfile" class="form-control" name="upfile" required>
        </div>
      </div>
      <div class="row">
        <div class="center">
          <button type="submit" class="btn btn-success col-md-2 col-md-offset-4">Lancer l'import</button>
        </div>
      </div>
      {$csrf}
    </form>
  </div>
</div>
{/if}

  {if $module == "importSonde" }
  <div class="row">
    <h2>Import d'un fichier de sonde "hydrolab"</h2>
    <div class="col-md-8">
      <form method="post" action="importSondeHydrolab" enctype="multipart/form-data" class="form-horizontal">
        <input type="hidden" name="trait_id" value="{$trait_id}">
        <div class="row">
          <label for="upfile" class="col-md-4 control-label">Nom du fichier à importer (xls ou xlsx) :</label>
          <div class="col-md-8">
            <input type="file" id="upfile" class="form-control" name="upfile">
          </div>
        </div>
        <div class="row">
          <div class="center">
            <button type="submit" class="btn btn-success col-md-2 col-md-offset-4">Lancer l'import</button>
          </div>
        </div>
        {$csrf}
      </form>
    </div>
  </div>
  {/if}

  {if $rights.manage == 1}
  <br>
  <a href="traitChange?trait_id=0">
    <img src="display/images/new.png" height="30">Nouveau trait...
  </a>
  {/if}

  {if !empty($warnings)}
  <h3>Erreurs rencontrées lors de l'importation</h3>
  <table id="warnings" class="table table-bordered table-hover datatable-export display">
    <thead>
      <th>N° du trait</th>
      <th>Date</th>
      <th>Site</th>
      <th>Rive</th>
      <th>Position</th>
      <th>Erreur(s) rencontrée(s)</th>
    </thead>
    <tbody>
      {foreach $warnings as $warning}
      <tr>
        <td>{$warning.trait_id}</td>
        <td>{$warning.date}</td>
        <td>{$warning.site}</td>
        <td>{$warning.rive}</td>
        <td>{$warning.position}</td>
        <td>{$warning.message}</td>
      </tr>
      {/foreach}
    </tbody>
  </table>
  {/if}
  <table id="traitListe" class="table table-bordered table-hover datatable-nopaging display">
    <thead>
      <tr>
        <th>Détail</th>
        <th>Pêches<br>associées</th>
        <th>Nb de saisies<br>non terminées</th>
        <th>Nb de congélations<br>non terminées</th>
        <th>Expérimentation</th>
        <th>Site</th>
        <th>Date début<br>(TU)</th>
        <th>Date fin<br>(TU)</th>
        <th>Commentaires</th>
        {if $rights.import == 1}
        <th>Import<br>données sonde</th>
        {/if}
      </tr>
    </thead>
    <tbody>
      {section name=lst loop=$data}
      <tr>
        <td class="center">
          <a href="traitDisplay?trait_id={$data[lst].trait_id}">
            <img src="display/images/ship-icon.png" height="30">
            ({$data[lst].trait_id})
          </a>
        </td>
        <td style="text-align:center;background-color:{if $trait_id==$data[lst].trait_id}green{else}white{/if};">
          <a href="traitPecheList?trait_id={$data[lst].trait_id}">
            <img src="display/images/detail.png" height="30">
          </a>
        </td>
        {$nbreste = $data[lst].peche_nb - $data[lst].peche_completed}
        <td class="center {if $nbreste == 0}green{elseif $nbreste < 3}blue{else}red{/if}">
          {$data[lst].peche_nb - $data[lst].peche_completed}
        </td>
        <td class="center red">{$data[lst].nb_freezing_pending}</td>
        <td>{$data[lst].experimentation_nom}</td>
        <td>{$data[lst].site_transect_name} {$data[lst].rive_libelle}</td>
        <td>{$data[lst].trait_debut_tu}</td>
        <td>{$data[lst].trait_fin_tu}</td>
        <td>{$data[lst].commentaire}</td>
        {if $rights.import == 1}
        <td class="center {if $data[lst].nombre > 0} background-green{/if}">
          <a href="traitImportSonde?trait_id={$data[lst].trait_id}">
            <img src="display/images/hydrolab.png" height="30" 
            {if $data[lst].nombre> 0} title="Données déjà importées"{/if}>
          </a>
        </td>
        {/if}
      </tr>
      {/section}
    </tbody>
  </table>
  <br>
  {/if}
  {if $module == "pecheList"}
  {include file="trait/pecheList.tpl"}
  {/if}
</div>
