<script>
	$(document).ready(function () {
		var numligne = 0;
		$(".num5").attr({
			pattern: "[0-9]+(\.[0-9]+)?",
			title: "Donnée numérique"
		});
		$(".naviguer").attr({
			autocomplete: 'OFF'
		});
		$(".nombre,.masse,.indmasse").focus(function () {
			//store old value
			$(this).data('oldValue', $(this).val());
		});
		$(".nombre").change(function () {
			var oldValue = $(this).data('oldValue');
			if (isNaN(oldValue)) oldValue = 0;
			if (oldValue == "") oldValue = 0;
			var newValue = $(this).val();
			if (isNaN(newValue)) newValue = 0;
			if (newValue == "") newValue = 0;
			var nomChamp = "#nombre";
			var total = $(nomChamp).val();
			if (isNaN(total)) total = 0;
			if (total == "") total = 0;
			/*Recuperation du coef de calcul*/
			var coef = $("#labo_coef_nombre").val();
			if (isNaN(coef)) coef = 1;
			if (coef == "") coef = 1;
			if ($(this).prop("name") == "labo_nombre") {
				total = parseInt(total) - ((parseInt(oldValue) - parseInt(newValue)) * parseInt(coef));
			} else {
				total = parseInt(total) - parseInt(oldValue) + parseInt(newValue);
			}
			$(nomChamp).val(total);
			/*Calcul de l'abondance*/
			var volumeFiltre = $("#volume_filtre").val();
			if (isNaN(volumeFiltre)) volumeFiltre = 0;
			if (volumeFiltre == "") volumeFiltre = 0;
			if (volumeFiltre > 0) $("#abondance").val(parseFloat(total) / (parseFloat(volumeFiltre) / 1000));
		});
		$(".masse").change(function () {
			var oldValue = $(this).data('oldValue');
			if (isNaN(oldValue)) oldValue = 0;
			if (oldValue == "") oldValue = 0;
			var newValue = $(this).val();
			if (isNaN(newValue)) newValue = 0;
			if (newValue == "") newValue = 0;
			var nomChamp = "#masse";
			var total = $(nomChamp).val();
			if (isNaN(total)) total = 0;
			if (total == "") total = 0;
			/*Recuperation du coef de calcul*/
			var coef = $("#labo_coef_nombre").val();
			if (isNaN(coef)) coef = 1;
			if (coef == "") coef = 1;
			if ($(this).prop("name") == "labo_masse") {
				total = parseFloat(total) - ((parseFloat(oldValue) - parseFloat(newValue)) * parseInt(coef));
			} else {
				total = parseFloat(total) - parseFloat(oldValue) + parseFloat(newValue);
			}
			$(nomChamp).val(total);
			/*Calcul de la densite*/
			var volumeFiltre = $("#volume_filtre").val();
			if (isNaN(volumeFiltre)) volumeFiltre = 0;
			if (volumeFiltre == "") volumeFiltre = 0;
			if (volumeFiltre > 0) $("#densite").val(parseFloat(total) / (parseFloat(volumeFiltre) / 1000));
		});
		/*Recalcul en fonction du coef d'échantillonnage*/
		$("#labo_coef_nombre").change(function () {
			/*Recuperation du coef de calcul*/
			var coef = $("#labo_coef_nombre").val();
			if (isNaN(coef)) coef = 1;
			if (coef == "") coef = 1;
			/*Recalcul de la masse*/
			var laboMasse = $("#labo_masse").val();
			if (isNaN(laboMasse)) laboMasse = 0;
			if (laboMasse == "") laboMasse = 0;
			var individuMasse = $("#individu_masse_total").val();
			if (isNaN(individuMasse)) individuMasse = 0;
			if (individuMasse == "") individuMasse = 0;
			var masse = parseFloat(laboMasse) * parseFloat(coef) + parseFloat(individuMasse);
			$("#masse").val(masse);
			/*Calcul de la densite*/
			var volumeFiltre = $("#volume_filtre").val();
			if (isNaN(volumeFiltre)) volumeFiltre = 0;
			if (volumeFiltre == "") volumeFiltre = 0;
			if (volumeFiltre > 0) $("#densite").val(parseFloat(masse) / (parseFloat(volumeFiltre) / 1000));
			/*Recalcul du nombre*/
			var laboNombre = $("#labo_nombre").val();
			if (isNaN(laboNombre)) laboNombre = 0;
			if (laboNombre == "") laboNombre = 0;
			var individuNombre = $("#individu_nombre_total").val();
			if (isNaN(individuNombre)) individuNombre = 0;
			if (individuNombre == "") individuNombre = 0;
			var nombre = parseInt(laboNombre) * parseInt(coef) + parseInt(individuNombre);
			$("#nombre").val(nombre);
			/*Calcul de l'abondance*/
			if (volumeFiltre > 0) $("#abondance").val(parseFloat(nombre) / (parseFloat(volumeFiltre) / 1000));
		});

		$("#recherche").keyup(function () {
			/*
			* Traitement de la recherche d'une espèce/type
			*/
			var texte = $(this).val();
			console.log("recherche de " + texte);
			if (texte.length > 2) {
				/*
				* declenchement de la recherche
				*/
				var url = "especeTypeSearchAjax";
				$.getJSON(url, { "libelle": texte }, function (data) {
					var options = '';
					for (var i = 0; i < data.length; i++) {
						options += '<option value="' + data[i].id + '">' + data[i].val + '</option>';
					};
					$("#espece_type_id").html(options);
				});
			};
		});

		/*
		 * Navigation entre champs avec les flèches
		 * flèche haute : 38, flèche basse : 40, page up : 33, page down : 34
		 */
		$(".naviguer").keypress(function (e) {
			var code = (e.keyCode ? e.keyCode : e.which);
			if (code == 33 || code == 34 || code == 38 || code == 40) {
				var field = $(this).attr("name");
				var prec = "";
				var suiv = "";
				switch (field) {
					case "labo_nombre":
						prec = "echantillon_commentaire";
						suiv = "labo_masse";
						break;
					case "labo_masse":
						prec = "labo_nombre";
						suiv = "individu_nombre_total";
						break;
					case "individu_nombre_total":
						prec = "labo_masse";
						suiv = "individu_masse_total";
						break;
					case "individu_masse_total":
						prec = "individu_nombre_total";
						suiv = "echantillon_commentaire";
						break;
					case "echantillon_commentaire":
						prec = "individu_masse_total";
						suiv = "labo_nombre";
						break;
				}
				if (code == 34 || code == 40) {
					$("#" + suiv).focus();
				} else {
					$('#' + prec).focus();
				}
			}
		});
		/*
		 * Rajout d'une ligne dans le tableau des individus
		 */
		$("#ajouterIndiv").click(function (e) {
			numligne++;
			var ligne = '<tr id="indiv' + numligne + '"><td style="text-align: center;"></td>';
			ligne += '<td style="text-align: center;">';
			ligne += '<input id="indmasse' + numligne + '" class="num5" name="im_new' + numligne + '">';
			ligne += '</td><td style="text-align: center;">';
			ligne += '<input id="indlf' + numligne + '" class="num5" name="lf_new' + numligne + '">';
			ligne += '</td><td style="text-align: center;">';
			ligne += '<input id="indlfclasse' + numligne + '" class="num5" name="lfbc_new' + numligne + '"></td>';
			ligne += '<td class="center"><img id="deleteIndiv' + numligne + '" data-ligne="' + numligne + '" src="display/images/remove-red-24.png" height="25"></td>';
			ligne += '</tr>';
			$("#indiv").last().append(ligne);
			/*
			 * ajout de l'evenement click pour le bouton de suppression
			 */
			$("#deleteIndiv" + numligne).bind('click', function () {
				$("#indiv" + $(this).data("ligne")).remove();
			});

			/*
			 * Rajoute 1 au nombre de poissons captures sur le bateau
	
			var individuNombre = $("#individu_nombre_total").val();
			if (isNaN(individuNombre)) individuNombre = 0;
			if (individuNombre == "") individuNombre = 0;
			individuNombre ++;
			$("#individu_nombre_total").val(individuNombre);
			*/
		});
		/*
		 * Suppression des individus deja existants au chargement de la page
		 */
		$(".removeIndiv").click(function () {
			$("#indiv" + $(this).data("ligne")).remove();
		});



		/*
		 * Verification de la saisie de l'espece avant envoi du formulaire
		 */
		$("#echantillonForm").submit(function (event) {
			var espece = $("#espece_type_id").val();
			if (!espece > 0) {
				event.preventDefault();
			}
		});

		/*
		 * Recalcule la masse des poissons mesures sur le bateau
		 */
		/*	$(".indmasse").change(function() {
				var individuMasse = $("#individu_masse_total").val();
				if (isNaN(individuMasse)) individuMasse = 0;
				if (individuMasse == "") individuMasse = 0;
				var oldValue = $(this).data('oldValue');
				if (isNaN(oldValue)) oldValue = 0 ;
				if (oldValue == "") oldValue = 0;
				var newValue = $(this).val();
				if (isNaN(newValue)) newValue = 0;
				if (newValue == "") newValue = 0 ;
				var masse = parseFloat(individuMasse) - parseFloat(oldValue) + parseFloat(newValue);
				$("#individu_masse_total").val(masse);
			} ) ;*/
	});
</script>

<fieldset>
	<legend>{if $data.echantillon_id == 0}Création d'un échantillon{else}Modification de l'échantillon n°
		{$data.echantillon_id}{/if}
	</legend>
	<div class="row">
		<div class="col-md-12">
			<form method="post" id="echantillonForm" action="echantillonWrite" class="form-horizontal protoform">
				<input type="hidden" name="echantillon_id" value="{$data.echantillon_id}">
				<input type="hidden" name="peche_id" value="{$data.peche_id}">
				<input type="hidden" id="volume_filtre" value="{$data.volume_filtre}">
				<input type="hidden" name="moduleBase" value="echantillon">

				<div class="form-group">
					<label for="recherche" class="control-label col-md-4">Espèce / type :</label>
					<div class="col-md-8">
						<input class="text10" id="recherche" autocomplete="off" autofocus
							placeholder="espèce à chercher" title="Tapez au moins 3 caractères...">
						<select name="espece_type_id" id="espece_type_id">
							{if $data.espece_type_id > 0}
							<option value="{$data.espece_type_id}" selected>{$data.nom} {$data.code_csp}
								{$data.type_code} - {$data.message}
								{/if}
							</option>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label for="labo_coef_nombre" class="control-label col-md-4">Labo - coefficient de comptage
						:</label>
					<div class="col-md-8">
						<select name="labo_coef_nombre" id="labo_coef_nombre">
							<option value="1" {if $data.labo_coef_nombre==1 || $data.labo_coef_nombre==""
								}selected{/if}>1/1</option>
							<option value="2" {if $data.labo_coef_nombre==2}selected{/if}>1/2</option>
							<option value="3" {if $data.labo_coef_nombre==3}selected{/if}>1/3</option>
							<option value="4" {if $data.labo_coef_nombre==4}selected{/if}>1/4</option>
							<option value="6" {if $data.labo_coef_nombre==6}selected{/if}>1/6</option>
							<option value="8" {if $data.labo_coef_nombre==8}selected{/if}>1/8</option>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label for="labo_nombre" class="control-label col-md-4"><span class="red">Labo</span> - nombre /
						masse :</label>
					<div class="col-md-8">
						<input class="num5 nombre naviguer" name="labo_nombre" id="labo_nombre"
							value="{$data.labo_nombre}" title="nombre" placeholder="nb">
						<input class="num5 masse naviguer" name="labo_masse" id="labo_masse" value="{$data.labo_masse}"
							title="masse" placeholder="pds">
					</div>
				</div>
				<div class="form-group">
					<label for="individu_nombre_total" class="control-label col-md-4"><span class="red">Bateau (poissons
							non conservés)</span> - nombre / masse totale :</label>
					<div class="col-md-8">
						<input class="num5 nombre naviguer" name="individu_nombre_total" id="individu_nombre_total"
							value="{$data.individu_nombre_total}" title="nombre" placeholder="nb">
						<input class="num5 masse naviguer" name="individu_masse_totale" id="individu_masse_totale"
							value="{$data.individu_masse_totale}" title="masse" placeholder="pds">
					</div>
				</div>
				<div class="form-group">
					<label for="echantillon_commentaire" class="control-label col-md-4">Commentaires :</label>
					<div class="col-md-8">
						<input class="textlarge naviguer" name="echantillon_commentaire" id="echantillon_commentaire"
							title="Commentaires généraux sur l'échantillon" value="{$data.echantillon_commentaire}">
					</div>
				</div>
				<div class="form-group">
					<label for="echan_cs_uid" class="control-label col-md-4">UID de l'échantillon dans <a
							href="https://collab.eabx.inrae.fr" target="_blank">Collab</a> :</label>
					<div class="col-md-8">
						<input class="nombre" name="echan_cs_uid" id="echan_cs_uid" title="UID de Collab - nombre"
							value="{$data.echan_cs_uid}">
					</div>
				</div>
				<div class="form-group">
					<label for="echan_cs_uid" class="control-label col-md-4">UUID de l'échantillon :</label>
					<div class="col-md-8">
						<input class="form-control" name="uuid" id="uuid" value="{$data.uuid}" readonly>
					</div>
				</div>
				<fieldset>
					<legend>Totaux</legend>
					<div class="form-group">
						<label for="nombre" class="control-label col-md-4">Nombre / masse :</label>
						<div class="col-md-8">
							<input id="nombre" name="nombre" class="num5" readonly value="{$data.nombre}"
								title="nombre">
							<input id="masse" name="masse" class="num5" readonly value="{$data.masse}" title="masse">
						</div>
					</div>
					<div class="form-group">
						<label for="abondance" class="control-label col-md-4">Abondance / densité :</label>
						<div class="col-md-8">
							<input id="abondance" name="abondance" class="num5" readonly value="{$data.abondance}"
								title="abondance">
							<input id="densite" name="densite" class="num5" readonly value="{$data.densite}"
								title="densité">
						</div>
				</fieldset>
				<fieldset>
					<legend>Individus</legend>
					<table class="tablemulticolonneblue">
						<tr>
							<td>
								<table id="indiv" class="table table-bordered table-hover">
									<thead>
										<tr>
											<th>Id</th>
											<th>Masse (g)</th>
											<th>Longueur fourche (mm)</th>
											<th>Longueur fourche (classe)</th>
											<th class="center"><img src="display/images/remove-red-24.png" height="25">
											</th>
										</tr>
									</thead>
									<tbody>
										{section name=lst loop=$individus}
										<tr id="indiv{$individus[lst].individu_id}">
											<td class="center">{$individus[lst].individu_id}</td>
											<td style="text-align: center;">
												<input id="indmasse" name="im_{$individus[lst].individu_id}"
													class="num5 indmasse" value="{$individus[lst].individu_masse}">
											</td>
											<td style="text-align: center;">
												<input id="indlf" name="lf_{$individus[lst].individu_id}" class="num5"
													value="{$individus[lst].longueur_fourche}">
											</td>
											<td style="text-align: center;">
												<input id="indlfclasse" name="lfbc_{$individus[lst].individu_id}"
													class="num5" value="{$individus[lst].longueur_fourche_by_classe}">
											</td>
											<td class="center"><img class="removeIndiv"
													src="display/images/remove-red-24.png" height="25"
													data-ligne="{$individus[lst].individu_id}">
										</tr>
										{/section}
									</tbody>
								</table>
							</td>
							<td>
								<input type="button" class="btn btn-info" value="Ajouter" id="ajouterIndiv">
							</td>
						</tr>
					</table>
				</fieldset>
				<div class="form-group center">
					<button type="submit" class="btn btn-primary button-valid">Valider</button>
					{if $data.echantillon_id>0 && $rights["labo"] == 1 }
					<button class="btn btn-danger button-delete">Supprimer</button>
					{/if}
				</div>
			{$csrf}</form>
		</div>
	</div>
</fieldset>