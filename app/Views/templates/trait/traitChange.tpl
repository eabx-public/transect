<script>
function convertGPStoDD(valeur) { 
	var parts = valeur.split(/[^\d]+/);
	var dd = parseFloat(parts[0]) + parseFloat((parts[1]+"."+parts[2])/60);
	//dd = parseFloat(dd);
	var lastChar = valeur.substr(-1);
	dd = Math.round(dd * 1000000) / 1000000;
	if (lastChar == "S" || lastChar == "W" || lastChar == "O") {  
		dd *= -1;
	};
	return dd;
}

/**
 * Fonction formatant sur deux chiffres les heures/minutes/secondes
 */
function formatTimeFromNumber(valeur) { 
	var val = valeur.toString();
	var retour;
	if (val.length == 1) {
		retour = "0" + val;
	} else {
		if (val.length == 2) {
			retour = val;
		} else {
			retour = "00";
		}
	}
	return retour;
}

/*
 * Fonction de transformation de l'heure en UTC
 */
function convertDateLocaleToUtc(heure) {
	var dateLocale = $("#trait_date").val();
	/*
	 * reformatage de la date
	 */
	 var a_date = dateLocale.split("/");
	 var a_heure = heure.split(":");
	 var o_date =  new Date(a_date[2],a_date[1],a_date[0], a_heure[0], a_heure[1], a_heure[2]);
	 //var heure = dateutc.getHours();
	 
	 //alert(o_date);
	 var heureGmt = o_date.getUTCHours();
	 //var minute = o_date.getUTCMinutes();
	// var seconde = o_date.getUTCSeconds();
	 var heureUTC = formatTimeFromNumber(heureGmt) + ":" + a_heure[1] +":" + a_heure[2];
	 return heureUTC;
}
$(document).ready(function() {

	$("#longitude_txt").change(function () {  
		$('#long_wgs84').val ( convertGPStoDD($(this).val()));
	});
	$("#latitude_txt").change(function () {  
		$('#lat_wgs84').val ( convertGPStoDD($(this).val()));
	});
	$("#trait_debut_time").change(function() {
		$('#trait_debut_tu_time').val( convertDateLocaleToUtc($(this).val()));
	});
	$("#trait_fin_time").change(function() {
		$('#trait_fin_tu_time').val( convertDateLocaleToUtc($(this).val()));
	});
	
});
</script>
<a href="traitList"><img src="display/images/list.png" height="30">Retour à la liste</a>

<div class="row">
<div class="col-md-6">
<fieldset>
<legend>
{if $data.trait_id > 0}
<a href="traitDisplay?trait_id={$data.trait_id}">
<img src="display/images/ship-icon.png" height="30">Retour au trait n° {$data.trait_id}
</a>
{else}
Nouveau trait...
{/if}
</legend>
<div>

<form method="post" class="form-horizontal protoform" action="traitWrite">
<input type="hidden" name="trait_id" value="{$data.trait_id}">
<input type="hidden" name="moduleBase" value="trait">

<div class="form-group">
<label for="" class="control-label col-md-4">Date <span class="red">*</span> :</label>
<div class="col-md-8"><input class="datepicker form-control" id="trait_date" name="trait_date" value="{$data.trait_date}" required> </div>
</div>
<div class="form-group"><label for="trait_debut_time" class="control-label col-md-4">Heures de début et de fin :</label>
<div class="col-md-8"><input class="timepicker col-md-4" id="trait_debut_time" name="trait_debut_time" required value="{$data.trait_debut_time}">
<label for="trait_fin_time" class="control-label col-md-1">à </label>
<input class="timepicker col-md-4" id="trait_fin_time" name="trait_fin_time" value="{$data.trait_fin_time}">
</div>
</div>
<div class="form-group"><label for="trait_debut_tu_time" class="control-label col-md-4">Heures de début et de fin (en TU) :</label>
<div class="col-md-8"><input class="timepicker col-md-4" id="trait_debut_tu_time" name="trait_debut_tu_time" value="{$data.trait_debut_tu_time}">
<label for="trait_fin_tu_time" class="control-label col-md-1"> à </label>
<input class="timepicker col-md-4" id="trait_fin_tu_time" name="trait_fin_tu_time" value="{$data.trait_fin_tu_time}">
</div>
</div>
<div class="form-group">
<label for="site_id" class="control-label col-md-4">Site<span class="red">*</span> :</label>
<div class="col-md-8">
<select id="site_id" name="site_id" class="form-control">
{section name=lst loop=$site}
{strip}
<option value="{$site[lst].site_id}" {if $site[lst].site_id == $data.site_id}selected{/if}>
{$site[lst].cours_eau_nom} {$site[lst].site_transect_name} {$site[lst].rive_libelle}
</option>
{/strip}
{/section}
</select>
</div>
</div>
<div class="form-group"><label for="experimentation_id" class="control-label col-md-4">Expérimentation<span class="red">*</span> :</label>
<div class="col-md-8">
<select id="experimentation_id" name="experimentation_id" class="form-control">
{section name=lst loop=$experimentation}
{strip}
<option value="{$experimentation[lst].experimentation_id}" 
{if $experimentation[lst].experimentation_id == $data.experimentation_id}selected{/if}>
{$experimentation[lst].experimentation_nom}
</option>
{/strip}
{/section}
</select>
</div>
</div>
<div class="form-group">
	<label for="campagne_id" class="control-label col-md-4">Campagne de pêche :</label>
	<div class="col-md-8">
		<select id="campagne_id" name="campagne_id" class="form-control">
			<option value="" {if $data.campagne_id == ""}selected{/if}>Sélectionnez...</option>
			{foreach $campagnes as $campagne}
			<option value="{$campagne.campagne_id}" {if $data.campagne_id == $campagne.campagne_id}selected{/if}>{$campagne.date_debut} {$campagne.campagne_statut_name}</option>
			{/foreach}
		</select>
	</div>
</div>
<div class="form-group"><label for="protocole_id" class="control-label col-md-4">Protocole<span class="red">*</span> :</label>
<div class="col-md-8">
<select id="protocole_id" name="protocole_id" class="form-control">
{section name=lst loop=$protocole}
{strip}
<option value="{$protocole[lst].protocole_id}" 
{if $protocole[lst].protocole_id == $data.protocole_id}selected{/if}>
{$protocole[lst].protocole_id} {$protocole[lst].protocole_description}
</option>
{/strip}
{/section}
</select>
</div>
</div>
<div class="form-group"><label for="personne_id" class="control-label col-md-4">Responsable de la pêche :</label>
<div class="col-md-8">
<select id="personne_id" name="personne_id" class="form-control">
<option value="" {if $data.personne_id == ""} selected{/if}></option>
{section name=lst loop=$personne}
{strip}
<option value="{$personne[lst].personne_id}" 
{if $personne[lst].personne_id == $data.personne_id}selected{/if}>
{$personne[lst].personne_prenom} {$personne[lst].personne_nom}
</option>
{/strip}
{/section}
</select>
</div>
</div>

<div class="form-group"><label for="bateau_id" class="control-label col-md-4">Bateau<span class="red">*</span> :</label>
<div class="col-md-8">
<select id="bateau_id" name="bateau_id" class="form-control">
{section name=lst loop=$bateau}
{strip}
<option value="{$bateau[lst].bateau_id}" 
{if $bateau[lst].bateau_id == $data.bateau_id}selected{/if}>
{$bateau[lst].bateau_nom}
</option>
{/strip}
{/section}
</select>
</div>
</div>
<div class="form-group"><label for="maree_coef" class="control-label col-md-4">Coefficient de marée :</label>
<div class="col-md-8"><input class="nombre form-control" id="maree_coef" name="maree_coef" value="{$data.maree_coef}"></div>
</div>
<div class="form-group"><label for="hauteur_eau" class="control-label col-md-4">Hauteur d'eau :</label>
<div class="col-md-8"><input class="taux form-control" id="hauteur_eau" name="hauteur_eau" value="{$data.hauteur_eau}"></div>
</div>

<div class="form-group">
<label for="commentaire" class="control-label col-md-4">Commentaire :</label>
<div class="col-md-8"><input class="commentaire col-md-12" id="commentaire" name="commentaire" value="{$data.commentaire}"></div>
</div>
<fieldset><legend>Coordonnée géographique précise du point (WGS84)</legend>
<div class="form-group"><label for="longitude_txt" class="control-label col-md-4">Coordonnées textuelles (degrés et minutes décimales) :</label>
<div class="col-md-8">
<input id="longitude_txt" name="longitude_txt" class="form-control" value="{$geom.longitude_txt}" placeholder="longitude : 0°1.0W">
<br>
<input id="latitude_txt" name="latitude_txt" class="form-control" value="{$geom.latitude_txt}" placeholder="latitude : 45°0.0N">
</div>
</div>
<div class="form-group"><label for="long_wgs84" class="control-label col-md-4">Coordonnées numériques :</label>
<div class="col-md-8"><input id="long_wgs84" name="long_wgs84" value="{$geom.long_wgs84}" class="taux form-control" placeholder="long">
<br>
<input id="lat_wgs84" name="lat_wgs84" value="{$geom.lat_wgs84}" class="taux form-control" placeholder="lat">
</div>
</div>
</fieldset>

<div class="form-group center">
      <button type="submit" class="btn btn-primary button-valid">Valider</button>
      {if $data.trait_id > 0 }
      <button class="btn btn-danger button-delete">Supprimer</button>
      {/if}
 </div>
{$csrf}</form>

</div>
</fieldset>
<span class="red">*</span><span class="messagebas">Champ obligatoire</span>