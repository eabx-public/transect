<a href="traitList"><img src="display/images/list.png" height="30">Retour à la liste</a>
{if $rights.manage == 1}
<a href="traitChange?trait_id={$data.trait_id}">
  <img src="display/images/edit.gif" height="30">
  Modifier...
</a>
{/if}
<div class="row">
  <fieldset class="col-md-8">
    <legend>Détail du trait n° {$data.trait_id}</legend>
    <div class="form-display">
      <dl class="dl-horizontal">
        <dt>Date/heure du trait<br>(temps universel) :</dt>
        <dd>de : {$data.trait_debut} à : {$data.trait_fin}
          <br>(de &nbsp;{$data.trait_debut_tu} à : {$data.trait_fin_tu})
        </dd>
      </dl>
      <dl class="dl-horizontal">
        <dt>Station : </dt>
        <dd>{$data.cours_eau_nom} {$data.site_transect_name} {$data.rive_libelle}</dd>
      </dl>
      <dl class="dl-horizontal">
        <dt>Campagne de pêche :</dt>
        <dd>{$data.date_debut} {$data.campagne_statut_name}</dd>
      </dl>
      <dl class="dl-horizontal">
        <dt>Responsable :</dt>
        <dd>{$data.personne_prenom} {$data.personne_nom}</dd>
      </dl>
      <dl class="dl-horizontal">
        <dt>Bateau :</dt>
        <dd>{$data.bateau_nom}</dd>
      </dl>
      <dl class="dl-horizontal">
        <dt>Marée et coefficient :</dt>
        <dd>{$data.maree_name}&nbsp;{$data.maree_coef}</dd>
      </dl>
      <dl class="dl-horizontal">
        <dt>Hauteur d'eau :</dt>
        <dd>{$data.hauteur_eau}</dd>
      </dl>
      <dl class="dl-horizontal">
        <dt title="Expérimentation et protocole de pêche">Expérimentation et protocole de pêche :</dt>
        <dd>{$data.experimentation_nom}<br>
          {$data.protocole_description}</dd>
      </dl>

      <dl class="dl-horizontal">
        <dt>Commentaire :</dt>
        <dd>{$data.commentaire}</dd>
      </dl>
      <fieldset>
        <legend>Coordonnée géographique précise</legend>
        <dl class="dl-horizontal">
          <dt title="Longitude (WGS84 numérique)">Longitude (WGS84 numérique) :</dt>
          <dd>{$data.long_wgs84}</dd>
        </dl>
        <dl class="dl-horizontal">
          <dt title="Latitude (WGS84 numérique)">Latitude (WGS84 numérique) :</dt>
          <dd>{$data.lat_wgs84}</dd>
        </dl>
      </fieldset>
      <dl class="dl-horizontal"></dl>
    </div>
  </fieldset>
</div>
{if $data.trait_id > 0}
  {include file="trait/pecheList.tpl"}
{/if}