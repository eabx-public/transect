<table id="echantillonList" class="table table-bordered table-hover datatable-nopaging display">
    <thead>
        <tr>
            {if $rights["manage"]==1 ||$rights["labo"]==1}
            <th>Modif</th>
            {/if}
            <th>Espèce</th>
            <th>Nombre</th>
            <th>Masse</th>
            <th>(dont nombre/masse mesurés en labo)</th>
            <th>(dont nombre/masse des individus)</th>
            <th>(labo) coef. d'échantillonnage</th>
            <th>Abondance</th>
            <th>Densité</th>
            <th>Commentaire</th>
            <th>
                <img src="display/images/qrcode.jpg" height="25" title="Générer les étiquettes pour les poissons">
            </th>
            <th>
                <img src="display/images/collab.png" height="25"
                    title="Générer le fichier d'importation des poissons dans Collab">
            </th>
        </tr>
    </thead>
    <tbody>
        {section name=lst loop=$dataEchan}
        <tr>
            {if $rights["manage"]==1 ||$rights["labo"]==1}
            <td class="center">
                <a
                    href="echantillonChange?echantillon_id={$dataEchan[lst].echantillon_id}&peche_id={$dataEchan[lst].peche_id}">
                    <img src="display/images/eprouvette.png" height="30">
                </a>
            </td>
            {/if}
            <td>{$dataEchan[lst].code_csp} {$dataEchan[lst].nom} {$dataEchan[lst].type_code}
                ({$dataEchan[lst].echantillon_id})</td>
            <td class="center">{$dataEchan[lst].nombre}</td>
            <td class="center">{$dataEchan[lst].masse}</td>
            <td class="center">{$dataEchan[lst].labo_nombre} / {$dataEchan[lst].labo_masse}</td>
            <td class="center">{$dataEchan[lst].individu_nombre_total} / {$dataEchan[lst].individu_masse_totale}</td>
            <td class="center">
                {if $dataEchan[lst].labo_coef_nombre > 0}
                1/{$dataEchan[lst].labo_coef_nombre}
                {/if}
            </td>
            <td>{$dataEchan[lst].abondance}</td>
            <td>{$dataEchan[lst].densite}</td>
            <td>{$dataEchan[lst].echantillon_commentaire}</td>
            <td class="center">
                <a href="echantillonPrintLabel?echantillon_id={$dataEchan[lst].echantillon_id}&peche_id={$dataEchan[lst].peche_id}"
                    target="_blank">
                    <img src="display/images/qrcode.jpg" height="25" title="Générer les étiquettes pour les poissons">
                </a>
            </td>
            <td class="center">
                <a
                    href="echantillonSendIndividusToCollecScience?echantillon_id={$dataEchan[lst].echantillon_id}&peche_id={$dataEchan[lst].peche_id}">
                    <img src="display/images/collab.png" height="25"
                        title="Créer ou mettre à jour les individus dans Collab">
                </a>
            </td>
        </tr>
        {/section}
    </tbody>
</table>
<br>