<script>
$(document).ready(function() {
	/*$('select').change(function() {
		$('#searchForm').submit();
	} );*/
	$("#boutonPdf").click( function() {
		$("#module").val( "traitPdf");
		$("#searchForm").submit();
	} );
	$(".date").datepicker( { dateFormat: "dd/mm/yy" } );
} ) ;
</script>
<div class="row col-md-8">
	<form  class="form-horizontal protoform" id="searchForm" method="GET" action="traitList">
		<input type="hidden" name="isSearch" value="1">
		<div class="form-group">
			<label for="site_id" class="col-md-2 control-label">Site :</label>
			<div class="col-md-4">
				<select id="site_id" name="site_id" class="form-control">
					<option value="0" {if $dataSearch.site_id == "0"}selected{/if}>Sélectionnez le site...</option>
					{section name=lst loop=$site}
						<option value="{$site[lst].site_id}" {if $dataSearch.site_id == $site[lst].site_id}selected{/if}>
						{$site[lst].site_transect_name} {$site[lst].rive_libelle}
						</option>
					{/section}
				</select>
			</div>
			<label for="site_transect_id" class="col-md-2 control-label">
			ou transect : </label>
			<div class="col-md-4">
				<select name="site_transect_id" id="site_transect_id" class="form-control">
					<option value="0" {if $dataSearch.site_transect_id == "0"}selected{/if}>Sélectionnez le transect...</option>
					{section name=lst loop=$transect}
						<option value="{$transect[lst].site_transect_id}" {if $dataSearch.site_transect_id == $transect[lst].site_transect_id}selected{/if}>
						{$transect[lst].site_transect_name}
						</option>
					{/section}
				</select>
			</div>

			<label for="experimentation_id" class="col-md-2 control-label">Expérimentation :</label>
			<div class="col-md-4">
				<select name="experimentation_id" id="experimentation_id" class="form-control">
				<option value="0" {if $dataSearch.experimentation_id == "0"}selected{/if}>Sélectionnez l'expérimentation...</option>
				{section name=lst loop=$experimentation}
				<option value="{$experimentation[lst].experimentation_id}" {if $dataSearch.experimentation_id == $experimentation[lst].experimentation_id}selected{/if}>
				{$experimentation[lst].experimentation_nom}
				</option>
				{/section}
				</select>
			</div>
			<label for="campagne_id" class="col-md-2 control-label">Campagne de pêche :</label>
			<div class="col-md-4">
				<select name="campagne_id" id="campagne_id" class="form-control">
					<option value="0" {if $dataSearch.campagne_id == "0"}selected{/if}>Sélectionnez la campagne de pêche...</option>
					{foreach $campagnes as $campagne}
						<option value="{$campagne.campagne_id}" {if $dataSearch.campagne_id == $campagne.campagne_id}selected{/if}>
							{$campagne.date_debut} - <i>{$campagne.campagne_statut_name}</i> ({$campagne.experimentation_nom})
						</option>
					{/foreach}
				</select>
			</div>
		</div>

		<div class="form-group">
			<label for="date_debut" class="col-md-2 control-label">
			Période : du</label>
			<input class="date class-control col-md-2" id="date_debut" name="date_debut" value="{$dataSearch.date_debut}">
			<label for="date_fin" class="col-md-2 control-label">
			au </label>
			<input class="date class-control col-md-2" id="date_fin" name="date_fin" value="{$dataSearch.date_fin}">
		</div>
		<div class="form-group">
			<div class="center">
				<input type="submit" class="btn btn-success col-md-2 col-md-offset-2" value="Valider">
				{if $isSearch == 1}
				<input id="boutonPdf" class="btn btn-info col-md-offset-2 col-md-4" value="PDF d'archivage des traits">
				{/if}
			</div>
		</div>
	{$csrf}</form>
</div>