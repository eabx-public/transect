<div class="col-md-12">
  <div class="row">
    <a href="traitPecheList?trait_id={$dataPeche.trait_id}">
      <img src="display/images/list.png" height="30">
      Retour à la liste...
    </a>

    {if $rights["manage"] == 1 || $rights["labo"] == 1}
    &nbsp;
    <a href="pecheChangeComment?peche_id={$dataPeche.peche_id}">
      <img src="display/images/edit.gif" height="30">
      Modifier le commentaire
    </a>
    {/if}
    <!-- Affichage des peches associees au trait -->
    <a href="traitDisplay?trait_id={$dataPeche.trait_id}">
      <img src="display/images/ship-icon.png" height="30">Retour au détail du trait
      {$dataPeche.site_transect_name} {$dataPeche.rive_libelle}
    </a>
    :
    {section name=lst loop=$peches}
    <span {if $peches[lst].peche_id==$dataPeche.peche_id}class="background-green" {/if}>
      <a href="echantillonList?trait_id={$peches[lst].trait_id}&peche_id={$peches[lst].peche_id}">
        {$peches[lst].position_engin_name}
      </a>
    </span>
    &nbsp;
    {/section}
  </div>
  <div class="row">
    <table class="tablemulticolonne">
      <tr>
        <td>
          <fieldset>
            <legend>Détail de la pêche n° {$dataPeche.peche_id}</legend>
            {include file="trait/pecheDetail.tpl"}
          </fieldset>
        </td>
      </tr>

      {if $rights["manage"] == 1 || $rights["labo"] == 1}
      <tr>
        <td>
          <a href="echantillonChange?echantillon_id=0&peche_id={$dataPeche.peche_id}">
            <img src="display/images/eprouvette.png" height="30">
            Saisir un nouvel échantillon...
          </a>
          {if $change == 1}
          {include file="trait/echantillonChange.tpl"}
          {/if}
        </td>
      </tr>
      {/if}
      <tr>
        <td>
          <fieldset>
            <legend>Récapitulatif</legend>
            <div class="row">
              <div class="col-md-8">
                {include file="trait/pecheCompletedChange.tpl"}
              </div>
              <div class="col-md-4">
                {if $dataPeche.cs_uid > 0 && $dataPeche.completed == 1}
                {include file="trait/pecheGenerateCSfile.tpl"}
                {/if}
              </div>
            </div>
            <div class="row">
              {include file="trait/echantillonList.tpl"}
            </div>
          </fieldset>
          <br>
          <fieldset>
            <legend>Individus non conservés et mesurés, ou congelés à bord</legend>
            {include file="trait/individuList.tpl"}
          </fieldset>
        </td>
      </tr>
    </table>
  </div>
</div>