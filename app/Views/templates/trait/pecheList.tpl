<h2>Liste des opérations de pêche rattachées</h2>
{if $rights.manage == 1}
<a href="pecheChange?trait_id={$trait_id}&peche_id=0">
<img src="display/images/new.png" height="30">
Nouvelle pêche (filet)...
</a>
{/if}

<table id="pecheListe" class="table table-bordered table-hover datatable-nopaging " data-order='[2,"asc"]'>
<thead>
<tr>
{if $rights.manage == 1}
<th>Modif.</th>
{/if}
<th>Captures</th>
<th>Position de l'engin</th>
<th>Conductivité µS/cm</th>
<th>O2 sat %</th>
<th>O2 mg/l</th>
<th>T° l'eau °C</th>
<th>Turbidité TFU</th>
<th>Salinité mg/l</th>
<th>pH</th>
<th>Volume filtré m3</th>
<th>Couranto valide</th>
<th>Vitesse m/s</th>
<th>Vitesse couranto m/s</th>
<th>Captures non conservées</th>
<th>Captures pour autres manips</th>
<th>Gélatineux</th>
<th>Nb échan. saisis</th>
<th>Nb poissons congelés</th>
<th>Commentaire</th>
</tr>
</thead>
<tbody>
{section name=lst loop=$dataPeche}
<tr>
{if $rights.manage == 1}
<td class="center">
<a href="pecheChange?trait_id={$trait_id}&peche_id={$dataPeche[lst].peche_id}">
<img src="display/images/edit.gif" height="30">({$dataPeche[lst].peche_id})
</a>
</td>
{/if}
<td class="center {if $dataPeche[lst].completed == 1} background-green {/if}">
<a href="echantillonList?trait_id={$dataPeche[lst].trait_id}&peche_id={$dataPeche[lst].peche_id}" {if $dataPeche[lst].completed == 1}title="Saisie des échantillons terminée"{/if}>
<img src="display/images/poisson.png" height="30">
</a>
</td>
<td>{$dataPeche[lst].position_engin_name}</td>
<td class="right">{$dataPeche[lst].conductivite}</td>
<td class="right">{$dataPeche[lst].oxygene_sat}</td>
<td class="right">{$dataPeche[lst].oxygene}</td>
<td class="right">{$dataPeche[lst].temp_eau}</td>
<td class="right">{$dataPeche[lst].turbidite}</td>
<td class="right">{$dataPeche[lst].salinite}</td>
<td class="right">{$dataPeche[lst].ph}</td>
<td class="right">{$dataPeche[lst].volume_filtre}</td>
<td class="center">{if $dataPeche[lst].couranto_valid == 't'}oui{else}<span class="red">non</span>{/if}</td>
<td class="right">{$dataPeche[lst].vitesse}</td>
<td class="right">{$dataPeche[lst].vitesse_couranto}</td>
<td>{$dataPeche[lst].capture_non_conserve}</td>
<td>{$dataPeche[lst].capture_other}</td>
<td class="right">{$dataPeche[lst].gelatineux}</td>
<td style="text-align:center;">{$dataPeche[lst].echantillon_nb}</td>
<td class="center {if $dataPeche[lst].poisson_congele > 0 && $dataPeche[lst].congelation_ok != 1}red{/if}">
{$dataPeche[lst].poisson_congele}
</td>
<td>{$dataPeche[lst].commentaire}
</td>
</tr>
{/section}
</tbody>
</table>
 
