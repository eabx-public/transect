<h2>Liste des campagnes de pêche</h2>
<div class="col-md-12 col-lg-8">
  <a href="campagneChange?campagne_id=0">
    Nouvelle campagne
  </a>
  <table id="campagneList" class="table table-bordered table-hover datatable-searching " data-order='[[1,"desc"]]'>
    <thead>
      <tr>
        <th>Id</th>
        <th>Date de début</th>
        <th>Durée (jours)</th>
        <th>Statut</th>
        <th>Commentaire</th>
        <th>Motif d'annulation ou d'interruption</th>
        <th>Compte-rendu</th>
      </tr>
    </thead>
    <tbody>
      {foreach $data as $row}
      <tr>
        <td class="center">
          <a href="campagneChange?campagne_id={$row.campagne_id}">
            {$row.campagne_id}
          </a>
        </td>
        <td>{$row.date_debut}</td>
        <td class="center">{$row.duree}</td>
        <td>{$row.campagne_statut_name}</td>
        <td class="textareaDisplay">{$row.commentaire}</td>
        <td>{$row.motif_annulation_name}</td>
        <td class="center">
          {if $row.has_compte_rendu == 't'}
            <a href="campagneGetCompteRendu?campagne_id={$row.campagne_id}" title="Consulter le compte-rendu">
              <img src="display/images/pdf.png" height="25">
            </a>
          {/if}
        </td>
      </tr>
      {/foreach}
    </tbody>
  </table>
</div>