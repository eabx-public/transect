<h2>Modification du commentaire de l'opération de pêche</h2>
<a href="traitPecheList?trait_id={$data.trait_id}">
      <img src="display/images/list.png" height="30">
      Retour à la liste...
</a>&nbsp;
<a href="echantillonList?trait_id={$data.trait_id}&peche_id={$data.peche_id}">
      Retour aux échantillons</a>
<div class="row">
      <div class="col-md-6">
            <fieldset>
                  <legend>Modifier le commentaire concernant l'opération de pêche n° {$data.peche_id}</legend>
                  <div>
                        <form method="post" class="form-horizontal protoform"
                              action="pecheWriteComment">
                              <input type="hidden" name="peche_id" value="{$data.peche_id}">
                              <input type="hidden" name="trait_id" value="{$data.trait_id}">
                              <input type="hidden" name="materiel_id" value="{$data.materiel_id}">
                              <input type="hidden" name="position_engin_id" value="{$data.position_engin_id}">
                              <div class="form-group">
                                    <label for="commentaire" class="control-label col-md-4">Commentaire :</label>
                                    <div class="col-md-8">
                                          <textarea id="commentaire" name="commentaire" class="form-control"
                                                rows="5">{$data.commentaire}</textarea>
                                    </div>
                              </div>

                              <div class="form-group">
                                    <label for="cs_uid" class="control-label col-md-4">
                                          UID de l'échantillon dans
                                          <a target="new" href="{$collecscience}">Collab</a> :
                                    </label>
                                    <div class="col-md-8">
                                          <input id="cs_uid" name="cs_uid" class="form-control nombre"
                                                placeholder="1234" value="{$data.cs_uid}">
                                    </div>
                              </div>
                              <div class="form-group center">
                                    <button type="submit" class="btn btn-primary button-valid">Valider</button>
                              </div>
                        {$csrf}</form>
                  </div>
            </fieldset>
      </div>
</div>