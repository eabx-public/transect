<table id="individuList" class="table table-bordered table-hover datatable display">
<thead>
<tr>
<th>Code</th>
<th>Espèce</th>
<th>Type</th>
<th>Longueur<br>fourche</th>
<th>L. fourche<br>par classe</th>
<th>masse</th>
</tr>
</thead>
<tbody>
{section name=lst loop=$dataIndividu}
<tr>
<td>{$dataIndividu[lst].individu_id}</td>
<td>{$dataIndividu[lst].code_csp} {$dataIndividu[lst].nom}</td>
<td>{$dataIndividu[lst].type_code}</td>
<td>{$dataIndividu[lst].longueur_fourche}</td>
<td>{$dataIndividu[lst].longueur_fourche_by_classe}</td>
<td>{$dataIndividu[lst].individu_masse}</td>
</tr>
{/section}
</tbody>
</table>
