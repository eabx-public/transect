<h2>Modification d'un statut de campagne</h2>

<div class="row">
  <div class="col-md-6">
    <a href="campagneList">
      <img src="display/images/list.png" height="25">
      Retour à la liste
    </a>

    <form class="form-horizontal protoform" id="campagne_statutForm" method="post" action="campagneWrite" enctype="multipart/form-data">
      <input type="hidden" name="campagne_id" value="{$data.campagne_id}">
      <input type="hidden" name="moduleBase" value="campagne">
      <div class="form-group">
          <label for="experimentation_id" class="control-label col-md-4">Experimentation<span class="red">*</span> :</label>
          <div class="col-md-8">
            <select id="experimentation_id" name="experimentation_id" class="form-control">
              {foreach $experimentations as $exp}
                <option value="{$exp.experimentation_id}" {if $exp.experimentation_id == $data.experimentation_id}selected{/if}>
                  {$exp.experimentation_nom}
                </option>
              {/foreach}
            </select>
          </div>
        </div>
      <div class="form-group">
        <label for="date_debut" class="control-label col-md-4">Date de début<span class="red">*</span> :</label>
        <div class="col-md-8">
          <input id="date_debut" name="date_debut" required value="{$data.date_debut}" class="form-control datepicker">
        </div>
      </div>
      <div class="form-group">
        <label for="duree" class="control-label col-md-4">Durée :</label>
        <div class="col-md-8">
          <input id="duree" name="duree" value="{$data.duree}" class="form-control nombre">
        </div>
      </div>
      <div class="form-group">
        <label for="campagne_statut_id" class="control-label col-md-4">Statut de la campagne :</label>
        <div class="col-md-8">
          <select id="campagne_statut_id" name="campagne_statut_id" class="form-control">
            {foreach $statuts as $statut}
              <option value="{$statut.campagne_statut_id}" {if $statut.campagne_statut_id == $data.campagne_statut_id}selected{/if}>
                {$statut.campagne_statut_name}
              </option>
            {/foreach}
          </select>
        </div>
      </div>
      <div class="form-group">
        <label for="motif_annulation_id" class="control-label col-md-4">motif d'annulation ou d'interruption de la campagne :</label>
        <div class="col-md-8">
          <select id="motif_annulation_id" name="motif_annulation_id" class="form-control">
            <option value="" {if $data.modif_annulation_id == ""}selected{/if}>Sélectionnez...</option>
            {foreach $motifs as $motif}
              <option value="{$motif.motif_annulation_id}" {if $motif.motif_annulation_id == $data.motif_annulation_id}selected{/if}>
                {$motif.motif_annulation_name}
              </option>
            {/foreach}
          </select>
        </div>
      </div>
      <div class="form-group">
        <label for="commentaire" class="control-label col-md-4">Commentaire :</label>
        <div class="col-md-8">
          <textarea id="commentaire" name="commentaire" class="form-control" rows="5">{$data.commentaire}</textarea>
        </div>
      </div>
      <div class="form-group">
        <label for="compte_rendu" class="control-label col-md-4">Télécharger un compte-rendu (format PDF) :</label>
        <div class="col-md-8">
          <input type="file" accept="application/pdf" id="compte_rendu" name="compte_rendu" class="form-control">
        </div>
      </div>
      {if $data.has_compte_rendu == 1}
        <div class="form-group">
          <label for="delete_compte_rendu" class="control-label col-md-4">Supprimer le compte-rendu :</label>
          <div class="col-md-8">
            <input type="checkbox" id="delete_compte_rendu" name="delete_compte_rendu" class="form-control" value="1">
          </div>
        </div>
      {/if}
      <div class="form-group center">
        <button type="submit" class="btn btn-primary button-valid">Valider</button>
        {if $data.campagne_id > 0 }
          <button class="btn btn-danger button-delete">Supprimer</button>
        {/if}
      </div>
    {$csrf}</form>
  </div>
</div>
<span class="red">*</span><span class="messagebas">Donnée obligatoire</span>