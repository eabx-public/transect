<form class="form-horizontal protoform" id="pecheChange" method="post" action="pecheSendSamplesToCollecScience">
    <input type="hidden" name="peche_id" value="{$dataPeche.peche_id}">
    <input type="hidden" name="trait_id" value="{$dataPeche.trait_id}">
    <div class="form-group">
        <div class="col-md-12 center">
            <button type="submit" class="btn btn-primary button-valid">
                <img src="display/images/eprouvette.png" height="18">
                Générer les échantillons dans Collec-Science (lyophilisation)
            </button>
        </div>
    </div>
{$csrf}</form>