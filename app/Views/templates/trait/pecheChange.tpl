<h2>Modification de l'opération de pêche</h2>
<a href="traitPecheList?trait_id={$data.trait_id}">
<img src="display/images/list.png" height="30">
Retour à la liste des traits
</a>&nbsp;
<a href="traitDisplay?trait_id={$data.trait_id}">
<img src="display/images/ship-icon.png" height="30">
Retour au trait n° {$data.trait_id} - {$trait.site_transect_name} {$trait.rive_libelle} {$trait.trait_debut}
</a>

<div class="row">
<div class="col-md-6">
<fieldset>
<legend>
{if $data.peche_id > 0}
Modifier l'opération de pêche n° {$data.peche_id}
{else}
Nouvelle pêche/filet
{/if}
</legend>
<div>
<form class="form-horizontal protoform" id="pecheChange" method="post" action="pecheWrite">
<input type="hidden" name="peche_id" value="{$data.peche_id}">
<input type="hidden" name="trait_id" value="{$data.trait_id}">
<input type="hidden" name="moduleBase" value="peche">

<div class="form-group"><label for="position_engin_id" class="control-label col-md-4">Position de l'engin de pêche<span class="red">*</span> :</label>
<div class="col-md-8">
<select id="position_engin_id" name="position_engin_id"  class="form-control">
{section name=lst loop=$position_engin}
{strip}
<option value="{$position_engin[lst].position_engin_id}" 
{if $position_engin[lst].position_engin_id == $data.position_engin_id}selected{/if}>
{$position_engin[lst].position_engin_name}
</option>
{/strip}
{/section}
</select>
</div>
</div>
<div class="form-group">
<label for="materiel_id" class="control-label col-md-4">Matériel utilisé<span class="red">*</span> :</label>
<div class="col-md-8">
<select id="materiel_id" name="materiel_id" class="form-control">
{section name=lst loop=$materiel}
{strip}
<option value="{$materiel[lst].materiel_id}" 
{if $materiel[lst].materiel_id == $data.materiel_id}selected{/if}>
{$materiel[lst].materiel_code} {$materiel[lst].materiel_description}
</option>
{/strip}
{/section}
</select>
</div>
</div>
<div class="form-group"><label for="immersion_tx" class="control-label col-md-4">Taux d'immersion de l'engin de pêche</label>
<div class="col-md-8"><input class="taux form-control" id="immersion_tx" name="immersion_tx" value="{$data.immersion_tx}"></div>
</div>
<div class="form-group"><label for="volume_filtre" class="control-label col-md-4">Volume filtré retenu (m3) :</label>
<div class="col-md-8"><input class="taux form-control" id="volume_filtre" name="volume_filtre" value="{$data.volume_filtre}"></div>
</div>
<div class="form-group"><label for="duree_filage" class="control-label col-md-4">Durée de filage (sec) :</label>
<div class="col-md-8"><input class="nombre form-control" id="duree_filage" name="duree_filage" value="{$data.duree_filage}"></div>
</div>
<div class="form-group"><label for="duree_virage" class="control-label col-md-4">Durée de virage (sec) :</label>
<div class="col-md-8"><input class="nombre form-control" id="duree_virage" name="duree_virage" value="{$data.duree_virage}"></div>
</div>
<fieldset>
<legend>Courantomètre principal</legend>
<div class="form-group">
<label for="couranto_debut" class="control-label col-md-4">Valeur relevée au début :</label>
<div class="col-md-8"><input class="nombre form-control" id="couranto_debut" name="couranto_debut" value="{$data.couranto_debut}"></div>
</div>
<div class="form-group">
<label for="couranto_fin" class="control-label col-md-4">Valeur relevée à la fin :</label>
<div class="col-md-8"><input class="nombre form-control" id="couranto_fin" name="couranto_fin" value="{$data.couranto_fin}"></div>
</div>
<div class="form-group">
<label for="volume_filtre_pri" class="control-label col-md-4">Volume filtré (m3) :</label>
<div class="col-md-8"><input class="taux form-control" id="volume_filtre_pri" name="volume_filtre_pri" value="{$data.volume_filtre_pri}"></div>
</div>
<div class="form-group">
<label for="vitesse" class="control-label col-md-4">Vitesse calculée :</label>
<div class="col-md-8"><input class="taux form-control" id="vitesse" name="vitesse" value="{$data.vitesse}"></div>
</div>
<div class="form-group">
      <label for="couranto_valid1" class="control-label col-md-4">Mesure de couranto valide :</label>
      <div class="col-md-8">
            <input type="radio" id="couranto_valid1" name="couranto_valid" {if $data.couranto_valid == 1}checked{/if} value="1">oui
            <input type="radio" id="couranto_valid0" name="couranto_valid" {if $data.couranto_valid != 1}checked{/if} value="0">non
      </div>
      </div>
</fieldset>
<fieldset>
<legend>Courantomètre de secours</legend>
<div class="form-group">
<label for="couranto_sec_debut" class="control-label col-md-4">Valeur relevée au début :</label>
<div class="col-md-8"><input class="nombre form-control" id="couranto_sec_debut" name="couranto_sec_debut" value="{$data.couranto_sec_debut}"></div>
</div>
<div class="form-group">
<label for="couranto_sec_fin" class="control-label col-md-4">Valeur relevée à la fin :</label>
<div class="col-md-8"><input class="nombre form-control" id="couranto_sec_fin" name="couranto_sec_fin" value="{$data.couranto_sec_fin}"></div>
</div>
<div class="form-group">
<label for="volume_filtre_secours" class="control-label col-md-4">Volume filtré (m3) :</label>
<div class="col-md-8"><input class="taux form-control" id="volume_filtre_secours" name="volume_filtre_secours" value="{$data.volume_filtre_secours}"></div>
</div>
<div class="form-group">
<label for="vitesse_sec" class="control-label col-md-4">Vitesse calculée :</label>
<div class="col-md-8"><input class="taux form-control" id="vitesse_sec" name="vitesse_sec" value="{$data.vitesse_sec}"></div>
</div>
</fieldset>

<fieldset>
<legend>Données physico-chimiques</legend>
<div class="form-group">
<label for="conductivite" class="control-label col-md-4">Conductivité (µS/cm) :</label>
<div class="col-md-8"><input class="taux form-control" id="conductivite" name="conductivite" value="{$data.conductivite}"></div>
</div>
<div class="form-group">
<label for="oxygene_sat" class="control-label col-md-4">Oxygène sat :</label>
<div class="col-md-8"><input class="taux form-control" id="oxygene_sat" name="oxygene_sat" value="{$data.oxygene_sat}"></div>
</div>
<div class="form-group">
<label for="oxygene" class="control-label col-md-4">Oxygène mg/l :</label>
<div class="col-md-8"><input class="taux form-control" id="oxygene" name="oxygene" value="{$data.oxygene}"></div>
</div>
<div class="form-group">
<label for="temp_eau" class="control-label col-md-4">Température de l'eau (°C) :</label>
<div class="col-md-8"><input class="taux form-control" id="temp_eau" name="temp_eau" value="{$data.temp_eau}"></div>
</div>
<div class="form-group">
<label for="turbidite" class="control-label col-md-4">Turbidité (FTU) :</label>
<div class="col-md-8"><input class="taux form-control" id="turbidite" name="turbidite" value="{$data.turbidite}"></div>
</div>
<div class="form-group">
<label for="salinite" class="control-label col-md-4">Salinité (mg/l) :</label>
<div class="col-md-8"><input class="taux form-control" id="salinite" name="salinite" value="{$data.salinite}"></div>
</div>
<div class="form-group">
<label for="ph" class="control-label col-md-4">pH :</label>
<div class="col-md-8"><input class="taux form-control" id="ph" name="ph" value="{$data.ph}"></div>
</div>
</fieldset>
<div class="form-group">
<label for="capture_non_conserve" class="control-label col-md-4">Captures non conservées :</label>
<div class="col-md-8"><input class="commentaire form-control" id="capture_non_conserve" name="capture_non_conserve" value="{$data.capture_non_conserve}"></div>
</div>
<div class="form-group">
<label for="capture_other" class="control-label col-md-4">Autres captures pour expérimentations :</label>
<div class="col-md-8"><input class="commentaire form-control" id="capture_other" name="capture_other" value="{$data.capture_other}"></div>
</div>
<div class="form-group"><label for="gelatineux" class="control-label col-md-4">Volume de gélatineux pêchés, en litres :</label>
<div class="col-md-8"><input class="taux form-control" id="gelatineux" name="gelatineux" value="{$data.gelatineux}"></div>
</div>
<div class="form-group"><label for="poisson_congele" class="control-label col-md-4">Nbre de poissons congelés à bord :</label>
<div class="col-md-8"><input class="nombre form-control" id="poisson_congele" name="poisson_congele" value="{$data.poisson_congele}"></div>
</div>

<div class="form-group">
<label for="commentaire" class="control-label col-md-4">Commentaire général :</label>
<div class="col-md-8"><textarea class="form-control" rows="3" id="commentaire" name="commentaire">{$data.commentaire}</textarea>
</div>
</div>

<div class="form-group center">
      <button type="submit" class="btn btn-primary button-valid">Valider</button>
      {if $data.peche_id > 0 }
      <button class="btn btn-danger button-delete">Supprimer</button>
      {/if}
 </div>
{$csrf}</form>
</div>

</fieldset>
</div>
 </div>
<span class="red">*</span><span class="messagebas">Champ obligatoire</span>

