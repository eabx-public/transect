<form class="form-horizontal" id="pecheChangeForm" method="post" action="pecheCompletedWrite">
  <input type="hidden" name="peche_id" value="{$dataPeche.peche_id}">
  <input type="hidden" name="trait_id" value="{$dataPeche.trait_id}">
  <input type="hidden" name="moduleBase" value="pecheCompleted">
  <div class="form-group">
    <label for="completed0" class="control-label col-md-2">Saisie finie ?</label>
    <div class="col-md-2">
      <label class="radio-inline"><input type="radio" id="completed0" name="completed" value="0" {if $dataPeche.completed == 0} checked{/if}>non</label>
      <label class="radio-inline"><input type="radio" id="completed1" name="completed" value="1" {if $dataPeche.completed == 1} checked{/if}>oui</label>
    </div>
    <label for="freezing0" class="control-label col-md-4">Trait. des poissons congelés fini ?</label>
    <div class="col-md-2">
      <label class="radio-inline"><input type="radio" id="freezing0" name="congelation_ok" value="0" {if $dataPeche.congelation_ok != 1} checked{/if}>non</label>
      <label class="radio-inline"><input type="radio" id="freezing1" name="congelation_ok" value="1" {if $dataPeche.congelation_ok == 1} checked{/if}>oui</label>
    </div>
    <div class="col-md-2">
      <button type="submit" class="btn btn-primary button-valid">Valider</button>
    </div>
  </div>
{$csrf}</form>
