<h2>Liste des motifs d'annulation ou d'interruption des campagnes</h2>
<div class="col-md-6">
  <a href="motifAnnulationChange?motif_annulation_id=0">
    Nouveau motif
  </a>
  <table id="motif_annulationList" class="table table-bordered table-hover datatable ">
    <thead>
      <tr>
        <th>Id</th>
        <th>Libellé</th>
      </tr>
    </thead>
    <tbody>
      {foreach $data as $row}
      <tr>
        <td class="center">
          <a href="motifAnnulationChange?motif_annulation_id={$row.motif_annulation_id}">
            {$row.motif_annulation_id}
          </a>
        </td>
        <td>{$row.motif_annulation_name}</td>
      </tr>
      {/foreach}
    </tbody>
  </table>
</div>