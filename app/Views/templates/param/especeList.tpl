<script>
    $(document).ready(function() { 
        var tableList = $( '#especeList' ).DataTable( {
					dom: 'Birtp',
					"language": dataTableLanguage,
					"paging": false,
					"searching": true,
					"stateSave": false,
					"stateDuration": 60 * 60 * 24 * 30
				});
        $( '#especeList thead th' ).each( function () {
				var title = $( this ).text();
				var size = title.trim().length;
				if ( size > 0 ) {
					$( this ).html( '<input type="text" placeholder="' + title + '" size="' + size + '" class="searchInput" title="'+title+'">' );
				}
			} );
			tableList.columns().every( function () {
				var that = this;
				if ( that.index() > 0 ) {
					$( 'input', this.header() ).on( 'keyup change clear', function () {
						if ( that.search() !== this.value ) {
							that.search( this.value ).draw();
						}
					} );
				}
			} );
			$( ".searchInput" ).hover( function () {
				$( this ).focus();
			} );
    });
</script>

<h2>Liste des espèces</h2>
<div class="col-md-12">
  <a href="especeChange?espece_id=0">
    Nouvelle espèce
  </a>
  <table id="especeList" class="table table-bordered table-hover " data-order='[[1,"asc"]]'>
    <thead>
      <tr>
        <th>Id</th>
        <th>Nom latin</th>
        <th>Nom français</th>
        <th>Auteur</th>
        <th>Phylum</th>
        <th>Subphylum</th>
        <th>Classe</th>
        <th>Ordre</th>
        <th>Famille</th>
        <th>Genre</th>
        <th>Code PERM IFREMER</th>
        <th>Code SANDRE</th>
      </tr>
    </thead>
    <tbody>
      {foreach $data as $row}
      <tr>
        <td class="center">
          <a href="especeChange?espece_id={$row.espece_id}">
            {$row.espece_id}
          </a>
        </td>
        <td>{$row.nom}</td>
        <td>{$row.nom_fr}</td>
        <td>{$row.auteur}</td>
        <td>{$row.phylum}</td>
        <td>{$row.subphylum}</td>
        <td>{$row.classe}</td>
        <td>{$row.ordre}</td>
        <td>{$row.famille}</td>
        <td>{$row.genre}</td>
        <td>{$row.code_perm_ifremer}</td>
        <td>{$row.code_sandre}</td>
      </tr>
      {/foreach}
    </tbody>
  </table>
</div>