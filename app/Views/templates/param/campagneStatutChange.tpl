<h2>Modification d'un statut de campagne</h2>

<div class="row">
  <div class="col-md-6">
    <a href="campagneStatutList">
      <img src="display/images/list.png" height="25">
      Retour à la liste
    </a>

    <form class="form-horizontal protoform" id="campagne_statutForm" method="post" action="campagneStatutWrite">
      <input type="hidden" name="campagne_statut_id" value="{$data.campagne_statut_id}">
      <input type="hidden" name="moduleBase" value="campagneStatut">

      <div class="form-group">
        <label for="campagne_statut_nom" class="control-label col-md-4">Nom<span class="red">*</span> :</label>
        <div class="col-md-8">
          <input id="campagne_statut_name" name="campagne_statut_name" required value="{$data.campagne_statut_name}" class="form-control">
        </div>
      </div>
      <div class="form-group center">
        <button type="submit" class="btn btn-primary button-valid">Valider</button>
        {if $data.campagne_statut_id > 0 }
          <button class="btn btn-danger button-delete">Supprimer</button>
        {/if}
      </div>
    {$csrf}</form>
  </div>
</div>
<span class="red">*</span><span class="messagebas">Donnée obligatoire</span>