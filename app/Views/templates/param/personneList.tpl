<h2>Liste des opérateurs</h2>
{if $rights["param"] == 1}
<a href="personneChange?personne_id=0">
Nouvel opérateur...
</a>
{/if}
<table id="personneList" class="table table-bordered table-hover datatable " >
<thead>
<tr>
<th>Id</th>
<th>Nom</th>
<th>Prénom</th>
<th>Actif ?</th>
</tr>
</thead><tbody>
{section name=lst loop=$data}
<tr>
<td class="center">
{if $rights["param"] == 1}
<a href="personneChange?personne_id={$data[lst].personne_id}">
{$data[lst].personne_id}
</a>
{else}
{$data[lst].personne_id}
{/if}
</td>
<td>{$data[lst].personne_nom}</td>
<td>{$data[lst].personne_prenom}</td>
<td class="center">
{if $data[lst].actif == 1}Oui{else}Non{/if}
</td>
</tr>
{/section}
</tbody>
</table>