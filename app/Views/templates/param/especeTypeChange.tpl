<h2>Modification d'une espèce-type</h2>
<a href="especeTypeList">
            <img src="display/images/list.png" height="25">
            Retour à la liste
        </a>
        <div class="row">
            <div class="col-md-6">
                <form class="form-horizontal" id="espece_typeForm" method="post" action="especeTypeWrite">
                    <input type="hidden" name="espece_type_id" value="{$data.espece_type_id}">
                    <input type="hidden" name="moduleBase" value="especeType">
        
                    <div class="form-group">
                        <label for="espece_id" class="control-label col-md-4">
                            <span class="red">*</span>Espèce :
                        </label>
                        <div class="col-md-8">
                            <select id="espece_id" name="espece_id" class="form-control">
                                {foreach $especes as $espece}
                                <option value="{$espece.espece_id}" {if $data.espece_id==$espece.espece_id}selected{/if}>
                                    {$espece.nom} ({$espece.nom_fr})
                                </option>
                                {/foreach}
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="type_id" class="control-label col-md-4">
                            Stade :
                        </label>
                        <div class="col-md-8">
                            <select id="type_id" name="type_id" class="form-control">
                                <option value="" {if $data.type_id=="" }selected{/if}>
                                    Choisissez...
                                </option>
                                {foreach $types as $type}
                                <option value="{$type.type_id}" {if $type.type_id==$data.type_id}selected{/if}>
                                    {$type.type_code} ({$type.type_description})
                                </option>
                                {/foreach}
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="code_csp" class="control-label col-md-4">Code CSP :</label>
                        <div class="col-md-8">
                            <input id="code_csp" name="code_csp" value="{$data.code_csp}" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="message" class="control-label col-md-4">Message (précision) :</label>
                        <div class="col-md-8">
                            <input id="message" name="message" value="{$data.message}" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="ordre_tri" class="control-label col-md-4">Ordre de tri :</label>
                        <div class="col-md-8">
                            <input type="number" id="ordre_tri" name="ordre_tri" value="{$data.ordre_tri}" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="espece_type_grouping" class="control-label col-md-4">Identifiant de l'espèce-type de regroupement (export Collec-Science) :</label>
                        <div class="col-md-8">
                            <input id="espece_type_grouping" name="espece_type_grouping" value="{$data.espece_type_grouping}" class="form-control nombre">
                        </div>
                    </div>
                    <div class="form-group center">
                        <button type="submit" class="btn btn-primary button-valid">Valider</button>
                        {if $data.espece_type_id > 0 }
                        <button class="btn btn-danger button-delete">Supprimer</button>
                        {/if}
                    </div>
                {$csrf}</form>
            </div>
        </div>
<span class="red">*</span><span class="messagebas">Donnée obligatoire</span>