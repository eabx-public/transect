<h2>Modification d'un opérateur</h2>

<fieldset class="col-sm-6">
      <legend>Opérateur n° {$data.personne_id}</legend>

      <div class="row">
            <div class="col-md-6">
                  <a href="personneList">
                        <img src="display/images/list.png" height="25">
                        Retour à la liste
                  </a>

                  <form class="form-horizontal protoform" id="personneForm" method="post" action="personneWrite">
                        <input type="hidden" name="personne_id" value="{$data.personne_id}">
                        <input type="hidden" name="moduleBase" value="personne">

                        <div class="form-group">
                              <label for="personne_nom" class="control-label col-md-4">Nom<span class="red">*</span> :</label>
                              <div class="col-md-8">
                                    <input id="personne_nom" name="personne_nom" required value="{$data.personne_nom}"
                                          class="form-control">
                              </div>
                        </div>

                        <div class="form-group">
                              <label for="personne_prenom" class="control-label col-md-4">Prénom :</label>
                              <div class="col-md-8">
                                    <input id="personne_nom" name="personne_prenom" value="{$data.personne_prenom}"
                                          class="form-control">
                              </div>
                        </div>

                        <div class="form-group">
                              <label for="actif" class="control-label col-md-4">Opérateur actif ?</label>
                              <div class="col-md-8">
                                    <label class="radio-inline"><input type="radio" name="actif" value="1" {if $data.actif==1}checked
                                                {/if}>Oui </label> <label class="radio-inline"><input type="radio" name="actif" value="0"
                                                {if $data.actif !=1}checked {/if}>Non </label> </div> </div> <div class="form-group center">
                                          <button type="submit" class="btn btn-primary button-valid">Valider</button>
                                          {if $data.personne_id > 0 }
                                          <button class="btn btn-danger button-delete">Supprimer</button>
                                          {/if}
                                    </label>
                              </div>
                        </div>
                  {$csrf}</form>
            </div>
      </div>
</fieldset>

<span class="red">*</span><span class="messagebas">Donnée obligatoire</span>