<h2>Liste des statuts de campagne</h2>
<div class="col-md-6">
  <a href="campagneStatutChange?campagne_statut_id=0">
    Nouveau statut
  </a>
  <table id="campagne_statutList" class="table table-bordered table-hover datatable ">
    <thead>
      <tr>
        <th>Id</th>
        <th>Libellé</th>
      </tr>
    </thead>
    <tbody>
      {foreach $data as $row}
      <tr>
        <td class="center">
          <a href="campagneStatutChange?campagne_statut_id={$row.campagne_statut_id}">
            {$row.campagne_statut_id}
          </a>
        </td>
        <td>{$row.campagne_statut_name}</td>
      </tr>
      {/foreach}
    </tbody>
  </table>
</div>