<script>
  $(document).ready(function() { 
      var tableList = $( '#especeTypeList' ).DataTable( {
        dom: 'Birtp',
        "language": dataTableLanguage,
        "paging": false,
        "searching": true,
        "stateSave": false,
        "stateDuration": 60 * 60 * 24 * 30
      });
      $( '#especeTypeList thead th' ).each( function () {
      var title = $( this ).text();
      var size = title.trim().length;
      if ( size > 0 ) {
        $( this ).html( '<input type="text" placeholder="' + title + '" size="' + size + '" class="searchInput" title="'+title+'">' );
      }
    } );
    tableList.columns().every( function () {
      var that = this;
      if ( that.index() > 0 ) {
        $( 'input', this.header() ).on( 'keyup change clear', function () {
          if ( that.search() !== this.value ) {
            that.search( this.value ).draw();
          }
        } );
      }
    } );
    $( ".searchInput" ).hover( function () {
      $( this ).focus();
    } );
  });
</script>

<h2>Liste des espèces-types</h2>
<div class="col-md-6">
  <a href="especeTypeChange?espece_type_id=0">
    Nouvelle espèce-type
  </a>
  <table id="especeTypeList" class="table table-bordered table-hover " data-order='[[6,"asc"],[3,"asc"]]'>
    <thead>
      <tr>
        <th>Id</th>
        <th>Code CSP</th>
        <th>Nom latin</th>
        <th>Nom français</th>
        <th>Stade</th>
        <th>Message</th>
        <th>Ordre de tri</th>
        <th>Id de regroupement (export Collec-Science)</th>
      </tr>
    </thead>
    <tbody>
      {foreach $data as $row}
      <tr>
        <td class="center">
          <a href="especeTypeChange?espece_type_id={$row.espece_type_id}">
            {$row.espece_type_id}
          </a>
        </td>
        <td>{$row.code_csp}</td>
        <td>{$row.nom}</td>
        <td>{$row.nom_fr}</td>
        <td>{$row.type_code}</td>
        <td>{$row.message}</td>
        <td class="center">{$row.ordre_tri}</td>
        <td class="center">{$row.espece_type_grouping}</td>
      </tr>
      {/foreach}
    </tbody>
  </table>
</div>