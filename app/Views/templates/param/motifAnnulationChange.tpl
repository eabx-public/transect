<h2>Modification d'un motif d'annulation ou d'interruption de campagne</h2>

<div class="row">
  <div class="col-md-6">
    <a href="motifAnnulationList">
      <img src="display/images/list.png" height="25">
      Retour à la liste
    </a>

    <form class="form-horizontal protoform" id="motif_annulationForm" method="post" action="motifAnnulationWrite">
      <input type="hidden" name="motif_annulation_id" value="{$data.motif_annulation_id}">
      <input type="hidden" name="moduleBase" value="motifAnnulation">

      <div class="form-group">
        <label for="motif_annulation_nom" class="control-label col-md-4">Nom<span class="red">*</span> :</label>
        <div class="col-md-8">
          <input id="motif_annulation_name" name="motif_annulation_name" required value="{$data.motif_annulation_name}" class="form-control">
        </div>
      </div>
      <div class="form-group center">
        <button type="submit" class="btn btn-primary button-valid">Valider</button>
        {if $data.motif_annulation_id > 0 }
          <button class="btn btn-danger button-delete">Supprimer</button>
        {/if}
      </div>
    {$csrf}</form>
  </div>
</div>
<span class="red">*</span><span class="messagebas">Donnée obligatoire</span>