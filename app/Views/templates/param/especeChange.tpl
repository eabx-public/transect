<h2>Modification d'une espèce</h2>
<a href="especeList">
    <img src="display/images/list.png" height="25">
    Retour à la liste
</a>
<div class="row">
    <div class="col-md-6">
        <form class="form-horizontal" id="especeForm" method="post" action="especeWrite">
            <input type="hidden" name="espece_id" value="{$data.espece_id}">
            <input type="hidden" name="moduleBase" value="espece">

            <div class="form-group">
                <label for="nom" class="control-label col-md-4">
                    <span class="red">*</span>Nom latin :
                </label>
                <div class="col-md-8">
                    <input id="nom" name="nom" value="{$data.nom}" class="form-control" required>
                </div>
            </div>
            <div class="form-group">
                <label for="nom_fr" class="control-label col-md-4">
                    Nom français :
                </label>
                <div class="col-md-8">
                    <input id="nom_fr" name="nom_fr" value="{$data.nom_fr}" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label for="auteur" class="control-label col-md-4">
                    Auteur :
                </label>
                <div class="col-md-8">
                    <input id="auteur" name="auteur" value="{$data.auteur}" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label for="phylum" class="control-label col-md-4">
                    Phylum :
                </label>
                <div class="col-md-8">
                    <input id="phylum" name="phylum" value="{$data.phylum}" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label for="subphylum" class="control-label col-md-4">
                    Subphylum :
                </label>
                <div class="col-md-8">
                    <input id="subphylum" name="subphylum" value="{$data.subphylum}" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label for="classe" class="control-label col-md-4">
                    Classe :
                </label>
                <div class="col-md-8">
                    <input id="classe" name="classe" value="{$data.classe}" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label for="ordre" class="control-label col-md-4">
                    Ordre :
                </label>
                <div class="col-md-8">
                    <input id="ordre" name="ordre" value="{$data.ordre}" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label for="famille" class="control-label col-md-4">
                    Famille :
                </label>
                <div class="col-md-8">
                    <input id="famille" name="famille" value="{$data.famille}" class="form-control">
                </div>
            </div>

            <div class="form-group">
                <label for="genre" class="control-label col-md-4">
                    Genre :
                </label>
                <div class="col-md-8">
                    <input id="genre" name="genre" value="{$data.genre}" class="form-control">
                </div>
            </div>

            <div class="form-group">
                <label for="code_perm_ifremer" class="control-label col-md-4">
                    Code PERM IFREMER :
                </label>
                <div class="col-md-8">
                    <input id="code_perm_ifremer" name="code_perm_ifremer" value="{$data.code_perm_ifremer}" class="form-control">
                </div>

            </div>
            <div class="form-group">
                <label for="code_sandre" class="control-label col-md-4">
                    Code SANDRE :
                </label>
                <div class="col-md-8">
                    <input id="code_sandre" name="code_sandre" value="{$data.code_sandre}" class="form-control">
                </div>
            </div>

            <div class="form-group center">
                <button type="submit" class="btn btn-primary button-valid">Valider</button>
                {if $data.espece_id > 0 }
                <button class="btn btn-danger button-delete">Supprimer</button>
                {/if}
            </div>
        {$csrf}</form>
    </div>
</div>
<span class="red">*</span><span class="messagebas">Donnée obligatoire</span>