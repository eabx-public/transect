<script>
setDataTables("lastTraitPeche");
</script>
<table id="lastTraitPeche" class="tableaffichage">
<thead>
<tr>
<th>Heure début</th>
<th>Heure fin</th>
<th>Nom site transect</th>
<th>Nom station</th>
<th>Rive</th>
<th>Position<br>de l'engin</th>
</tr>
</thead><tbody>
{section name=lst loop=$data}
<tr>
<td>{$data[lst].trait_debut}</td>
<td>{$data[lst].trait_fin}</td>
<td>{$data[lst].site_transect_name}</td>
<td>{$data[lst].station_nom}</td>
<td>{$data[lst].rive_libelle}</td>
<td>{$data[lst].position_engin_name}</td>
</tr>
{/section}
</tbody>
</table>