<?php

namespace App\Controllers;

use \Ppci\Controllers\PpciController;
use App\Libraries\EspeceType as LibrariesEspeceType;

class EspeceType extends PpciController
{
    protected $lib;
    function __construct()
    {
        $this->lib = new LibrariesEspeceType();
    }
    function getListAjaxJson()
    {
        return $this->lib->getListAjaxJson();
    }
    function list()
    {
        return $this->lib->list();
    }
    function change()
    {
        return $this->lib->change();
    }
    function write()
    {
        return $this->lib->write();
    }
    function delete()
    {
        return $this->lib->delete();
    }
}
