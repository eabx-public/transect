<?php

namespace App\Controllers;

use \Ppci\Controllers\PpciController;
use App\Libraries\Peche as LibrariesPeche;

class Peche extends PpciController
{
    protected $lib;
    function __construct()
    {
        $this->lib = new LibrariesPeche();
    }
    function changeComment()
    {
        return $this->lib->changeComment();
    }
    function write()
    {
        return $this->lib->write();
    }
    function writeComment() {
        return $this->lib->writeComment();
    }
    function change()
    {
        return $this->lib->change();
    }
    function delete()
    {
        return $this->lib->delete();
    }
    function completedWrite()
    {
        return $this->lib->completedWrite();
    }
    function generateCSfile()
    {
        return $this->lib->generateCSfile();
    }
    function sendSamplesToCollecScience()
    {
        return $this->lib->sendSamplesToCollecScience();
    }
}
