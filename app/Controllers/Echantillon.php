<?php

namespace App\Controllers;

use \Ppci\Controllers\PpciController;
use App\Libraries\Echantillon as LibrariesEchantillon;

class Echantillon extends PpciController
{
    protected $lib;
    function __construct()
    {
        $this->lib = new LibrariesEchantillon();
    }
    function list()
    {
        return $this->lib->list();
    }
    function change()
    {
        return $this->lib->change();
    }
    function write()
    {
        return $this->lib->write();
    }
    function delete()
    {
        return $this->lib->delete();
    }
    function CSFile()
    {
        return $this->lib->CSFile();
    }
    function sendIndividusToCollecScience()
    {
        return $this->lib->sendIndividusToCollecScience();
    }
    function printLabel()
    {
        return $this->lib->printLabel();
    }
}
