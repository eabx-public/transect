<?php

namespace App\Controllers;

use \Ppci\Controllers\PpciController;
use App\Libraries\Sonde as LibrariesSonde;
use App\Libraries\Trait_op;

class Sonde extends PpciController
{
    protected $lib;
    function __construct()
    {
        $this->lib = new LibrariesSonde();
    }
    function hydrolab()
    {
        $this->lib->hydrolab();
        $trait = new Trait_op;
        return $trait->list();
    }
}
