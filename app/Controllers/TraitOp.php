<?php

namespace App\Controllers;

use \Ppci\Controllers\PpciController;
use App\Libraries\Trait_op as LibrariesTrait;

class TraitOp extends PpciController
{
    protected $lib;
    function __construct()
    {
        $this->lib = new LibrariesTrait();
    }
    function list()
    {
        return $this->lib->list();
    }
    function pecheList()
    {
        return $this->lib->pecheList();
    }
    function importExcel()
    {
        return $this->lib->importExcel();
    }
    function importXml()
    {
        return $this->lib->importXml();
    }
    function importSonde()
    {
        return $this->lib->importSonde();
    }
    function traitPdf()
    {
        return $this->lib->traitPdf();
    }
    function display()
    {
        return $this->lib->display();
    }
    function change()
    {
        return $this->lib->change();
    }
    function write()
    {
        return $this->lib->write();
    }
    function delete()
    {
        return $this->lib->delete();
    }
}
