<?php

namespace App\Controllers;

use \Ppci\Controllers\PpciController;
use App\Libraries\Campagne as LibrariesCampagne;

class Campagne extends PpciController
{
    protected $lib;
    function __construct()
    {
        $this->lib = new LibrariesCampagne();
    }
    function list()
    {
        return $this->lib->list();
    }
    function change()
    {
        return $this->lib->change();
    }
    function write()
    {
        return $this->lib->write();
    }
    function delete()
    {
        return $this->lib->delete();
    }
    function getCompteRendu()
    {
        return $this->lib->getCompteRendu();
    }
}
