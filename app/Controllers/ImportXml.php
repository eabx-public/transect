<?php

namespace App\Controllers;

use \Ppci\Controllers\PpciController;
use App\Libraries\ImportXml as LibrariesImportXml;

class ImportXml extends PpciController
{
    protected $lib;
    function __construct()
    {
        $this->lib = new LibrariesImportXml();
    }
    function import()
    {
        return $this->lib->import();
    }
}
