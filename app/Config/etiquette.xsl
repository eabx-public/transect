<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:fo="http://www.w3.org/1999/XSL/Format">
    <xsl:output method="xml" indent="yes" />
    <xsl:template match="objects">
        <fo:root>
            <fo:layout-master-set>
                <fo:simple-page-master master-name="label" page-height="4cm" page-width="9cm" margin-left="0.5cm" margin-top="0.5cm" margin-bottom="0cm" margin-right="0cm">
                    <fo:region-body />
                </fo:simple-page-master>
            </fo:layout-master-set>

            <fo:page-sequence master-reference="label">
                <fo:flow flow-name="xsl-region-body">
                    <fo:block>
                        <xsl:apply-templates select="object" />
                    </fo:block>

                </fo:flow>
            </fo:page-sequence>
        </fo:root>
    </xsl:template>
    <xsl:template match="object">

        <fo:table table-layout="fixed" border-collapse="collapse" border-style="none" width="8cm" keep-together.within-page="always">
            <fo:table-column column-width="2.5cm" />
            <fo:table-column column-width="6cm" />
            <fo:table-body border-style="none">
                <fo:table-row>
                    <fo:table-cell>
                        <fo:block>
                            <fo:external-graphic>
                                <xsl:attribute name="src">
                                    <xsl:value-of select="concat(individu_id,'.png')" />
                                </xsl:attribute>
                                <xsl:attribute name="content-height">scale-to-fit</xsl:attribute>
                                <xsl:attribute name="height">2cm</xsl:attribute>
                                <xsl:attribute name="content-width">2cm</xsl:attribute>
                                <xsl:attribute name="scaling">uniform</xsl:attribute>
                            </fo:external-graphic>
                        </fo:block>
                    </fo:table-cell>
                    <fo:table-cell font-size="10pt">
                        <fo:block>
                            <fo:inline font-weight="bold">INRAE</fo:inline>
                        </fo:block>
                        <fo:block>TRANSECT:<fo:inline font-weight="bold">
                            <xsl:value-of select="individu_id" />
                        </fo:inline>
                    </fo:block>
                    <fo:block>
                        <xsl:value-of select="taxon" />
                    </fo:block>
                    <fo:block>lf:<fo:inline>
                        <xsl:value-of select="longueur_fourche"/>
                    </fo:inline>
                </fo:block>
                <fo:block>poids(g):<fo:inline>
                    <xsl:value-of select="individu_masse"/>
                </fo:inline>
            </fo:block>
        </fo:table-cell>
    </fo:table-row>
</fo:table-body>
</fo:table>
<fo:block margin-left="0.5cm">
<fo:inline font-weight="bold" font-size="10pt">
    <xsl:value-of select="radical" />
</fo:inline>
</fo:block>

<fo:block page-break-after="always" />

</xsl:template>
</xsl:stylesheet>