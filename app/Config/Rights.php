<?php

namespace App\Config;

use Ppci\Config\RightsPpci;

/**
 * List of all rights required by modules
 */
class Rights extends RightsPpci
{
    protected array $rights = [
        "dbstructureHtml" => ["param"],
        "dbstructureGacl" => ["admin"],
        "dbstructureLatex" => ["param"],
        "dbstructureSchema" => ["param"],
        "importExcelImport" => ["import"],
        "importXmlImport" => ["import"],
        "traitList" => ["consult"],
        "traitPecheList" => ["consult"],
        "traitImportExcel" => ["import"],
        "traitImportXml" => ["import"],
        "traitImportSonde" => ["import"],
        "traitPdf" => ["consult"],
        "traitDisplay" => ["consult"],
        "traitChange" => ["manage"],
        "traitWrite" => ["manage"],
        "traitDelete" => ["manage"],
        "importSondeHydrolab" => ["import"],
        "echantillonList" => ["consult"],
        "echantillonChange" => ["labo", "manage"],
        "echantillonWrite" => ["labo", "manage"],
        "echantillonDelete" => ["labo"],
        "echantillonCSFile" => ["labo"],
        "echantillonSendIndividusToCollecScience" => ["labo"],
        "echantillonPrintLabel" => ["labo"],
        "pecheChangeComment" => ["labo", "manage"],
        "pecheWriteComment" => ["labo", "manage"],
        "pecheChange" => ["manage"],
        "pecheWrite" => ["manage"],
        "pecheDelete" => ["manage"],
        "pecheCompletedWrite" => ["labo", "manage"],
        "pecheGenerateCSfile" => ["labo"],
        "pecheSendSamplesToCollecScience" => ["labo"],
        "especeTypeSearchAjax" => ["consult"],
        "personneList" => ["param"],
        "personneChange" => ["param"],
        "personneWrite" => ["param"],
        "personneDelete" => ["param"],
        "campagneStatutList" => ["param"],
        "campagneStatutChange" => ["param"],
        "campagneStatutWrite" => ["param"],
        "campagneStatutDelete" => ["param"],
        "motifAnnulationList" => ["param"],
        "motifAnnulationChange" => ["param"],
        "motifAnnulationWrite" => ["param"],
        "motifAnnulationDelete" => ["param"],
        "campagneList" => ["manage"],
        "campagneChange" => ["manage"],
        "campagneWrite" => ["manage"],
        "campagneDelete" => ["manage"],
        "campagneGetCompteRendu" => ["manage"],
        "especeTypeList" => ["param"],
        "especeTypeChange" => ["param"],
        "especeTypeWrite" => ["param"],
        "especeTypeDelete" => ["param"],
        "especeList" => ["param"],
        "especeChange" => ["param"],
        "especeWrite" => ["param"],
        "especeDelete" => ["param"],
    ];
}
