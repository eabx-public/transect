<?php

use CodeIgniter\Router\RouteCollection;

/**
 * @var RouteCollection $routes
 */
//$routes->get('/', 'Home::index');
$routes->add('dbstructureHtml', '\Ppci\Controllers\Miscellaneous::structureHtml');
$routes->add('dbstructureLatex', '\Ppci\Controllers\Miscellaneous::structureLatex');
$routes->add('dbstructureSchema', '\Ppci\Controllers\Miscellaneous::structureSchema');
$routes->add('importExcelImport', 'ImportExcel::import');
$routes->add('importXmlImport', 'ImportXml::import');
$routes->add('traitList', 'TraitOp::list');
$routes->add('traitPecheList', 'TraitOp::pecheList');
$routes->add('traitImportExcel', 'TraitOp::importExcel');
$routes->add('traitImportXml', 'TraitOp::importXml');
$routes->add('traitImportSonde', 'TraitOp::importSonde');
$routes->add('traitPdf', 'TraitOp::traitPdf');
$routes->add('traitDisplay', 'TraitOp::display');
$routes->add('traitChange', 'TraitOp::change');
$routes->post('traitWrite', 'TraitOp::write');
$routes->post('traitDelete', 'TraitOp::delete');
$routes->add('importSondeHydrolab', 'Sonde::hydrolab');
$routes->add('echantillonList', 'Echantillon::list');
$routes->add('echantillonChange', 'Echantillon::change');
$routes->post('echantillonWrite', 'Echantillon::write');
$routes->post('echantillonDelete', 'Echantillon::delete');
$routes->add('echantillonCSFile', 'Echantillon::CSFile');
$routes->add('echantillonSendIndividusToCollecScience', 'Echantillon::sendIndividusToCollecScience');
$routes->add('echantillonPrintLabel', 'Echantillon::printLabel');
$routes->add('pecheChangeComment', 'Peche::changeComment');
$routes->post('pecheWriteComment', 'Peche::writeComment');
$routes->add('pecheChange', 'Peche::change');
$routes->post('pecheWrite', 'Peche::write');
$routes->post('pecheDelete', 'Peche::delete');
$routes->add('pecheCompletedWrite', 'Peche::completedWrite');
$routes->add('pecheGenerateCSfile', 'Peche::generateCSfile');
$routes->add('pecheSendSamplesToCollecScience', 'Peche::sendSamplesToCollecScience');
$routes->add('especeTypeSearchAjax', 'EspeceType::getListAjaxJson');
$routes->add('parametres', '\Ppci\Controllers\Utils::submenu/parametres');
$routes->add('aide', '\Ppci\Controllers\Utils::submenu/aide');
$routes->add('personneList', 'Personne::list');
$routes->add('personneChange', 'Personne::change');
$routes->post('personneWrite', 'Personne::write');
$routes->post('personneDelete', 'Personne::delete');
$routes->add('campagneStatutList', 'CampagneStatut::list');
$routes->add('campagneStatutChange', 'CampagneStatut::change');
$routes->post('campagneStatutWrite', 'CampagneStatut::write');
$routes->post('campagneStatutDelete', 'CampagneStatut::delete');
$routes->add('motifAnnulationList', 'MotifAnnulation::list');
$routes->add('motifAnnulationChange', 'MotifAnnulation::change');
$routes->post('motifAnnulationWrite', 'MotifAnnulation::write');
$routes->post('motifAnnulationDelete', 'MotifAnnulation::delete');
$routes->add('campagneList', 'Campagne::list');
$routes->add('campagneChange', 'Campagne::change');
$routes->post('campagneWrite', 'Campagne::write');
$routes->post('campagneDelete', 'Campagne::delete');
$routes->add('campagneGetCompteRendu', 'Campagne::getCompteRendu');
$routes->add('especeTypeList', 'EspeceType::list');
$routes->add('especeTypeChange', 'EspeceType::change');
$routes->post('especeTypeWrite', 'EspeceType::write');
$routes->post('especeTypeDelete', 'EspeceType::delete');
$routes->add('especeList', 'Espece::list');
$routes->add('especeChange', 'Espece::change');
$routes->post('especeWrite', 'Espece::write');
$routes->post('especeDelete', 'Espece::delete');

