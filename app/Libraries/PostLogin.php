<?php

namespace App\Libraries;

use Ppci\Libraries\PpciLibrary;

class PostLogin extends PpciLibrary
{
    static function index()
    {
        $dureeVie = 3600 * 24; // Suppression de tous les fichiers de plus de 24 heures
        /*
        * Ouverture du dossier
        */
        $APPLI_temp = WRITEPATH . "temp";
        $dossier = opendir($APPLI_temp);
        while (false !== ($entry = readdir($dossier))) {
            $path = $APPLI_temp . "/" . $entry;
            $file = fopen($path, 'r');
            $stat = fstat($file);
            $atime = $stat["atime"];
            fclose($file);
            $infos = pathinfo($path);
            if (! is_dir($path) && ($infos["basename"] != ".htaccess") && ($infos["basename"] != ".gitkeep")) {
                $age = time() - $atime;
                if ($age > $dureeVie) {
                    unlink($path);
                }
            }
        }
        closedir($dossier);
    }
}
