<?php

namespace App\Libraries;

use App\Models\Echantillon as ModelsEchantillon;
use App\Models\Individu;
use App\Models\Peche;
use App\Models\TraitPeche;
use Ppci\Libraries\PpciException;
use Ppci\Libraries\PpciLibrary;
use Ppci\Models\PpciModel;

class Echantillon extends PpciLibrary
{
    /**
     * @var ModelsEchantillon
     */
    protected PpciModel $dataclass;
    public $keyName;
    function __construct()
    {
        parent::__construct();
        $this->dataclass = new ModelsEchantillon();
        $this->keyName = "echantillon_id";
        if (isset($_REQUEST[$this->keyName])) {
            $this->id = $_REQUEST[$this->keyName];
        }
    }
    function change()
    {
        $this->vue = service('Smarty');
        if ($_REQUEST["peche_id"] > 0) {

            $traitPeche = new TraitPeche();
            $dataPeche = $traitPeche->lire($_REQUEST["peche_id"]);
            $peche = new Peche();
            $this->vue->set($peche->getListeFromTrait($dataPeche["trait_id"]), "peches");
            $this->vue->set($dataPeche, "dataPeche");
            $this->vue->set($this->dataclass->getListFromPeche($_REQUEST["peche_id"]), "dataEchan");

            $individu = new Individu();
            $this->vue->set($individu->getListFromPeche($_REQUEST["peche_id"]), "dataIndividu");
            $this->vue->set("trait/echantillon.tpl", "corps");
            $this->vue->set($_REQUEST["trait_id"], "trait_id");

            $data = $this->dataclass->lire($this->id, true, $_REQUEST["peche_id"]);
            $data["volume_filtre"] = $dataPeche["volume_filtre"];
            $this->vue->set($data, "data");
            $this->vue->set(1, "change");
            $this->vue->set($individu->getListFromEchantillon($this->id), "individus");
            return $this->vue->send();
        } else {
            return defaultPage();
        }
    }
    function list()
    {
        $this->vue = service('Smarty');
        if ($_REQUEST["peche_id"] > 0) {
            $traitPeche = new TraitPeche();
            $dataPeche = $traitPeche->lire($_REQUEST["peche_id"]);
            $peche = new Peche();
            $this->vue->set($peche->getListeFromTrait($dataPeche["trait_id"]), "peches");
            $this->vue->set($dataPeche, "dataPeche");
            $this->vue->set($this->dataclass->getListFromPeche($_REQUEST["peche_id"]), "dataEchan");

            $individu = new Individu();
            $this->vue->set($individu->getListFromPeche($_REQUEST["peche_id"]), "dataIndividu");
            $this->vue->set("trait/echantillon.tpl", "corps");
            $this->vue->set($_REQUEST["trait_id"], "trait_id");
            return $this->vue->send();
        } else {
            return defaultPage();
        }
    }
    function write()
    {
        try {
            $this->dataWrite($_REQUEST);
            $this->id = 0;
            return $this->change();
        } catch (PpciException) {
            return $this->change();
        }
    }

    function delete()
    {
        try {
            $this->dataDelete($this->id);
            return $this->change();
        } catch (PpciException $e) {
            return $this->change();
        }
    }
    function CSFile()
    {
        /*
         * Genere le fichier pret a etre importe dans Collec-Science
         * pour creer les individus lyophilises rattaches a un echantillon
         */
        try {
            $this->vue = service("CsvView");
            $data = $this->dataclass->generateCSfile($this->id, $_REQUEST["peche_id"]);
            $this->vue->set($data);
            $this->vue->setDelimiter(";");
        } catch (PpciException $e) {
            unset($this->vue);
            $this->message->set($e->getMessage());
            return $this->change();
        }
    }
    function sendIndividusToCollecScience()
    {
        $individu = new Individu();
        $individus = $individu->getListFromEchantillon($this->id);
        $CURL_debugmode = $this->appConfig->CURL_debugmode;
        if (!empty($individus)) {
            $listmois = array(
                "01" => "JANVIER",
                "02" => "FEVRIER",
                "03" => "MARS",
                "04" => "AVRIL",
                "05" => "MAI",
                "06" => "JUIN",
                "07" => "JUILLET",
                "08" => "AOUT",
                "09" => "SEPTEMBRE",
                "10" => "OCTOBRE",
                "11" => "NOVEMBRE",
                "12" => "DECEMBRE"
            );
            $annee = substr($individus[0]["trait_debut"], 0, 4);
            $mois = $listmois[substr($individus[0]["trait_debut"], 5, 2)];
            $name = strtoupper(
                str_replace(
                    " ",
                    "_",
                    $individus[0]["site_transect_name"] . "-" . $individus[0]["rive_libelle"] . "-" . $individus[0]["position_engin_name"] . "-" . $mois . "-" . $annee
                )
            );
            $uidMin = 99999999;
            $uidMax = 0;
            try {
                foreach ($individus as $ind) {
                    $data = array(
                        "dbuid_origin" => "transect:" . $ind["individu_id"],
                        "identifier" => $name . "-" . $ind["individu_id"],
                        "md_taxon" => $ind["nom"],
                        "parent_uuid" => $ind["parent_uuid"],
                        "sampling_date" => $ind["trait_debut"],
                        "sample_type_name" => $_SESSION["dbparams"]["cs_sample_name_individu"],
                        "collection_name" => $_SESSION["dbparams"]["cs_collection_name"],
                        "login" => $_SESSION["dbparams"]["cs_login"],
                        "token" => $_SESSION["dbparams"]["cs_token"]
                    );
                    $result_json = apiCall("POST", $_SESSION["dbparams"]["cs_update"], "", $data, $CURL_debugmode);
                    $result = json_decode($result_json, true);
                    if (empty($result)) {
                        throw new PpciException(_("L'opération n'a pas abouti pour une cause inconnue, aucune information technique n'est disponible"));
                    } elseif ($result["error_code"] != 200) {
                        throw new PpciException(sprintf(
                            _("L'erreur %1s a été générée lors du traitement de l'individu %2s : %3s"),
                            $result["error_code"],
                            $ind["individu_id"],
                            $result["error_message"] . " " . $result["error_detail"]
                        ));
                    }
                    $uid = $result['uid'];
                    if ($uid > 0) {
                        if ($uid < $uidMin) {
                            $uidMin = $uid;
                        }
                        if ($uid > $uidMax) {
                            $uidMax = $uid;
                        }
                    }
                }
                $this->message->set("Premier UID créé ou mis à jour : " . $uidMin);
                $this->message->set("Dernier UID créé ou mis à jour : " . $uidMax);
            } catch (PpciException $e) {
                $this->message->set($e->getMessage(), true);
            }
            return $this->change();
        } else {
            $this->message->set("Il n'existe pas d'individus pour l'échantillon sélectionné", true);
            return $this->change();
        }
    }

    function printLabel()
    {
        $individu = new Individu();
        try {
            $this->vue = service("PdfView");
            $this->vue->setFilename($individu->generatePdfFromEchan($this->id));
            $this->vue->setDisposition("inline");
            return $this->vue->send();
        } catch (PpciException $e) {
            unset($this->vue);
            $this->message->set($e->getMessage());
            return $this->change();
        }
    }
}
