<?php

namespace App\Libraries;

use App\Models\Personne as ModelsPersonne;
use Ppci\Libraries\PpciException;
use Ppci\Libraries\PpciLibrary;
use Ppci\Models\PpciModel;

class Personne extends PpciLibrary
{
	/**
	 * @var ModelsPersonne
	 */
	protected PpciModel $dataclass;
	public $keyName;
	function __construct()
	{
		parent::__construct();
		$this->dataclass = new ModelsPersonne();
		$this->keyName = "personne_id";
		if (isset($_REQUEST[$this->keyName])) {
			$this->id = $_REQUEST[$this->keyName];
		}
	}

	function list()
	{
		$this->vue = service('Smarty');
		$this->vue->set($this->dataclass->getListe(2), "data");
		$this->vue->set("param/personneList.tpl", "corps");
		return $this->vue->send();
	}
	function change()
	{
		$this->vue = service('Smarty');
		$this->dataRead($this->id, "param/personneChange.tpl");
		return $this->vue->send();
	}
	function write()
	{
		try {
			$this->id = $this->dataWrite($_REQUEST);
			if ($this->id > 0) {
				$_REQUEST[$this->keyName] = $this->id;
				return $this->list();
			} else {
				return $this->change();
			}
		} catch (PpciException) {
			return $this->change();
		}
	}
	function delete()
	{
		try {
			$this->dataDelete($this->id);
			return $this->list();
		} catch (PpciException $e) {
			return $this->change();
		}
	}
}
