<?php

namespace App\Libraries;

use App\Models\CampagneStatut as ModelsCampagneStatut;
use Ppci\Libraries\PpciException;
use Ppci\Libraries\PpciLibrary;
use Ppci\Models\PpciModel;

class CampagneStatut extends PpciLibrary
{
    /**
     * @var ModelsCampagneStatut
     */
    protected PpciModel $dataclass;
    public $keyName;
    function __construct()
    {
        parent::__construct();
        $this->dataclass = new ModelsCampagneStatut();
        $this->keyName = "campagne_statut_id";
        if (isset($_REQUEST[$this->keyName])) {
            $this->id = $_REQUEST[$this->keyName];
        }
    }


    function list()
    {
        $this->vue = service('Smarty');
        $this->vue->set($this->dataclass->getListe(1), "data");
        $this->vue->set("param/campagneStatutList.tpl", "corps");
        return $this->vue->send();
    }
    function change()
    {
        $this->vue = service('Smarty');
        $this->dataRead($this->id, "param/campagneStatutChange.tpl");
        return $this->vue->send();
    }
    function write()
    {
        try {
            $this->id = $this->dataWrite($_REQUEST);
            if ($this->id > 0) {
                $_REQUEST[$this->keyName] = $this->id;
                return $this->list();
            } else {
                return $this->change();
            }
        } catch (PpciException) {
            return $this->change();
        }
    }
    function delete()
    {
        try {
            $this->dataDelete($this->id);
            return $this->list();
        } catch (PpciException $e) {
            return $this->change();
        }
    }
}
