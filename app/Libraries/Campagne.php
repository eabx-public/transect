<?php

namespace App\Libraries;

use App\Models\Campagne as ModelsCampagne;
use App\Models\CampagneStatut;
use App\Models\Experimentation;
use App\Models\MotifAnnulation;
use Ppci\Libraries\PpciException;
use Ppci\Libraries\PpciLibrary;
use Ppci\Models\PpciModel;

class Campagne extends PpciLibrary
{
    /**
     * @var ModelsCampagne
     */
    protected PpciModel $dataclass;
    public $keyName;
    function __construct()
    {
        parent::__construct();
        $this->dataclass = new ModelsCampagne();
        $this->keyName = "campagne_id";
        if (isset($_REQUEST[$this->keyName])) {
            $this->id = $_REQUEST[$this->keyName];
        }
    }

    function list()
    {
        $this->vue = service('Smarty');
        ($this->vue->set($this->dataclass->getLastCampaigns(0, 0), "data"));
        $this->vue->set("trait/campagneList.tpl", "corps");
        $this->vue->send();
    }

    function change()
    {
        $this->vue = service('Smarty');
        $this->dataRead($this->id, "trait/campagneChange.tpl");
        $cs = new CampagneStatut();
        $this->vue->set($cs->getListe(1), "statuts");
        $ma = new MotifAnnulation();
        $this->vue->set($ma->getListe(1), "motifs");
        $experimentation = new Experimentation();
        $this->vue->set($experimentation->getListe(2), "experimentations");
        return $this->vue->send();
    }
    function write()
    {
        try {
            $this->id = $this->dataWrite($_REQUEST);
            if ($this->id > 0) {
                $_REQUEST[$this->keyName] = $this->id;
                return $this->list();
            } else {
                return $this->change();
            }
        } catch (PpciException) {
            return $this->change();
        }
    }

    function delete()
    {
        try {
            $this->dataDelete($this->id);
            return $this->list();
        } catch (PpciException $e) {
            return $this->change();
        }
    }
    function getCompteRendu()
    {
        /**
         * Download compte-rendu
         */

        if ($this->id > 0) {
            $data = $this->dataclass->getBinaryField($this->id, "compte_rendu");
            if (!empty($data)) {
                $filename = "transect-compte-rendu-campagne-$this->id.pdf";
                header('Content-Type: application/pdf');
                header('Content-Disposition: attachment; filename="' . $filename . '"');
                header('Expires: 0');
                header('Cache-Control: must-revalidate');
                header('Pragma: no-cache');
                $handle = fopen("php://output", 'w');
                ob_clean();
                flush();
                fwrite($handle, $data);
                exit();
            } else {
                $this->message->set("Un problème est survenu en tentant de lire le fichier PDF associé à la campagne", true);
                return $this->list();
            }
        }
    }
}
