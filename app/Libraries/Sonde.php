<?php

namespace App\Libraries;

use App\Models\Hydrolab;
use Ppci\Libraries\PpciException;
use Ppci\Libraries\PpciLibrary;
use Ppci\Models\PpciModel;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Sonde extends PpciLibrary
{
	/**
	 * @var Hydrolab
	 */
	protected PpciModel $dataclass;
	public $keyName;
	function __construct()
	{
		parent::__construct();
		$this->dataclass = new Hydrolab();
		$this->id = $_REQUEST["trait_id"];
	}

	function hydrolab()
	{
		$extension = array(
			"xlsx"
		);
		if (upload_verif('upfile', $extension) != -1 && $this->id > 0 && is_numeric($this->id)) {
			$i = 0;
			try {
				/*
			 * Suppression des donnees deja telechargees
			 */
				$this->dataclass->supprimerChamp($this->id, "trait_id");
				$excel = \PhpOffice\PhpSpreadsheet\IOFactory::load($_FILES['upfile']['tmp_name']);

				$dataExcel = $excel->setActiveSheetIndex(0)->toArray(null, true, false, true);
				date_default_timezone_set("UTC");
				$colonnes = $this->appConfig->hydrolab;
				$nbColonne = count($colonnes);
				if ($nbColonne == 0) {
					throw new PpciException("La configuration de la sonde hydrolab n'a pas pu être récupérée depuis les paramètres de l'application");
				}
				$listeColonnes = array(
					"A",
					"B",
					"C",
					"D",
					"E",
					"F",
					"G",
					"H",
					"I",
					"J"
				);
				$t_data = array();
				foreach ($listeColonnes as $colonne) {
					$colonneExcel = explode(" ", $dataExcel[1][$colonne]);
					$t_data[$colonne] = $colonnes[$colonneExcel[0]];
				}
				$nbLigne = count($dataExcel);
				for ($i = 2; $i <= $nbLigne; $i++) {
					$data = array();
					$data["trait_id"] = $this->id;
					$data["hydrolab_id"] = 0;
					/*
					 * Verification qu'il y a bien des donnees
					 */
					if ($dataExcel[$i]["A"] > 0) {
						/*
						 * Calcul de la date-heure Le nombre de jours depuis le 1/1/1900 est le nombre avant la virgule, les secondes sont une fraction du chiffre après la virgule
						 */
						$ts = $dataExcel[$i]["A"];
						/*
						 * Transformation de la date excel
						 */
						$data["releve_date"] = date("d/m/Y H:i:s", round(($ts - 25569) * 86400));

						/*
						 * Traitement des valeurs numériques
						 */
						foreach ($t_data as $key => $value) {
							if (strlen($value) > 0) {
								/*
							 * Remplacement des virgules par des points
							 */
								$val = str_replace(",", ".", $dataExcel[$i][$key]);
								$data[$value] = $val;
							}
						}
						/*
						 * Ecriture
						 */
						$ret = $this->dataclass->ecrire($data);
						if (! $ret > 0) {
							$this->message->set("Erreur d'écriture de la ligne $i dans la table hydrolab");
						}
					}
				}
				$this->message->set(($nbLigne - 1) . " lignes traitées.");
			} catch (PpciException $e) {
				$this->message->set($e->getMessage(), true);
				$this->message->set("Ligne en cours de traitement : $i");
			}
		}
	}
}
