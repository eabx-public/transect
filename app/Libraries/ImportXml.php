<?php

namespace App\Libraries;

use App\Libraries\Trait_op as LibrariesTrait_op;
use App\Models\Campagne;
use App\Models\Peche;
use App\Models\Trait_op;
use App\Models\TraitPeche;
use App\Models\Xml;
use Ppci\Libraries\PpciException;
use Ppci\Libraries\PpciLibrary;
use Ppci\Models\PpciModel;

class ImportXml extends PpciLibrary
{
	/**
	 * @var Peche
	 */
	protected PpciModel $dataclass;
	public $keyName;	public $warnings;

	function __construct()
	{
		parent::__construct();
		$this->dataclass = new Peche();
		$this->keyName = "peche_id";
		if (isset($_REQUEST[$this->keyName])) {
			$this->id = $_REQUEST[$this->keyName];
		}
	}

	function list()
	{
		$this->vue = service('Smarty');
		$this->dataclass = new TraitPeche();
		$this->vue->set($this->dataclass->getLastTrait(100), "data");
		$this->vue->set("importDataBateau/xmlFileSelect.tpl", "corps");
		return $this->vue->send();
	}
	function import()
	{
		$libraryTrait = new LibrariesTrait_op;
		$campagne = new Campagne();
		$extension = ["xml"];
		if (upload_verif("upfile", $extension) != -1 && $_REQUEST["campagne_id"] > 0) {
			try {
				/*
			 * Chargement des données dans un tableau
			 */
				$xml = new Xml();
				$dataXml = $xml->loadFile($_FILES['upfile']['tmp_name']);
				$dataXml = $dataXml["trait"];
				if (!empty($dataXml)) {
					$dcampagne = $campagne->lire($_REQUEST["campagne_id"]);
					/**
					 * Lancement de l'intégration des données dans la base
					 */
					$trait = new Trait_op();
					$peche = new Peche();
					$i = 0;
					$dateDebut = \DateTime::createFromFormat('Y-m-d', '2050-31-12');
					$dateFin = \DateTime::createFromFormat('Y-m-d', '1950-01-01');
					$warnings = array();
					$positions = array(2 => "babord", 3 => "tribord", 4 => "fond");
					foreach ($dataXml as $k => $v) {
						$ok = 1;
						/**
						 * Suppression des infos concernant la peche - traitement du trait pour le moment
						 */
						$dataTrait = $v;
						//unset($dataTrait[0]);
						unset($dataTrait["peche"]);
						/*
					 * Rajout des paramètres par défaut
					 */
						$dataTrait["protocole_id"] = $this->appConfig->XMLDefaultProtocol;
						$dataTrait["bateau_id"] = $this->appConfig->XMLDefaultBoat;
						$dataTrait["experimentation_id"] = $this->appConfig->XMLDefaultExperimentation;
						$dataTrait["trait_nom"] = "Peche";
						$dataTrait["campagne_id"] = $_REQUEST["campagne_id"];
						$datePeche = \DateTime::createFromFormat('d/m/Y H:i:s', $dataTrait["trait_debut"]);
						if ($datePeche < $dateDebut) {
							$dateDebut = $datePeche;
						}
						if ($datePeche > $dateFin) {
							$dateFin = $datePeche;
						}
						/*
					 * Enregistrement du trait
					 */
						$trait_id = $trait->importXml($dataTrait);
						if ($trait_id > 0) {
							/*
						 * Enregistrement des opérations de peche
						 */
							foreach ($v['peches']['peche'] as $kp => $dataPeche) {
								$dataPeche["trait_id"] = $trait_id;
								/*
							 * Gestion des parametres par defaut
							 */
								if ($dataPeche["position_engin_id"] == 2 || $dataPeche["position_engin_id"] == 3) {
									$dataPeche["materiel_id"] = 11;
								} else {
									$dataPeche["materiel_id"] = 12;
								}
								$peche_id = $peche->importData($dataPeche);
								if (count($peche->warning) > 0) {
									$dtrait = $trait->getDetail($trait_id);
									$warning = array(
										"trait_id" => $trait_id,
										"date" => $dataTrait["trait_debut"],
										"site" => $dtrait["site_transect_name"],
										"rive" => $dtrait["rive_libelle"],
										"position" => $positions[$dataPeche["position_engin_id"]],
										"message" => ""
									);
									foreach ($peche->warning as $dwarning) {
										$warning["message"] .= $dwarning . PHP_EOL;
									}
									$this->warnings[] = $warning;
								}
								if ($peche_id < 1) {
									$ok = 0;
									$messageErreur = $peche->getErrorData(1);
									$this->message->set("Problème d'enregistrement de l'opération de pêche
										(trait n°" . $k . ", peche n° " . $kp . ") :
												contactez l'assistance informatique en fournissant le fichier XML utilisé pour l'import", true);
									foreach ($messageErreur as $erreur) {
										$this->message->set($erreur["message"], true);
									}
								}
							}
						} else {
							$messageErreur = $trait->getErrorData(1);
							$ok = 0;
							$this->message->set("Problème d'enregistrement du trait n° " . $k . ".
								Contactez l'assistance informatique, en fournissant le fichier XML utilisé pour l'import", true);
							foreach ($messageErreur as $erreur) {
								$this->message->set($erreur["message"]);
							}
						}
						if ($ok == 1) {
							$i++;
						}
					}
					/**
					 * mise à niveau de la campagne
					 */
					if ($i > 0) {
						$dcampagne["date_debut"] = $dateDebut->format('d/m/Y');
						$dcampagne["duree"] = date_diff($dateDebut, $dateFin)->format("%a") + 1;
						if (!$campagne->ecrire($dcampagne) > 0) {
							$this->message->set("Problème rencontré lors de la mise à jour de la campagne", true);
						};
					}
					$this->message->set($i . " traits importés totalement");
				} else {
					$this->message->set("Le fichier fourni n'a pas pu être téléchargé sur le serveur, ou est vide", true);
				}
			} catch (PpciException $e) {
				$this->message->set($e->getMessage(), true);
			}
		}
		return $libraryTrait->list();
	}
}
