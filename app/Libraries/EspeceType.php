<?php

namespace App\Libraries;

use App\Models\Espece;
use App\Models\EspeceType as ModelsEspeceType;
use App\Models\Type;
use Ppci\Libraries\PpciException;
use Ppci\Libraries\PpciLibrary;
use Ppci\Models\PpciModel;

class EspeceType extends PpciLibrary
{
	/**
	 * @var ModelsEspeceType
	 */
	protected PpciModel $dataclass;
	public $keyName;
	function __construct()
	{
		parent::__construct();
		$this->dataclass = new ModelsEspeceType();
		$this->keyName = "espece_type_id";
		if (isset($_REQUEST[$this->keyName])) {
			$this->id = $_REQUEST[$this->keyName];
		}
	}

	function list()
	{
		$this->vue = service('Smarty');
		$this->vue->set($this->dataclass->getListe(), "data");
		$this->vue->set("param/especeTypeList.tpl", "corps");
		return $this->vue->send();
	}
	function change()
	{
		$this->vue = service('Smarty');
		$data = $this->dataRead($this->id, "param/especeTypeChange.tpl");
		$espece = new Espece();
		$this->vue->set($espece->getListName("nom"), "especes");
		$type = new Type();
		$this->vue->set($type->getListe(), "types");
		return $this->vue->send();
	}
	function write()
	{
		try {
			$this->id = $this->dataWrite($_REQUEST);
			if ($this->id > 0) {
				$_REQUEST[$this->keyName] = $this->id;
				return $this->list();
			} else {
				return $this->change();
			}
		} catch (PpciException) {
			return $this->change();
		}
	}
	function delete()
	{
		try {
			$this->dataDelete($this->id);
			return $this->list();
		} catch (PpciException $e) {
			return $this->change();
		}
	}
	function getListAjaxJson()
	{
		/*
		 * Recherche la liste et la retourne au format Ajax
		 */
		$this->vue = service("AjaxView");
		if (strlen($_REQUEST["libelle"]) > 0) {
			$data = $this->dataclass->getListByName($_REQUEST["libelle"]);
			$dataJson = array();
			$i = 0;
			/*
			 * Mise en forme du tableau pour etre facile a manipuler cote client
			 */
			foreach ($data as $value) {
				$dataJson[$i]["id"] = $value["espece_type_id"];
				$valeur = $value["nom"];
				if (strlen($value["nom_fr"]) > 0) {
					$valeur .= " - " . $value["nom_fr"];
				}
				if (strlen($value["code_csp"]) > 0) {
					$valeur .= " - " . $value["code_csp"];
				}
				if (strlen($value["type_code"]) > 0) {
					$valeur .= " - " . $value["type_code"];
				}
				if (strlen($value["message"]) > 0) {
					$valeur .= " - " . $value["message"];
				}
				$dataJson[$i]["val"] = $valeur;
				$i++;
			}
			$this->vue->set($dataJson);
			return $this->vue->send();
		}
	}
}
