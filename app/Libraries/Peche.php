<?php

namespace App\Libraries;

use App\Libraries\Echantillon as LibrariesEchantillon;
use App\Libraries\Trait_op as LibrariesTrait_op;
use App\Models\Echantillon;
use App\Models\Materiel;
use App\Models\Peche as ModelsPeche;
use App\Models\PositionEngin;
use App\Models\Trait_op;
use App\Models\TraitPeche;
use Ppci\Libraries\PpciException;
use Ppci\Libraries\PpciLibrary;
use Ppci\Models\PpciModel;

class Peche extends PpciLibrary
{
	/**
	 * @var ModelsPeche
	 */
	protected PpciModel $dataclass;
	public $keyName;
	function __construct()
	{
		parent::__construct();
		$this->dataclass = new ModelsPeche();
		$this->keyName = "peche_id";
		if (isset($_REQUEST[$this->keyName])) {
			$this->id = $_REQUEST[$this->keyName];
		}
	}

	function change()
	{
		$this->vue = service('Smarty');
		$this->dataRead($this->id, "trait/pecheChange.tpl", $_REQUEST["trait_id"]);
		/*
		 * Recuperation des tables de parametres
		 */
		$position_engin = new PositionEngin();
		$this->vue->set($position_engin->getListe(1), "position_engin");
		$materiel = new Materiel();
		$this->vue->set($materiel->getListeActif($this->id), "materiel");
		$trait = new Trait_op();
		$this->vue->set($trait->getDetail($_REQUEST["trait_id"]), "trait");
		return $this->vue->send();
	}
	function changeComment()
	{
		$this->vue = service('Smarty');
		$this->dataRead($this->id, "trait/pecheChangeCommentaire.tpl");
		$this->vue->set($_SESSION["dbparams"]["CS_address"], "collecscience");
		return $this->vue->send();
	}
	function write()
	{
		try {
			$this->id = $this->dataWrite($_REQUEST);
			if ($this->id > 0) {
				$_REQUEST[$this->keyName] = $this->id;
				$trait = new LibrariesTrait_op;
				return $trait->list();
			} else {
				return $this->change();
			}
		} catch (PpciException) {
			return $this->change();
		}
	}
	function writeComment() {
		$this->dataWrite($_REQUEST);
		$echantillon = new LibrariesEchantillon;
		return $echantillon->list();
	}
	function completedWrite()
	{
		/*
		 * write record in database
		 */
		$this->dataclass->disableMandatoryField("materiel_id");
		$this->dataclass->disableMandatoryField("position_engin_id");
		$this->id = $this->dataWrite($_REQUEST);
		if ($this->id > 0) {
			$_REQUEST[$this->keyName] = $this->id;
		}
		$echantillon = new LibrariesEchantillon;
		return $echantillon->list();
	}
	function delete()
	{
		try {
			$this->dataDelete($this->id);
			$trait = new LibrariesTrait_op;
			return $trait->list();
		} catch (PpciException $e) {
			return $this->change();
		}
	}
	function sendSamplesToCollecScience()
	{
		$traitPeche = new TraitPeche();
		$traitPeche->autoFormatDate = false;
		$dpeche = $traitPeche->lire($this->id);
		$libEchantillon = new LibrariesEchantillon;
		/*
		 * Verification que la saisie soit bien terminee et l'UID de Collec-Science renseigne
		 */
		if ($dpeche["cs_uid"] > 0 && $dpeche["completed"] == 1) {
			/*
			 * Lecture des echantillons associes
			 */
			$echantillon = new Echantillon();
			$echantillons = $echantillon->getListGroupedFromPeche($this->id);
			$listmois = array(
				"01" => "JANVIER",
				"02" => "FEVRIER",
				"03" => "MARS",
				"04" => "AVRIL",
				"05" => "MAI",
				"06" => "JUIN",
				"07" => "JUILLET",
				"08" => "AOUT",
				"09" => "SEPTEMBRE",
				"10" => "OCTOBRE",
				"11" => "NOVEMBRE",
				"12" => "DECEMBRE"
			);
			$annee = substr($dpeche["trait_debut"], 0, 4);
			$mois = $listmois[substr($dpeche["trait_debut"], 5, 2)];
			$name = $dpeche["site_transect_name"] . "-" . $dpeche["rive_libelle"] . "-" . $dpeche["position_engin_name"] . "-" . $mois . "-" . $annee;
			$name = str_replace(array(" ", "é"), array("_", "e"), $name);
			$uidMin = 99999999;
			$uidMax = 0;

			try {
				foreach ($echantillons as $ech) {
					$identifier = mb_strtoupper($name . "-" . $ech["nom_fr"]);
					$data = array(
						"uuid" => $ech["uuid"],
						"collection_name" => $_SESSION["dbparams"]["cs_collection_name"],
						"sample_type_name" => $_SESSION["dbparams"]["cs_sample_name_echantillon"],
						"identifier" => $identifier,
						"parent_uid" => $dpeche["cs_uid"],
						"sampling_date" => $dpeche["trait_debut"],
						"multiple_value" => $ech["nombre"],
						"md_taxon" => $ech["nom"],
						"dbuid_origin" => "transect:" . $ech["echantillon_id"],
						"login" => $_SESSION["dbparams"]["cs_login"],
						"token" => $_SESSION["dbparams"]["cs_token"]
					);
					$result_json = apiCall("POST", $_SESSION["dbparams"]["cs_update"], "", $data, $this->appConfig->CURL_debugmode);
					$result = json_decode($result_json, true);
					if (empty($result)) {
						throw new PpciException(_("L'opération n'a pas abouti pour une cause inconnue, aucune information technique n'est disponible"));
					} elseif ($result["error_code"] != 200) {
						throw new PpciException(sprintf(_("L'erreur %1s a été générée lors du traitement de l'échantillon %3s : %2s"), $result["error_code"], $result["error_message"] . " " . $result["error_detail"], $ech["echantillon_id"]));
					}
					$uid = $result['uid'];
					if ($uid > 0) {
						if ($uid < $uidMin) {
							$uidMin = $uid;
						}
						if ($uid > $uidMax) {
							$uidMax = $uid;
						}
					}
				}
				$this->message->set("Premier UID créé ou mis à jour : " . $uidMin);
				$this->message->set("Dernier UID créé ou mis à jour : " . $uidMax);
				return $libEchantillon->list();
			} catch (PpciException $e) {
				$this->message->set($e->getMessage(), true);
				return $libEchantillon->list();
			}
		} else {
			$this->message->set("L'UID Collec-Science n'a pas été saisi, ou la saisie n'est pas terminée", true);
			return $libEchantillon->list();
		}
	}
	function generateCSfile()
	{
		/*
		 * Genere le fichier pret a etre importe dans Collec-Science
		 * pour creer les echantillons de lyophilisation
		 */
		try {
			$this->vue = service ("CsvView");
			$tp = new TraitPeche();
			$data = $tp->generateCSfile($this->id, $_SESSION["dbparams"]["CS_project_id"], $_SESSION["dbparams"]["CS_sample_type_id"]);
			$this->vue->set($data);
			$this->vue->send();
		} catch (PpciException $e) {
			$this->message->set($e->getMessage(),true);
			$libEchantillon = new LibrariesEchantillon;
			return $libEchantillon->list();
		}
	}
}
