<?php

namespace App\Libraries;

use App\Models\Bateau;
use App\Models\Campagne;
use App\Models\Echantillon;
use App\Models\Experimentation;
use App\Models\Peche;
use App\Models\Personne;
use App\Models\Protocole;
use App\Models\SearchTrait;
use App\Models\Site;
use App\Models\Site_transect;
use App\Models\Trait_geom;
use App\Models\Trait_op as ModelsTrait_op;
use App\Models\TraitPdf;
use Ppci\Libraries\PpciException;
use Ppci\Libraries\PpciLibrary;
use Ppci\Models\PpciModel;

class Trait_op extends PpciLibrary
{
	/**
	 * @var ModelsTrait_op
	 */
	protected PpciModel $dataclass;
	public $keyName;	private $warnings = [];

	function __construct()
	{
		parent::__construct();
		$this->dataclass = new ModelsTrait_op();
		$this->keyName = "trait_id";
		if (isset($_REQUEST[$this->keyName])) {
			$this->id = $_REQUEST[$this->keyName];
		}
		if (!isset($_SESSION["searchTrait"])) {
			$_SESSION["searchTrait"] = new SearchTrait;
		}
	}
	function genericList()
	{
		$this->vue = service('Smarty');
		$_SESSION["searchTrait"]->setParam($_REQUEST);
		$dataSearch = $_SESSION["searchTrait"]->getParam();
		if ($_SESSION["searchTrait"]->isSearch() == 1) {
			$data = $this->dataclass->getListeSearch($dataSearch);
			$this->vue->set($data, "data");
			$this->vue->set(1, "isSearch");
		}
		$site = new Site();
		$this->vue->set($site->getListeConsult(), "site");
		$transect = new Site_transect();
		$this->vue->set($transect->getListe(2), "transect");
		$this->vue->set($dataSearch, "dataSearch");
		$this->vue->set("trait/traitList.tpl", "corps");
		$experimentation = new Experimentation();
		$this->vue->set($experimentation->getListe(2), "experimentation");
		$campagne = new Campagne();
		$this->vue->set($campagne->getLastCampaigns(0, 48), "campagnes");
		$this->vue->set($this->warnings, "warnings");
	}
	function list()
	{
		$this->genericList();
		return $this->vue->send();
	}
	function pecheList()
	{
		$this->genericList();
		if ($this->id > 0) {
			$peche = new Peche();
			$this->vue->set($peche->getListeFromTrait($this->id), "dataPeche");
			$this->vue->set($this->id, "trait_id");
			$this->vue->set("pecheList", "module");
		}
		return $this->vue->send();
	}
	function importExcel()
	{
		$this->genericList();
		$this->vue->set("importExcel", "module");
		return $this->vue->send();
	}
	function importXml()
	{
		$this->genericList();
		$this->vue->set("importXml", "module");
		return $this->vue->send();
	}

	function importSonde()
	{
		$this->genericList();
		if ($_SESSION["searchTrait"]->isSearch() == 1) {
			if ($this->id > 0) {
				$this->vue->set("importSonde", "module");
				$this->vue->set($this->id, "trait_id");
			}
		}
		return $this->vue->send();
	}
	function display()
	{
		$this->vue = service('Smarty');
		/*
		 * Display the detail of the record
		 */
		$data = $this->dataclass->getDetail($this->id);
		$this->vue->set($data, "data");
		$this->vue->set("trait/traitDisplay.tpl", "corps");

		if ($this->id > 0) {
			$peche = new Peche();
			$this->vue->set($peche->getListeFromTrait($this->id), "dataPeche");
			$this->vue->set($this->id, "trait_id");
		}
		return $this->vue->send();
	}
	function change()
	{
		$this->vue = service('Smarty');
		$data = $this->dataRead($this->id, "trait/traitChange.tpl");
		$traitGeom = new Trait_geom;
		$this->vue->set($traitGeom->lire($data["trait_id"]), "geom");
		/*
		 * Lecture des tables de parametres
		 */
		$site = new Site();
		$this->vue->set($site->getListeConsult(), "site");
		$personne = new Personne();
		$this->vue->set($personne->getListeActif($this->id), "personne");
		$experimentation = new Experimentation();
		$this->vue->set($experimentation->getListActif($this->id), "experimentation");
		$protocole = new Protocole();
		$this->vue->set($protocole->getListe(1), "protocole");
		$bateau = new Bateau();
		$this->vue->set($bateau->getListe(1), "bateau");
		$campagne = new Campagne();
		$this->vue->set($campagne->getLastCampaigns(0, 0), "campagnes");
		return $this->vue->send();
	}
	function write()
	{
		try {
			$this->id = $this->dataWrite($_REQUEST);
			if ($this->id > 0) {
				$_REQUEST[$this->keyName] = $this->id;
				return $this->display();
			} else {
				return $this->change();
			}
		} catch (PpciException) {
			return $this->change();
		}
	}
	function delete()
	{
		/*
		 * delete record
		 */
		try {
			$this->dataDelete($this->id);
			return $this->list();
		} catch (PpciException $e) {
			return $this->change();
		}
	}
	function traitPdf()
	{
		/*
		 * Generation de la fiche récapitulative des traits
		 */
		$_SESSION["searchTrait"]->setParam($_REQUEST);
		$dataSearch = $_SESSION["searchTrait"]->getParam();
		/*
		 * Recuperation des données du trait
		 */
		$dataTrait = $this->dataclass->getListePdf($dataSearch);
		if (count($dataTrait) > 0) {
			/*
			 * Generation du PDF
			 */
			$peche = new Peche();
			$echantillon = new Echantillon();
			$traitPdf = new TraitPdf([]);
			foreach ($dataTrait as $key => $value) {
				$traitPdf->setDataTrait($value);
				/*
				 * Recuperation des donnees de peche
				 */
				$dataPeche = $peche->getListeFromTrait($value["trait_id"]);
				foreach ($dataPeche as $key => $value) {
					$dataEchan = $echantillon->getListFromPeche($value["peche_id"]);
					$dataPeche[$key]["echantillon"] = $dataEchan;
				}
				$traitPdf->setDataPeche($dataPeche);
			}
			/*
			 * Envoi au navigateur
			 */
			$traitPdf->sent();
		} else {
			$this->message->set("Aucun trait dans la liste ou traits trop anciens (édition PDF uniquement pour les 2 dernières années)");
			return $this->list();
		}
	}
}
