<?php

namespace App\Libraries;

use App\Models\MotifAnnulation as ModelsMotifAnnulation;
use Ppci\Libraries\PpciException;
use Ppci\Libraries\PpciLibrary;
use Ppci\Models\PpciModel;

class MotifAnnulation extends PpciLibrary
{
    /**
     * @var ModelsMotifAnnulation
     */
    protected PpciModel $dataclass;
    public $keyName;
    function __construct()
    {
        parent::__construct();
        $this->dataclass = new ModelsMotifAnnulation();
        $this->keyName = "motif_annulation_id";
        if (isset($_REQUEST[$this->keyName])) {
            $this->id = $_REQUEST[$this->keyName];
        }
    }

    function list()
    {
        $this->vue = service('Smarty');
        $this->vue->set($this->dataclass->getListe(1), "data");
        $this->vue->set("param/motifAnnulationList.tpl", "corps");
        return $this->vue->send();
    }
    function change()
    {
        $this->vue = service('Smarty');
        $this->dataRead($this->id, "param/motifAnnulationChange.tpl");
        return $this->vue->send();
    }
    function write()
    {
        try {
            $this->id = $this->dataWrite($_REQUEST);
            if ($this->id > 0) {
                $_REQUEST[$this->keyName] = $this->id;
                return $this->list();
            } else {
                return $this->change();
            }
        } catch (PpciException) {
            return $this->change();
        }
    }
    function delete()
    {
        try {
            $this->dataDelete($this->id);
            return $this->list();
        } catch (PpciException $e) {
            return $this->change();
        }
    }
}
